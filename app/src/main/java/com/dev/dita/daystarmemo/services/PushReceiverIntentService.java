package com.dev.dita.daystarmemo.services;

import android.app.Activity;
import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.baasbox.android.json.JsonObject;
import com.dev.dita.daystarmemo.controller.utils.StringUtils;
import com.dev.dita.daystarmemo.model.database.Connection;
import com.dev.dita.daystarmemo.model.objects.Notification;


/**
 * The type Push receiver intent service. Receives message from gcm service and then sends an
 * ordered broadcast with the message details
 */
public class PushReceiverIntentService extends IntentService {

    private final String TAG = getClass().getName();
    private Handler.Callback callback = new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            throw new IllegalArgumentException("PUSH_RECEIVED NOT HANDLED");
        }
    };

    public PushReceiverIntentService() {
        super("PushReceiverIntentService");
    }


    @Override
    protected void onHandleIntent(Intent intent) {
        Bundle extras = intent.getExtras();
        Notification notification;

        if (extras.getSerializable("notification") != null) {
            notification = (Notification) extras.getSerializable("notification");
            if (Connection.exists(notification.sender)) {
                sendNotification(notification);
            } else {
                Log.i(TAG, "NOTIFICATION IGNORED");
            }
        } else {
            // Create Notification object containing push message details
            String custom = extras.getString("custom", null);
            custom = StringUtils.parseStringToCorrectJsonString(custom);
            JsonObject object = JsonObject.decode(custom);
            notification = new Notification();
            notification.sender = object.getString("sender", "");
            notification.id = object.getString("id", "");
            notification.message = object.getString("message", "no message");
            notification.type = object.getString("type").equals("file") ? Notification.FILE : Notification.MESSAGE;
            notification.size = object.getLong("size", 0);
            notification.extraMessage = object.getString("name", "");
            if (Connection.exists(notification.sender)) {
                sendNotification(notification, extras);
            } else {
                Log.i(TAG, "NOTIFICATION IGNORED");
            }

        }
    }

    private void sendNotification(Notification notification) {
        sendNotification(notification, new Bundle());
    }

    private void sendNotification(Notification notification, Bundle extras) {
        Intent broadcast = new Intent();
        extras.putSerializable("notification", notification);
        broadcast.putExtras(extras);
        broadcast.setAction("com.dev.dita.daystarmemo.BROADCAST_NOTIFICATION");
        sendOrderedBroadcast(broadcast, null, null, new Handler(callback), Activity.RESULT_OK, null, extras);
    }
}
