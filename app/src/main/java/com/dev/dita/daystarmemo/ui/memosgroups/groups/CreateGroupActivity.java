package com.dev.dita.daystarmemo.ui.memosgroups.groups;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.baasbox.android.BaasDocument;
import com.baasbox.android.BaasUser;
import com.baasbox.android.json.JsonArray;
import com.baasbox.android.json.JsonObject;
import com.dev.dita.daystarmemo.R;
import com.dev.dita.daystarmemo.controller.bus.GroupBus;
import com.dev.dita.daystarmemo.model.baas.GroupBaas;
import com.dev.dita.daystarmemo.model.database.Group;
import com.dev.dita.daystarmemo.model.objects.GroupContainer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import fr.ganfra.materialspinner.MaterialSpinner;

public class CreateGroupActivity extends AppCompatActivity {

    @BindView(R.id.thumbnail)
    CircleImageView thumbnail;
    @BindView(R.id.name)
    EditText name;
    @BindView(R.id.type)
    MaterialSpinner type;
    @BindView(R.id.privacy)
    MaterialSpinner privacy;
    @BindView(R.id.interaction)
    MaterialSpinner interaction;
    @BindView(R.id.create_button)
    Button createButton;
    ProgressDialog progressDialog;
    List<BaasDocument> groups;

    private void init() {
        GroupContainer container = (GroupContainer) getIntent().getSerializableExtra("groups");
        groups = container.groups;
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Creating group ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);

        name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                name.setError(null);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                type.setError(null);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        privacy.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                privacy.setError(null);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        interaction.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                interaction.setError(null);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        String[] types = {"Academic", "School/Club", "Committee"};
        ArrayAdapter<String> typeAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, types);
        typeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        type.setAdapter(typeAdapter);
        String[] privacyOptions = {"Open", "Closed"};
        ArrayAdapter<String> privacyAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, privacyOptions);
        typeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        privacy.setAdapter(privacyAdapter);
        String[] interactionOptions = {"Informative", "Interactive"};
        ArrayAdapter<String> interactionAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, interactionOptions);
        typeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        interaction.setAdapter(interactionAdapter);

        setTitle("Create group");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_group);
        ButterKnife.bind(this);
        EventBus.getDefault().register(this);
        init();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @OnClick(R.id.create_button)
    public void onCreateClicked() {
        if (name.getText().toString().isEmpty()) {
            name.requestFocus();
            name.setError("Field required");
            return;
        }
        Log.i(getClass().getName(), String.valueOf(type.getSelectedItemPosition()));
        if (type.getSelectedItemPosition() == 0) {
            type.requestFocus();
            type.setError("Please select a type");
            return;
        }

        if (privacy.getSelectedItemPosition() == 0) {
            privacy.requestFocus();
            privacy.setError("Please select privacy type");
            return;
        }

        if (interaction.getSelectedItemPosition() == 0) {
            privacy.requestFocus();
            privacy.setError("Please select interaction type");
            return;
        }

        progressDialog.show();
        String groupName = name.getText().toString();
        checkIfGroupExists(groupName);
    }

    @Subscribe
    public void onEvent(GroupBus.CreateGroupResult event) {
        if (event.error) {
            progressDialog.dismiss();
            Toast.makeText(this, "Unable to create group", Toast.LENGTH_LONG).show();
        } else {
            Group.saveGroup(BaasDocument.from(event.object));
        }
    }

    @Subscribe
    public void onEvent(GroupBus.SaveGroupResult event) {
        progressDialog.dismiss();
        // TODO: 25/09/16 launch group details activity

    }

    private void checkIfGroupExists(final String groupName) {
        new AsyncTask<Void, Void, Boolean>() {
            @Override
            protected Boolean doInBackground(Void... voids) {
                if (groups != null) {
                    for (BaasDocument group : groups) {
                        if (group.getString("name").equalsIgnoreCase(groupName)) {
                            return true;
                        }
                    }
                }

                return false;
            }

            @Override
            protected void onPostExecute(Boolean aBoolean) {
                if (aBoolean) {
                    progressDialog.dismiss();
                    new AlertDialog.Builder(CreateGroupActivity.this)
                            .setMessage(String.format(Locale.getDefault(), "'%s' exists. Join it instead?", groupName))
                            .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {

                                }
                            })
                            .setNegativeButton(R.string.no, null)
                            .show();
                } else {
                    String typeField = (String) type.getSelectedItem();
                    String privacyField = (String) privacy.getSelectedItem();
                    String interactionField = (String) interaction.getSelectedItem();

                    JsonArray array = new JsonArray();
                    array.add(BaasUser.current().getName());

                    JsonObject object = new JsonObject();
                    object.put("name", groupName);
                    object.put("type", typeField);
                    object.put("privacy", privacyField);
                    object.put("interaction", interactionField);
                    object.put("owners", array);
                    object.put("members", array);
                    GroupBaas.createGroup(CreateGroupActivity.this, object);
                }
            }
        }.execute();

    }
}
