package com.dev.dita.daystarmemo.controller.bus;

import com.dev.dita.daystarmemo.model.database.Memo;

import java.util.ArrayList;

public class MemoBus {
    public static class SendMemoEvent {
    }

    public static class SendMemoResult {

        public Boolean error;
        public Memo memo;
    }

    public static class SendBroadcastMemoResult {
        public boolean error;
        public ArrayList<Memo> memos;
    }

    public static class FetchMemoResult {
        public boolean error;
    }

    public static class PendingMemoUpdatedResult {
    }

    public static class TransferProgress {
        public String id;
        public long current;
        public long total;
        public int progress;
        public boolean isDone;
    }

    public static class SendFileResult {
        public String transferId;
        public boolean error;
        public Memo memo;
    }

    public static class StartTransferService {
    }

    public static class ScrollToBottomEvent {
    }
}
