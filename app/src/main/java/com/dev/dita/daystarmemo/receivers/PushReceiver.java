package com.dev.dita.daystarmemo.receivers;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.dev.dita.daystarmemo.services.PushIntentService;

/**
 * The type Push receiver. Receives notification object if not intercepted by an activity in the app
 */
public class PushReceiver extends BroadcastReceiver {
    public PushReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Intent push = new Intent(context, PushIntentService.class);
        push.putExtras(getResultExtras(true));
        context.startService(push);
        setResultCode(Activity.RESULT_OK);
    }
}
