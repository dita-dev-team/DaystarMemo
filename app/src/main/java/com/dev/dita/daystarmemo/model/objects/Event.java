package com.dev.dita.daystarmemo.model.objects;

import java.io.Serializable;

/**
 * Created by nathan on 06/08/16.
 */

public final class Event implements Serializable {
    public String title;

    public Event(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return title;
    }
}
