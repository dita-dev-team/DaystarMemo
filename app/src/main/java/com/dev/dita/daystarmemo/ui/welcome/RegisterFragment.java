package com.dev.dita.daystarmemo.ui.welcome;


import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.dev.dita.daystarmemo.R;
import com.dev.dita.daystarmemo.controller.bus.UserBus;
import com.dev.dita.daystarmemo.controller.utils.UIUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class RegisterFragment extends Fragment {
    /**
     * The Username txt.
     */
    @BindView(R.id.register_username)
    TextInputEditText usernameTxt;
    /**
     * The Email txt.
     */
    @BindView(R.id.register_email)
    TextInputEditText emailTxt;
    /**
     * The Password txt.
     */
    @BindView(R.id.register_pass)
    TextInputEditText passwordTxt;
    /**
     * The Password confirm txt.
     */
    @BindView(R.id.register_pass_confirm)
    TextInputEditText passwordConfirmTxt;

    /**
     * The Username.
     */
    String username;
    /**
     * The Email.
     */
    String email;
    /**
     * The Password.
     */
    String password;

    /**
     * Instantiates a new Register fragment.
     */
    public RegisterFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_register, container, false);
        ButterKnife.bind(this, view);
        EventBus.getDefault().register(this);
        return view;
    }

    @Override
    public void onDestroyView() {
        EventBus.getDefault().unregister(this);
        super.onDestroyView();
    }

    /**
     * Register.
     */
    @OnClick(R.id.register_submit_button)
    public void register() {
        UIUtils.hideKeyboard(getActivity());
        // Verify all input has been provided
        if (TextUtils.isEmpty(usernameTxt.getText().toString()) || !isValidUsername(usernameTxt.getText().toString())) {
            usernameTxt.requestFocus();
            usernameTxt.setError(getString(R.string.username_format_error));
            return;
        }

        if (TextUtils.isEmpty(emailTxt.getText().toString()) || !isValidEmail(emailTxt.getText().toString())) {
            emailTxt.requestFocus();
            emailTxt.setError(getString(R.string.email_format_error));
            return;
        }

        if (TextUtils.isEmpty(passwordTxt.getText().toString()) ||
                TextUtils.isEmpty(passwordConfirmTxt.getText().toString())) {
            Toast.makeText(getContext(), R.string.fill_all_fields, Toast.LENGTH_SHORT).show();
            return;
        }

        if (!passwordTxt.getText().toString().equals(passwordConfirmTxt.getText().toString())) {
            passwordConfirmTxt.setError(getResources().getString(R.string.passwords_dont_match));
            Toast.makeText(getContext(), R.string.passwords_dont_match, Toast.LENGTH_SHORT).show();
            return;
        }

        username = usernameTxt.getText().toString().trim();
        password = passwordTxt.getText().toString();
        email = emailTxt.getText().toString().trim();

        // Post a register event
        EventBus.getDefault().post(new UserBus.RegisterEvent(username, email, password));
    }

    /**
     * On event.
     *
     * @param registerResult the register result
     */
    @Subscribe
    public void onEvent(UserBus.RegisterResult registerResult) {
        EventBus.getDefault().post(new UserBus.Notify());
        if (registerResult.error) {
            Toast.makeText(getContext(), registerResult.message, Toast.LENGTH_LONG).show();
        } else {
            ((WelcomeActivity) getActivity()).showLoginView(null);
            Toast.makeText(getContext(), R.string.please_login, Toast.LENGTH_LONG).show();
        }
    }

    private boolean isValidUsername(String username) {
        String USERNAME_PATTERN = "^([0-9]{2}-[0-9]{4})$";
        Pattern pattern = Pattern.compile(USERNAME_PATTERN);
        Matcher matcher = pattern.matcher(username);
        return matcher.matches();
    }

    private boolean isValidEmail(String email) {
        String EMAIL_PATTERN = "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }
}
