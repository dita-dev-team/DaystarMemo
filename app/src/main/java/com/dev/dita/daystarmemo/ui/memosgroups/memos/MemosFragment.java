package com.dev.dita.daystarmemo.ui.memosgroups.memos;


import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dev.dita.daystarmemo.R;
import com.dev.dita.daystarmemo.model.database.Memo;
import com.dev.dita.daystarmemo.ui.connections.ConnectionDetailsActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;
import io.realm.Sort;

/**
 * A simple {@link Fragment} subclass.
 */
public class MemosFragment extends Fragment {
    final String TAG = getClass().getName();
    @BindView(R.id.empty)
    TextView empty;
    @BindView(R.id.list_view)
    RecyclerView listView;


    private Realm realm;
    private RealmResults<Memo> memos;
    private MemosListAdapter adapter;
    private RealmChangeListener<RealmResults<Memo>> listener = new RealmChangeListener<RealmResults<Memo>>() {
        @Override
        public void onChange(RealmResults<Memo> element) {
            for (Memo memo : element) {
                System.out.println(memo.body);
            }
            if (!element.isEmpty()) {
                adapter.updateData(element.distinct("connection"));
                adapter.notifyDataSetChanged();
            }
            empty.setVisibility(adapter.getItemCount() == 0 ? View.VISIBLE : View.GONE);
        }
    };

    private ActionMode actionMode;

    public MemosFragment() {
        // Required empty public constructor
    }

    private void init() {
        realm = Realm.getDefaultInstance();
        memos = realm.where(Memo.class).equalTo("isFile", false).findAllSortedAsync("date", Sort.DESCENDING);
        memos.addChangeListener(listener);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        listView.setLayoutManager(layoutManager);
        adapter = new MemosListAdapter(getContext(), memos, true);
        adapter.setOnItemClickListener(new MemosListAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Memo memo = adapter.getItem(position);
                String username = memo.connection;
                Intent intent = new Intent(getContext(), MemosChatActivity.class);
                intent.putExtra("username", username);
                startActivity(intent);
            }
        });
        adapter.setOnItemLongClickListener(new MemosListAdapter.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(View view, int position) {
                if (actionMode != null) {
                    return false;
                }

                actionMode = getActivity().startActionMode(buildCallback(position));
                return true;
            }
        });
        listView.setAdapter(adapter);

        empty.setVisibility(adapter.getItemCount() == 0 ? View.VISIBLE : View.GONE);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_memos, container, false);
        ButterKnife.bind(this, view);
        init();
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        memos.removeChangeListeners();
        realm.close();
    }

    private ActionMode.Callback buildCallback(final int position) {
        return new ActionMode.Callback() {
            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                MenuInflater inflater = mode.getMenuInflater();
                inflater.inflate(R.menu.memos_list_contextual_menu, menu);
                return true;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                Memo memo = adapter.getItem(position);
                final String username = memo.connection;
                switch (item.getItemId()) {
                    case R.id.memos_list_contextual_delete:
                        new AlertDialog.Builder(getContext())
                                .setTitle("Delete")
                                .setMessage("Are you sure?")
                                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        Memo.deleteMemoByUsername(username);
                                    }
                                })
                                .setNegativeButton("No", null)
                                .show();
                        actionMode.finish();
                        return true;
                    case R.id.memos_list_contextual_view:
                        Intent intent = new Intent(getContext(), ConnectionDetailsActivity.class);
                        intent.putExtra("username", username);
                        startActivity(intent);
                        actionMode.finish();
                        return true;
                    case R.id.memos_list_contextual_mark:
                        Memo.markAsReadByUsername(username);
                        actionMode.finish();
                        return true;
                    default:
                        return false;
                }
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {
                actionMode = null;
            }
        };
    }

}
