package com.dev.dita.daystarmemo.controller.utils;


import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.view.View;

public class AnimationUtils {

    public static void translateVisibility(final View view, int duration, final int visibility) {
        if (visibility == View.VISIBLE) {
            view.setY(1000); // for initial visibility
            view.animate()
                    .translationY(0)
                    .setDuration(duration)
                    .alpha(1.0f)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationStart(Animator animation) {
                            super.onAnimationStart(animation);
                            view.setVisibility(visibility);
                        }
                    });
        } else {
            view.animate()
                    .translationY(1000)
                    .setDuration(duration)
                    .alpha(0.0f)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            super.onAnimationEnd(animation);
                            view.setVisibility(visibility);
                        }
                    });
        }

    }
}
