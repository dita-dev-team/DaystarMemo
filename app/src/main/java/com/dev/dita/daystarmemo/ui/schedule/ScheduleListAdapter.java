package com.dev.dita.daystarmemo.ui.schedule;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.dev.dita.daystarmemo.R;
import com.dev.dita.daystarmemo.model.database.Schedule;

import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.OrderedRealmCollection;
import io.realm.RealmRecyclerViewAdapter;

/**
 * The type Schedule list adapter.
 */
class ScheduleListAdapter extends RealmRecyclerViewAdapter<Schedule, ScheduleListAdapter.ViewHolder> {

    /**
     * The Item click listener.
     */
    private OnItemClickListener itemClickListener;
    /**
     * The Item long click listener.
     */
    private OnItemLongClickListener itemLongClickListener;

    private Drawable background1;
    private Drawable background2;

    private String day;
    private Date time;

    /**
     * Instantiates a new Schedule list adapter.
     *
     * @param context    the context
     * @param data       the data
     * @param autoUpdate the auto update
     */
    ScheduleListAdapter(Context context, OrderedRealmCollection<Schedule> data, boolean autoUpdate) {
        super(context, data, autoUpdate);
        background1 = ContextCompat.getDrawable(context, R.drawable.circular_background1);
        background2 = ContextCompat.getDrawable(context, R.drawable.circular_background2);
        day = com.dev.dita.daystarmemo.controller.utils.DateUtils.getCurrentDay();
        time = new Date();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.schedule_list_item, parent, false);
        final ViewHolder viewHolder = new ViewHolder(view);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemClickListener.onItemClick(view, viewHolder.getLayoutPosition());
            }
        });
        view.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                return itemLongClickListener.onItemLongClick(view, viewHolder.getLayoutPosition());
            }
        });
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Schedule schedule = getItem(position);
        holder.eventTitle.setText(schedule.title);
        String dayAndTime = schedule.day + " " + DateUtils.formatDateTime(context, schedule.startTime.getTime(), DateUtils.FORMAT_SHOW_TIME);
        holder.eventDuration.setText(dayAndTime);
        holder.eventLocation.setText(schedule.location);
        String firstLetter = String.valueOf(schedule.title.charAt(0));
        holder.thumbnail.setText(firstLetter.toUpperCase());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {

            if (position == 0) {
                holder.thumbnail.setBackground(background1);
            } else {
                if (position % 2 == 0) {
                    holder.thumbnail.setBackground(background1);
                } else {
                    holder.thumbnail.setBackground(background2);
                }
            }
        }

        // set active schedule
        if (com.dev.dita.daystarmemo.controller.utils.DateUtils.compareTimes(schedule.startTime, time) >= 0 &&
                com.dev.dita.daystarmemo.controller.utils.DateUtils.compareTimes(schedule.endTime, time) <= 0 &&
                day.equalsIgnoreCase(schedule.day)) {
            holder.active.setVisibility(View.VISIBLE);
        } else {
            holder.active.setVisibility(View.GONE);
        }
    }

    /**
     * Sets on item click listener.
     *
     * @param itemClickListener the item click listener
     */
    public void setOnItemClickListener(OnItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    /**
     * Sets on item long click listener.
     *
     * @param itemLongClickListener the item long click listener
     */
    void setOnItemLongClickListener(OnItemLongClickListener itemLongClickListener) {
        this.itemLongClickListener = itemLongClickListener;
    }

    /**
     * The interface On item click listener.
     */
    public interface OnItemClickListener {
        /**
         * On item click.
         *
         * @param view     the view
         * @param position the position
         */
        void onItemClick(View view, int position);
    }

    /**
     * The interface On item long click listener.
     */
    interface OnItemLongClickListener {
        /**
         * On item long click boolean.
         *
         * @param view     the view
         * @param position the position
         * @return the boolean
         */
        boolean onItemLongClick(View view, int position);
    }

    /**
     * The type View holder.
     */
    class ViewHolder extends RecyclerView.ViewHolder {
        /**
         * The Event title.
         */
        @BindView(R.id.event_title)
        TextView eventTitle;
        /**
         * The Event duration.
         */
        @BindView(R.id.event_duration)
        TextView eventDuration;
        /**
         * The Event location.
         */
        @BindView(R.id.event_location)
        TextView eventLocation;

        @BindView(R.id.thumbnail)
        TextView thumbnail;

        @BindView(R.id.active)
        ImageButton active;

        /**
         * Instantiates a new View holder.
         *
         * @param view the view
         */
        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
