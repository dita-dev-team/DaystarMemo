package com.dev.dita.daystarmemo.ui.documents;


import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.dev.dita.daystarmemo.R;
import com.dev.dita.daystarmemo.controller.utils.FileUtils;
import com.dev.dita.daystarmemo.model.database.Memo;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;
import io.realm.Sort;

/**
 * A simple {@link Fragment} subclass.
 */
public class ReceivedDocumentsFragment extends Fragment {

    final String TAG = getClass().getName();
    @BindView(R.id.received_documents_empty)
    TextView empty;
    @BindView(R.id.received_documents_gridview)
    RecyclerView gridView;

    private DocumentsAdapter adapter;
    private RealmChangeListener listener = new RealmChangeListener() {
        @Override
        public void onChange(Object element) {
            empty.setVisibility(adapter.getItemCount() == 0 ? View.VISIBLE : View.GONE);
        }
    };

    public ReceivedDocumentsFragment() {
        // Required empty public constructor
    }

    private void init() {
        Realm realm = Realm.getDefaultInstance();
        RealmResults<Memo> memos = realm.where(Memo.class).equalTo("isFile", true).equalTo("sentByMe", false).findAllSortedAsync("date", Sort.DESCENDING);
        memos.addChangeListener(listener);
        GridLayoutManager layoutManager = new GridLayoutManager(getContext(), 3);
        gridView.setLayoutManager(layoutManager);
        adapter = new DocumentsAdapter(getContext(), memos, true);
        adapter.adapterType = DocumentsAdapter.RECEIVED;
        adapter.setOnItemClickListener(new DocumentsAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Memo memo = adapter.getItem(position);
                File file = new File(memo.filepath);
                Uri uri = Uri.fromFile(file);
                String mime = FileUtils.getMimeType(FileUtils.removeWhitespace(file.getName()));

                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.setDataAndType(uri, mime);
                try {
                    startActivity(intent);
                } catch (ActivityNotFoundException ex) {
                    Crashlytics.logException(ex);
                    Toast.makeText(getContext(), "No application found", Toast.LENGTH_SHORT).show();
                }
            }
        });
        gridView.setAdapter(adapter);
        empty.setVisibility(adapter.getItemCount() == 0 ? View.VISIBLE : View.GONE);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_received_documents, container, false);
        ButterKnife.bind(this, view);
        EventBus.getDefault().register(this);
        init();
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe
    public void onEvent(DocumentsActivity.ClearReceivedDocuments event) {
        int count = adapter.getItemCount();
        if (count == 0) {
            return;
        }

        new AlertDialog.Builder(getContext())
                .setMessage(getString(R.string.remove_documents, count))
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Memo.deleteReceivedMemoDocuments();
                    }
                })
                .setNegativeButton(R.string.no, null)
                .show();
    }

}
