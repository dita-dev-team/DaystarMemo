package com.dev.dita.daystarmemo.ui.memosgroups.groups;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dev.dita.daystarmemo.R;
import com.dev.dita.daystarmemo.model.database.Group;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;

/**
 * A simple {@link Fragment} subclass.
 */
public class GroupsFragment extends Fragment {
    final String TAG = getClass().getName();
    @BindView(R.id.empty)
    TextView empty;
    @BindView(R.id.list_view)
    RecyclerView listView;

    RealmResults<Group> groups;
    GroupsListAdapter adapter;
    private RealmChangeListener<RealmResults<Group>> listener = new RealmChangeListener<RealmResults<Group>>() {
        @Override
        public void onChange(RealmResults<Group> element) {
//            if (!element.isEmpty()) {
//                adapter.updateData(element.distinct("connection"));
//                adapter.notifyDataSetChanged();
//            }
            empty.setVisibility(adapter.getItemCount() == 0 ? View.VISIBLE : View.GONE);
        }
    };

    public GroupsFragment() {
        // Required empty public constructor
    }

    private void init() {
        Realm realm = Realm.getDefaultInstance();
        groups = realm.where(Group.class).findAllAsync();
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        listView.setLayoutManager(layoutManager);
        adapter = new GroupsListAdapter(getContext(), groups, true);
        listView.setAdapter(adapter);
        empty.setVisibility(adapter.getItemCount() == 0 ? View.VISIBLE : View.GONE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_groups, container, false);
        ButterKnife.bind(this, view);
        init();
        return view;
    }

}
