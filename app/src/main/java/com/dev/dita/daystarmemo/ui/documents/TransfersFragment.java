package com.dev.dita.daystarmemo.ui.documents;


import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SimpleItemAnimator;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dev.dita.daystarmemo.R;
import com.dev.dita.daystarmemo.controller.bus.MemoBus;
import com.dev.dita.daystarmemo.model.database.Transfer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;
import io.realm.Sort;

/**
 * A simple {@link Fragment} subclass.
 */
public class TransfersFragment extends Fragment {

    final String TAG = getClass().getName();
    @BindView(R.id.transfers_empty)
    TextView empty;
    @BindView(R.id.transfers_listview)
    RecyclerView listView;
    RealmResults<Transfer> transfers;
    private TransfersAdapter adapter;
    private final RealmChangeListener<RealmResults<Transfer>> listener = new RealmChangeListener<RealmResults<Transfer>>() {
        @Override
        public void onChange(RealmResults<Transfer> element) {
            ArrayList<Transfer> transfers = new ArrayList<>();
            for (Transfer t : element) {
                transfers.add(t.copyObject());
            }
            Log.i(TAG, "DATA BEING SET");
            adapter.setData(transfers);
            adapter.notifyDataSetChanged();
            empty.setVisibility(adapter.getItemCount() == 0 ? View.VISIBLE : View.GONE);
        }
    };

    public TransfersFragment() {
        // Required empty public constructor
    }

    private void init() {
        listView.setHasFixedSize(true);
        ((SimpleItemAnimator) listView.getItemAnimator()).setSupportsChangeAnimations(false);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        listView.setLayoutManager(layoutManager);
        adapter = new TransfersAdapter(getContext(), new ArrayList<Transfer>());
        listView.setAdapter(adapter);

        prepareSimulationData();
        empty.setVisibility(adapter.getItemCount() == 0 ? View.VISIBLE : View.GONE);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_transfers, container, false);
        ButterKnife.bind(this, view);
        EventBus.getDefault().register(this);
        init();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        loadTransfers();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe
    public void onEvent(final MemoBus.TransferProgress progress) {
        if (adapter.containsTransfer(progress.id)) {
            EventBus.getDefault().cancelEventDelivery(progress);
            final int index = adapter.getItemIndex(progress.id);
            if (adapter.getData().get(index).status != Transfer.IN_PROGRESS) {
                adapter.getData().get(index).status = Transfer.IN_PROGRESS;
            }
            adapter.getData().get(index).current = progress.current;
            adapter.getData().get(index).progress = progress.progress;


            if (progress.isDone) {
                adapter.getData().get(index).status = Transfer.DONE_PENDING;
            }

            Handler handler = new Handler(Looper.getMainLooper());
            handler.post(new Runnable() {
                @Override
                public void run() {
                    adapter.notifyItemChanged(index);
                }
            });
        }
    }

    @Subscribe
    public void onEvent(DocumentsActivity.ClearTransfers event) {
        int count = adapter.getItemCount();
        if (count == 0) {
            return;
        }

        new AlertDialog.Builder(getContext())
                .setMessage(getString(R.string.remove_transfers, count))
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Transfer.removeCompletedTransfers();
                    }
                })
                .setNegativeButton(R.string.no, null)
                .show();
    }

    private void loadTransfers() {
        Realm realm = Realm.getDefaultInstance();
        transfers = realm.where(Transfer.class).findAllSortedAsync("date", Sort.DESCENDING);
        //transfers.addChangeListener(listener);
    }

    private void prepareSimulationData() {
        Transfer transfer;

        ArrayList<Transfer> transfers = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            transfer = new Transfer();
            transfer.type = Transfer.DOWNLOAD;
            transfer.status = Transfer.PENDING;
            transfer.id = UUID.randomUUID().toString();
            transfer.name = "Download " + String.valueOf(i + 1);
            Log.i(TAG, transfer.name);
            transfer.date = new Date();
            transfers.add(transfer);
        }

        adapter.setData(transfers);
        adapter.notifyDataSetChanged();
    }
}
