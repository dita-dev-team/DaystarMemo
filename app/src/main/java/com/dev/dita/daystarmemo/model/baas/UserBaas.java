package com.dev.dita.daystarmemo.model.baas;

import android.content.Context;
import android.util.Log;

import com.baasbox.android.BaasHandler;
import com.baasbox.android.BaasQuery;
import com.baasbox.android.BaasResult;
import com.baasbox.android.BaasUser;
import com.baasbox.android.RequestToken;
import com.baasbox.android.json.JsonArray;
import com.baasbox.android.json.JsonObject;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.crashlytics.android.answers.LoginEvent;
import com.crashlytics.android.answers.SignUpEvent;
import com.dev.dita.daystarmemo.controller.ErrorHandler;
import com.dev.dita.daystarmemo.controller.bus.UserBus;

import org.greenrobot.eventbus.EventBus;

import java.util.List;


/**
 * A java class which handles all user-management related actions to and from the baas
 */
public class UserBaas {
    /**
     * The constant TAG.
     */
    public static final String TAG = UserBaas.class.getName();


    /**
     * logs in a user
     *
     * @param username user's username
     * @param password user's password
     */
    public static void loginUser(final Context context, String username, String password) {
        final BaasUser user = BaasUser.withUserName(username)
                .setPassword(password);

        user.login(new BaasHandler<BaasUser>() {
            @Override
            public void handle(BaasResult<BaasUser> baasResult) {
                UserBus.LoginResult loginResult = new UserBus.LoginResult();
                Answers.getInstance().logLogin(new LoginEvent()
                        .putMethod("Username")
                        .putSuccess(baasResult.isSuccess()));
                if (baasResult.isSuccess()) {
                    loginResult.error = false;
                } else {
                    loginResult.error = true;
                    loginResult.message = ErrorHandler.getMessage(baasResult.error());
                    Log.i(TAG, baasResult.error().getMessage());
                }
                EventBus.getDefault().post(loginResult);
            }
        });
    }

    /**
     * logout a user
     */
    public static void logoutUser(final Context context) {
        BaasUser.current().logout(new BaasHandler<Void>() {
            @Override
            public void handle(BaasResult<Void> baasResult) {
                UserBus.LogoutResult logoutResult = new UserBus.LogoutResult();
                Answers.getInstance().logCustom(new CustomEvent("Logout")
                        .putCustomAttribute("logoutSuccess", String.valueOf(baasResult.isSuccess())));
                if (baasResult.isSuccess()) {
                    logoutResult.error = false;
                } else {
                    logoutResult.error = true;
                    logoutResult.message = ErrorHandler.getMessage(baasResult.error());
                }
                EventBus.getDefault().post(logoutResult);
            }
        });
    }


    /**
     * create a user
     *
     * @param username user's username
     * @param email    user's email
     * @param password user's password
     */
    public static void createUser(final Context context, String username, String email, String password) {
        final BaasUser user = BaasUser.withUserName(username)
                .setPassword(password);

        user.getScope(BaasUser.Scope.FRIEND)
                .put("name", username)
                .put("email", email);

        user.signup(new BaasHandler<BaasUser>() {
            @Override
            public void handle(BaasResult<BaasUser> baasResult) {
                UserBus.RegisterResult registerResult = new UserBus.RegisterResult();
                Answers.getInstance().logSignUp(new SignUpEvent()
                        .putMethod("Username").putSuccess(baasResult.isSuccess()));
                if (baasResult.isSuccess()) {
                    registerResult.error = false;
                } else {
                    registerResult.error = true;
                    registerResult.message = ErrorHandler.getMessage(baasResult.error());
                    Log.i(TAG, baasResult.error().getMessage());
                }
                EventBus.getDefault().post(registerResult);
            }
        });
    }

    /**
     * Update user profile.
     *
     * @param details user profile details
     */
    public static void updateUserProfile(final JsonObject details) {
        BaasUser user = BaasUser.current();
        user.getScope(BaasUser.Scope.FRIEND).clear();
        user.getScope(BaasUser.Scope.FRIEND).merge(details);
        user.save(new BaasHandler<BaasUser>() {
            @Override
            public void handle(BaasResult<BaasUser> baasResult) {
                UserBus.ProfileUpdatedRemoteResult remoteResult = new UserBus.ProfileUpdatedRemoteResult();
                if (baasResult.isSuccess()) {
                    remoteResult.error = false;
                } else {
                    remoteResult.error = true;
                    Log.e(TAG, baasResult.error().getMessage());
                }
                EventBus.getDefault().post(remoteResult);

            }
        });
    }

    public static void updateSchedules(JsonArray schedule) {
        BaasUser user = BaasUser.current();
        user.getScope(BaasUser.Scope.PRIVATE).clear();
        user.getScope(BaasUser.Scope.PRIVATE).put("schedule", schedule);
        user.save(new BaasHandler<BaasUser>() {
            @Override
            public void handle(BaasResult<BaasUser> baasResult) {
                UserBus.ScheduleUpdatedOnProfile result = new UserBus.ScheduleUpdatedOnProfile();
                if (baasResult.isSuccess()) {
                    result.error = false;
                } else {
                    result.error = true;
                    baasResult.error().printStackTrace();
                }
                EventBus.getDefault().post(result);
            }
        });
    }

    public static void addConnectionToProfile(final BaasUser user) {
        user.follow(new BaasHandler<BaasUser>() {
            @Override
            public void handle(BaasResult<BaasUser> baasResult) {
                UserBus.AddConnectionResult addResult = new UserBus.AddConnectionResult();
                if (baasResult.isSuccess()) {
                    addResult.error = false;
                    addResult.user = user;
                } else {
                    addResult.error = true;
                }
                EventBus.getDefault().post(addResult);
            }
        });
    }

    public static void removeConnectionFromProfile(final String username) {
        BaasUser.withUserName(username).unfollow(new BaasHandler<BaasUser>() {
            @Override
            public void handle(BaasResult<BaasUser> baasResult) {
                UserBus.RemoveConnectionResult removeResult = new UserBus.RemoveConnectionResult();
                if (baasResult.isSuccess()) {
                    removeResult.error = false;
                    removeResult.username = username;
                } else {
                    Log.i(TAG, baasResult.error().toString());
                    removeResult.error = true;
                }
                EventBus.getDefault().post(removeResult);
            }
        });
    }

    /**
     * Change user password
     *
     * @param newPassword new password to be set
     */
    public static void changePassword(String newPassword) {
        BaasUser user = BaasUser.current();
        user.changePassword(newPassword, new BaasHandler<Void>() {
            @Override
            public void handle(BaasResult<Void> baasResult) {
                UserBus.PasswordChangeResult passwordResult = new UserBus.PasswordChangeResult();
                passwordResult.error = !baasResult.isSuccess();
                EventBus.getDefault().post(passwordResult);

            }
        });
    }

    public static RequestToken getAllUsers() {
        BaasQuery.Criteria filter = BaasQuery.builder().orderBy("user.name").criteria();
        return BaasUser.fetchAll(filter, new BaasHandler<List<BaasUser>>() {
            @Override
            public void handle(BaasResult<List<BaasUser>> baasResult) {
                UserBus.GetAllUsersResult usersResult = new UserBus.GetAllUsersResult();
                if (baasResult.isSuccess()) {
                    usersResult.error = false;
                    usersResult.users = baasResult.value();
                } else {
                    usersResult.error = true;
                    Log.i(TAG, baasResult.error().getMessage());
                }

                EventBus.getDefault().post(usersResult);
            }
        });
    }

    public static void getFollowing() {
        if (BaasUser.current() != null) {
            BaasUser.current().following(new BaasHandler<List<BaasUser>>() {
                @Override
                public void handle(BaasResult<List<BaasUser>> baasResult) {
                    if (baasResult.isSuccess()) {
                        UserBus.GetFollowingResult result = new UserBus.GetFollowingResult();
                        result.error = false;
                        result.users = baasResult.value();
                        EventBus.getDefault().post(result);
                    } else {
                        Log.i(TAG, "FAILED TO FETCH FOLLOWING");
                    }
                }
            });
        }
    }

    public static void getFollowers() {
        if (BaasUser.current() != null) {
            BaasUser.current().followers(new BaasHandler<List<BaasUser>>() {
                @Override
                public void handle(BaasResult<List<BaasUser>> baasResult) {
                    if (baasResult.isSuccess()) {
                        UserBus.GetFollowersResult result = new UserBus.GetFollowersResult();
                        result.error = false;
                        result.users = baasResult.value();
                        EventBus.getDefault().post(result);
                    } else {
                        Log.i(TAG, "FAILED TO FETCH FOLLOWERS");
                    }
                }
            });
        }
    }

    public static void refreshProfile() {
        BaasUser.current().refresh(new BaasHandler<BaasUser>() {
            @Override
            public void handle(BaasResult<BaasUser> baasResult) {
                if (baasResult.isSuccess()) {
                    Log.i(TAG, "PROFILE REFRESHED SUCCESSFULLY");
                } else {
                    Log.e(TAG, "FAILED TO REFRESH PROFILE");
                }
            }
        });
    }
}
