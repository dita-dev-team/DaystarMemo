package com.dev.dita.daystarmemo.ui.documents;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.text.format.Formatter;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.dev.dita.daystarmemo.R;
import com.dev.dita.daystarmemo.controller.utils.FileUtils;
import com.dev.dita.daystarmemo.model.database.Connection;
import com.dev.dita.daystarmemo.model.database.Memo;
import com.dev.dita.daystarmemo.model.objects.Recipient;
import com.dev.dita.daystarmemo.services.TransfersService;
import com.dev.dita.daystarmemo.ui.customviews.RecipientsCompletionView;
import com.tokenautocomplete.TokenCompleteTextView;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.OrderedRealmCollection;
import io.realm.Realm;
import io.realm.RealmRecyclerViewAdapter;
import io.realm.RealmResults;

public class DocumentsAdapter extends RealmRecyclerViewAdapter<Memo, DocumentsAdapter.ViewHolder> {

    public final static int SENT = 0;
    public final static int RECEIVED = 1;
    public int adapterType;
    OnItemClickListener itemClickListener;

    public DocumentsAdapter(Context context, OrderedRealmCollection<Memo> data, boolean autoUpdate) {
        super(context, data, autoUpdate);
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.documents_grid_item, parent, false);
        final ViewHolder viewHolder = new ViewHolder(view);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemClickListener.onItemClick(view, viewHolder.getLayoutPosition());
            }
        });
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Memo memo = getItem(position);
        File file = new File(memo.filepath);
        holder.headerTitle.setText(FileUtils.removeExtension(file.getName()));
        holder.subtitle.setText(DateUtils.formatDateTime(context, memo.date.getTime(), DateUtils.FORMAT_SHOW_TIME | DateUtils.FORMAT_SHOW_DATE));
        holder.thumbnail.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_file_document_blue_high));

        // Setup popup menu for each item
        final PopupMenu popupMenu = new PopupMenu(context, holder.popup);
        popupMenu.inflate(R.menu.menu_documents_popup);
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.documents_popup_info:
                        onInfo(memo);
                        return true;
                    case R.id.documents_popup_forward:
                        onForward(memo);
                        return true;
                    case R.id.documents_popup_delete:
                        onDelete(memo);
                        return true;
                    default:
                        return false;
                }
            }
        });
        holder.popup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popupMenu.show();
            }
        });
    }

    /**
     * Shows the details of the currently selected memo
     *
     * @param memo currently selected memo
     */
    private void onInfo(Memo memo) {

        File file = new File(memo.filepath);
        View view = View.inflate(context, R.layout.document_info_layout, null);
        if (file.exists()) {
            ScrollView scrollView = (ScrollView) view.findViewById(R.id.document_info_content);
            scrollView.setVisibility(View.VISIBLE);
            ImageView thumbnail = (ImageView) view.findViewById(R.id.document_info_thumbnail);
            TextView name = (TextView) view.findViewById(R.id.document_info_name);
            TextView type = (TextView) view.findViewById(R.id.document_info_type);
            TextView sender = (TextView) view.findViewById(R.id.document_info_sender);
            TextView size = (TextView) view.findViewById(R.id.document_info_size);
            TextView date = (TextView) view.findViewById(R.id.document_info_date);

            name.setText(file.getName());
            type.setText(FileUtils.getMimeType(FileUtils.removeWhitespace(file.getName())));
            sender.setText(memo.sentByMe ? "Me" : memo.connection);
            String fileSize = Formatter.formatShortFileSize(context, file.length());
            size.setText(fileSize);
            date.setText(DateUtils.formatDateTime(context, memo.date.getTime(), DateUtils.FORMAT_SHOW_TIME | DateUtils.FORMAT_SHOW_DATE));
            thumbnail.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_file_document_blue_high));
        } else {
            TextView noContent = (TextView) view.findViewById(R.id.document_info_no_content);
            noContent.setVisibility(View.VISIBLE);
        }

        final AlertDialog dialog = new AlertDialog.Builder(context)
                .setView(view)
                .create();

        Button cancelButton = (Button) view.findViewById(R.id.document_info_cancel_button);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    /**
     * Deletes the currently selected memo
     *
     * @param memo current memo object
     */
    private void onDelete(final Memo memo) {
        new AlertDialog.Builder(context)
                .setTitle("Delete document")
                .setMessage("Are you sure?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Memo.deleteMemoById(memo.id);
                        Toast.makeText(context, "Document deleted successfully", Toast.LENGTH_SHORT).show();
                    }
                })
                .setNegativeButton("No", null)
                .show();
    }

    private void onForward(Memo memo) {
        final File file = new File(memo.filepath);
        View view = View.inflate(context, R.layout.document_forward_layout, null);
        if (!file.exists()) {
            Toast.makeText(context, "File does not exist", Toast.LENGTH_SHORT).show();
            return;
        }

        // Load all users from database
        Realm realm = Realm.getDefaultInstance();
        RealmResults<Connection> connections = realm.where(Connection.class).findAll();
        ArrayList<Recipient> recipientsList = new ArrayList<>();
        for (Connection connection : connections) {
            recipientsList.add(new Recipient(connection.username, connection.name));
        }
        final RecipientsCompletionView completionView = (RecipientsCompletionView) view.findViewById(R.id.document_forward_recipient);
        completionView.performBestGuess(false);
        completionView.setAdapter(new ArrayAdapter<>(context, android.R.layout.simple_list_item_1, recipientsList));

        final Button forwardButton = (Button) view.findViewById(R.id.document_forward_forward_button);
        Button cancelButton = (Button) view.findViewById(R.id.document_forward_cancel_button);

        final AlertDialog dialog = new AlertDialog.Builder(context)
                .setView(view)
                .create();
        forwardButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                List<Recipient> recipientsList = completionView.getObjects();
                String[] usernames = new String[recipientsList.size()];
                for (int i = 0; i < recipientsList.size(); i++) {
                    usernames[i] = recipientsList.get(i).username;
                }

                TransfersService.addUpload(file, true, usernames);
                dialog.dismiss();
            }
        });
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        completionView.setTokenListener(new TokenCompleteTextView.TokenListener<Recipient>() {
            @Override
            public void onTokenAdded(Recipient token) {
                forwardButton.setEnabled(true);
            }

            @Override
            public void onTokenRemoved(Recipient token) {
                if (completionView.getObjects().size() == 0) {
                    forwardButton.setEnabled(false);
                }
            }
        });

        dialog.show();
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        itemClickListener = listener;
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.documents_grid_item_thumbnail)
        public ImageView thumbnail;
        @BindView(R.id.documents_grid_item_header)
        public TextView headerTitle;
        @BindView(R.id.documents_grid_item_info)
        public TextView subtitle;
        @BindView(R.id.documents_grid_item_menu)
        ImageButton popup;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
