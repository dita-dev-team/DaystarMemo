package com.dev.dita.daystarmemo.ui.memosgroups.memos;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.format.DateUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dev.dita.daystarmemo.R;
import com.dev.dita.daystarmemo.controller.utils.ImageUtils;
import com.dev.dita.daystarmemo.model.database.Connection;
import com.dev.dita.daystarmemo.model.database.Memo;

import java.lang.ref.WeakReference;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import hani.momanii.supernova_emoji_library.Helper.EmojiconTextView;
import io.realm.OrderedRealmCollection;
import io.realm.Realm;
import io.realm.RealmRecyclerViewAdapter;

/**
 * The type Memos list adapter.
 */
public class MemosListAdapter extends RealmRecyclerViewAdapter<Memo, MemosListAdapter.ViewHolder> {

    int color;
    int defaultColor;
    OnItemClickListener itemClickListener;
    OnItemLongClickListener itemLongClickListener;

    public MemosListAdapter(Context context, OrderedRealmCollection<Memo> data, boolean autoUpdate) {
        super(context, data, autoUpdate);

        color = ContextCompat.getColor(context, R.color.baseColor1);
        defaultColor = Color.parseColor("#c4c4c4");
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.memos_list_item, parent, false);
        final ViewHolder viewHolder = new ViewHolder(view);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemClickListener.onItemClick(view, viewHolder.getLayoutPosition());
            }
        });
        view.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                return itemLongClickListener.onItemLongClick(view, viewHolder.getLayoutPosition());
            }
        });
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Memo memo = getItem(position);
        String username = memo.connection;
        String body = memo.body;
        body = memo.sentByMe ? "<b>You</b>: " + body : body;
        //String date = (String) DateUtils.getRelativeTimeSpanString(memo.date.getTime(), NOW.getTime(), DateUtils.DAY_IN_MILLIS);
        String date;
        if (DateUtils.isToday(memo.date.getTime())) {
            date = DateUtils.formatDateTime(context, memo.date.getTime(), DateUtils.FORMAT_SHOW_TIME);
        } else if (com.dev.dita.daystarmemo.controller.utils.DateUtils.isYesterday(memo.date)) {
            date = context.getString(R.string.yesterday);
        } else {
            date = DateUtils.formatDateTime(context, memo.date.getTime(), DateUtils.FORMAT_SHOW_DATE);
        }
        // Truncate body text if its long than the max width
       /* int maxWidth = holder..getWidth();
        TextPaint textPaint = holder.body.getPaint();
        int chars = textPaint.breakText(body, true, maxWidth, null);
        if (chars < body.length()) {
            body = body.substring(0, chars);
        }*/

        holder.username.setText(username);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            holder.body.setText(Html.escapeHtml(body));
        }
        {
            holder.body.setText(Html.fromHtml(body));
        }
        holder.date.setText(date);

        // Set color of row based on memo status
        if (memo.status == Memo.UNREAD) {
            holder.date.setTextColor(color);
            holder.body.setTextColor(color);
        } else {
            holder.date.setTextColor(defaultColor);
            holder.body.setTextColor(defaultColor);
        }
        if (holder.thumbnail != null) {
            new ImageLoaderTask(holder.thumbnail, memo.connection).execute();
        }
    }

    public void setOnItemClickListener(OnItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    public void setOnItemLongClickListener(OnItemLongClickListener itemLongClickListener) {
        this.itemLongClickListener = itemLongClickListener;
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    public interface OnItemLongClickListener {
        boolean onItemLongClick(View view, int position);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.username)
        public TextView username;
        @BindView(R.id.details)
        public EmojiconTextView body;
        @BindView(R.id.date)
        public TextView date;
        @BindView(R.id.thumbnail)
        public CircleImageView thumbnail;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    class ImageLoaderTask extends AsyncTask<Void, Void, Bitmap> {
        private WeakReference<CircleImageView> circleImageViewWeakReference;
        private String connection;

        public ImageLoaderTask(CircleImageView circleImageView, String connection) {
            circleImageViewWeakReference = new WeakReference<>(circleImageView);
            this.connection = connection;
        }

        @Override
        protected Bitmap doInBackground(Void... voids) {
            Realm realm = Realm.getDefaultInstance();
            Connection connection = realm.where(Connection.class).equalTo("username", this.connection).findFirst();
            Bitmap bitmap = null;
            if (connection != null) {
                if (connection.image != null) {
                    bitmap = ImageUtils.decodeBitmapFromByteArray(connection.image);
                }
            }

            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            if (circleImageViewWeakReference != null) {
                CircleImageView circleImageView = circleImageViewWeakReference.get();
                if (circleImageView != null) {
                    if (bitmap != null) {
                        circleImageView.setImageBitmap(bitmap);
                    }
                }
            }
        }
    }
}
