package com.dev.dita.daystarmemo;

import android.app.Application;
import android.content.Context;

import com.baasbox.android.BaasBox;
import com.crashlytics.android.Crashlytics;
import com.dev.dita.daystarmemo.controller.utils.OkClient;
import com.dev.dita.daystarmemo.model.database.DaystarMemoMigration;

import io.fabric.sdk.android.Fabric;
import io.realm.Realm;
import io.realm.RealmConfiguration;

public class MemoApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        // Initialize baasbox
        BaasBox.builder(this).setAuthentication(BaasBox.Config.AuthType.SESSION_TOKEN)
                .setApiDomain(BuildConfig.HOST)
                .setPort(BuildConfig.PORT)
                .setAppCode(BuildConfig.APP_KEY)
                .setSessionTokenExpires(false)
                .setAuthentication(BaasBox.Config.AuthType.SESSION_TOKEN)
                .setRestClient(new OkClient())
                .setWorkerThreads(4)
                .init();

        // Initialize database
        Realm.init(this);
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder()
                .schemaVersion(4)
                .migration(new DaystarMemoMigration())
                .build();
        //Realm.deleteRealm(realmConfiguration);
        Realm.setDefaultConfiguration(realmConfiguration);

        //Data data = new Data(getApplicationContext());
        //data.fillData();
        //startService(new Intent(this, SheduleService.class));
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
    }
}
