/*
 * Copyright (C) 2014.
 *
 * BaasBox - info@baasbox.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 */

package com.dev.dita.daystarmemo.controller;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.text.Html;
import android.text.TextUtils;

import com.dev.dita.daystarmemo.PrefSettings;
import com.dev.dita.daystarmemo.R;
import com.dev.dita.daystarmemo.receivers.NotificationRemovedReceiver;
import com.dev.dita.daystarmemo.ui.memosgroups.MemoGroupActivity;
import com.dev.dita.daystarmemo.ui.memosgroups.memos.MemosChatActivity;

import java.util.ArrayList;

public class DaystarMemoNotification {
    public static final ArrayList<String> sendersList = new ArrayList<>();
    public static final ArrayList<com.dev.dita.daystarmemo.model.objects.Notification> notifications = new ArrayList<>();
    public static final String PROGRESS_NOTIFICATION_TAG = "Progress";
    /**
     * The unique identifier for this type of notification.
     */
    private static final String NOTIFICATION_TAG = "NewMessage";
    public static int memoNotificationsNo = 0;
    private static String TAG = DaystarMemoNotification.class.getName();

    public static void notify(final Context context, final String memoTitle,
                              final String memoContent) {
        final Resources res = context.getResources();



        final NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                //.setDefaults(Notification.DEFAULT_ALL)
                .setSound(getRingtone(context))
                .setSmallIcon(R.drawable.ic_notification_memo)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setAutoCancel(true);


        if (!sendersList.contains(memoTitle)) {
            sendersList.add(memoTitle);
        }

        com.dev.dita.daystarmemo.model.objects.Notification notification = new com.dev.dita.daystarmemo.model.objects.Notification();
        notification.sender = memoTitle;
        notification.message = memoContent;
        notifications.add(notification);

        NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();

        if (memoNotificationsNo == 0) {
            builder.setContentTitle(memoTitle);
            builder.setContentText(memoContent);
            inboxStyle.setBigContentTitle(memoTitle)
                    .addLine(memoContent)
                    .setSummaryText("Daystar Memo");
            memoNotificationsNo++;
        } else {
            memoNotificationsNo++;
            builder.setNumber(memoNotificationsNo);
            for (com.dev.dita.daystarmemo.model.objects.Notification n : notifications) {
                String line = "<b>" + n.sender + "</b> " + n.message;
                inboxStyle.setBigContentTitle("Daystar Memo")
                        .addLine(Html.fromHtml(line));
            }
            builder.setContentTitle("Daystar Memo");


            if (sendersList.size() == 1) {
                String summary = "You have " + String.valueOf(memoNotificationsNo) + " new memos";
                inboxStyle.setSummaryText(summary);
                builder.setContentText(summary);
            } else {
                String summary = String.valueOf(memoNotificationsNo) + " memos from " + String.valueOf(sendersList.size()) + " chats";
                inboxStyle.setSummaryText(summary);
                builder.setContentText(summary);
            }
        }
        builder.setStyle(inboxStyle);

        Intent intent;
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        if (sendersList.size() > 1) {
            intent = new Intent(context, MemoGroupActivity.class);
            stackBuilder.addParentStack(MemoGroupActivity.class);
        } else {
            intent = new Intent(context, MemosChatActivity.class);
            intent.putExtra("username", memoTitle);
            stackBuilder.addParentStack(MemosChatActivity.class);
        }
        stackBuilder.addNextIntent(intent);
        PendingIntent memosPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(memosPendingIntent);

        Intent removedIntent = new Intent(context, NotificationRemovedReceiver.class);
        PendingIntent removedPendingIntent = PendingIntent.getBroadcast(context, 0, removedIntent, PendingIntent.FLAG_CANCEL_CURRENT);
        builder.setDeleteIntent(removedPendingIntent);

        notify(context, builder.build());
    }

    @TargetApi(Build.VERSION_CODES.ECLAIR)
    private static void notify(final Context context, final Notification notification) {
        final NotificationManager nm = (NotificationManager) context
                .getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ECLAIR) {
            nm.notify(NOTIFICATION_TAG, 0, notification);
        } else {
            nm.notify(NOTIFICATION_TAG.hashCode(), notification);
        }
    }

    /**
     * Cancels any notifications of this type previously shown using
     *
     */
    @TargetApi(Build.VERSION_CODES.ECLAIR)
    public static void cancel(final Context context) {
        final NotificationManager nm = (NotificationManager) context
                .getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ECLAIR) {
            nm.cancel(NOTIFICATION_TAG, 0);
        } else {
            nm.cancel(NOTIFICATION_TAG.hashCode());
        }
    }

    private static Uri getRingtone(Context context) {
        String ringtone = PrefSettings.getRingtone(context);
        if (TextUtils.isEmpty(ringtone)) {
            return RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        } else {
            return Uri.parse(ringtone);
        }
    }

    public static void notifyProgress(Context context, String title) {
        final NotificationManager nm = (NotificationManager) context
                .getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                .setContentTitle(title)
                .setSmallIcon(R.drawable.ic_notification_memo)
                .setProgress(0, 0, true)
                .setOngoing(true)
                .setAutoCancel(true);

        nm.notify(PROGRESS_NOTIFICATION_TAG, 1, builder.build());
    }

    public static void cancelProgress(Context context) {
        final NotificationManager nm = (NotificationManager) context
                .getSystemService(Context.NOTIFICATION_SERVICE);
        nm.cancel(PROGRESS_NOTIFICATION_TAG, 1);
    }
}