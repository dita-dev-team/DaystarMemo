package com.dev.dita.daystarmemo.model.objects;

import com.baasbox.android.BaasDocument;

import java.io.Serializable;
import java.util.List;


public class GroupContainer implements Serializable {
    public List<BaasDocument> groups;


    public GroupContainer(List<BaasDocument> groups) {
        this.groups = groups;
    }
}
