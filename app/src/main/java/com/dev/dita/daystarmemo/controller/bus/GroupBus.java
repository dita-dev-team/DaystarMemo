package com.dev.dita.daystarmemo.controller.bus;


import com.baasbox.android.BaasDocument;
import com.baasbox.android.json.JsonObject;

import java.util.List;

public class GroupBus {
    public static class GetAllGroupsResult {
        public List<BaasDocument> groups;
        public boolean error;
        public String message;

        public GetAllGroupsResult(List<BaasDocument> groups, boolean error, String message) {
            this.groups = groups;
            this.error = error;
            this.message = message;
        }
    }

    public static class CreateGroupResult {
        public boolean error;
        public JsonObject object;

        public CreateGroupResult(boolean error) {
            this.error = error;
        }

        public CreateGroupResult(JsonObject object, boolean error) {
            this.object = object;
            this.error = error;
        }
    }

    public static class JoinGroupResult {
        public boolean error;
        public BaasDocument document;

        public JoinGroupResult(boolean error, BaasDocument document) {
            this.error = error;
            this.document = document;
        }
    }

    public static class LeaveGroupResult {
        public boolean error;
        public BaasDocument document;

        public LeaveGroupResult(boolean error, BaasDocument document) {
            this.error = error;
            this.document = document;
        }
    }

    public static class SaveGroupResult {
    }
}
