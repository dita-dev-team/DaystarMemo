package com.dev.dita.daystarmemo.services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import com.dev.dita.daystarmemo.controller.bus.MemoBus;
import com.dev.dita.daystarmemo.model.baas.MemoBaas;
import com.dev.dita.daystarmemo.model.database.MemoPendingDownload;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

/**
 * The type Memo pending download service. Downloads memos from remote server if there queue is
 * not empty
 */
public class MemoPendingDownloadService extends Service {

    public static boolean isRunning = false;

    @Override
    public void onCreate() {
        super.onCreate();
        EventBus.getDefault().register(this);
        Log.i("PendingDownloadService", "CREATING SERVICE");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i("PendingDownloadService", "STARTING SERVICE");
        isRunning = true;
        if (!MemoPendingDownload.isEmpty()) {
            Log.i("PendingDownloadService", "LIST IS NOT EMPTY");
            MemoBaas.fetchMemo(MemoPendingDownload.pop());
        } else {
            Log.i("PendingDownloadService", "LIST IS EMPTY");
            stopSelf();
        }

        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        isRunning = false;
        EventBus.getDefault().unregister(this);
    }

    @Subscribe
    public void onEvent(MemoBus.FetchMemoResult result) {
        if (!MemoPendingDownload.isEmpty()) {
            MemoBaas.fetchMemo(MemoPendingDownload.pop());
        } else {
            stopSelf();
        }
    }
}
