package com.dev.dita.daystarmemo.model.database;

import java.util.Date;

import io.realm.Realm;
import io.realm.RealmObject;


/**
 * The type Memo pending download. Simulates a queue data structure where items are sorted
 * according to date in ascending order
 */
public class MemoPendingDownload extends RealmObject {
    public String id = null;
    public Date date = null;

    public static void add(final String id) {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                MemoPendingDownload memo = new MemoPendingDownload();
                memo.id = id;
                memo.date = new Date();
                realm.copyToRealm(memo);
            }
        });
    }

    /**
     * Remove first element from the queue
     *
     * @return the string
     */
    public static String pop() {
        Realm realm = Realm.getDefaultInstance();
        MemoPendingDownload memo = realm.where(MemoPendingDownload.class).findAllSorted("date").first();
        String id = memo.id;
        realm.beginTransaction();
        memo.deleteFromRealm();
        realm.commitTransaction();
        return id;
    }

    public static boolean isEmpty() {
        Realm realm = Realm.getDefaultInstance();
        return realm.where(MemoPendingDownload.class).findAll().isEmpty();
    }
}
