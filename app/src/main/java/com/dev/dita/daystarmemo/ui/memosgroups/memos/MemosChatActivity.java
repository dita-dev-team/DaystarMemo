package com.dev.dita.daystarmemo.ui.memosgroups.memos;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import com.dev.dita.daystarmemo.R;
import com.dev.dita.daystarmemo.controller.bus.MemoBus;
import com.dev.dita.daystarmemo.controller.utils.ConnectionUtils;
import com.dev.dita.daystarmemo.controller.utils.ImageUtils;
import com.dev.dita.daystarmemo.controller.utils.UIUtils;
import com.dev.dita.daystarmemo.model.database.Connection;
import com.dev.dita.daystarmemo.model.database.Memo;
import com.dev.dita.daystarmemo.model.objects.Notification;
import com.dev.dita.daystarmemo.services.MemoDownloadService;
import com.dev.dita.daystarmemo.services.MemoService;
import com.dev.dita.daystarmemo.ui.connections.ConnectionDetailsActivity;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import hani.momanii.supernova_emoji_library.Actions.EmojIconActions;
import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;

/**
 * The type Memos chat activity.
 */
public class MemosChatActivity extends AppCompatActivity {

    final String TAG = getClass().getName();

    /**
     * The List view.
     */
    @BindView(R.id.memos_chat_list_view)
    RecyclerView listView;
    /**
     * The Edit text.
     */
    @BindView(R.id.memos_chat_edit)
    EmojiconEditText editText;
    /**
     * The Emoji button.
     */
    @BindView(R.id.memos_chat_emoji_button)
    ImageButton emojiButton;
    /**
     * The Send button.
     */
    @BindView(R.id.memos_chat_send_button)
    ImageButton sendButton;
    /**
     * The Root view.
     */
    @BindView(R.id.memos_chat_edit_view)
    View rootView;

    private Realm realm;
    private RealmResults<Memo> memos;
    private MemosChatListAdapter adapter;

    private String username;
    private RealmChangeListener<RealmResults<Memo>> listener = new RealmChangeListener<RealmResults<Memo>>() {
        @Override
        public void onChange(RealmResults<Memo> element) {
            Log.i(TAG, "SIZE: " + element.size());
            adapter.updateData(element);
        }
    };

    private ActionMode actionMode;
    private ActionModeCallback actionModeCallback = new ActionModeCallback();

    private BroadcastReceiver notificationReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Intent memo = new Intent(context, MemoDownloadService.class);
            Notification notification = (Notification) intent.getExtras().getSerializable("notification");
            if (notification.type == Notification.MESSAGE) {
                memo.putExtra("remoteId", notification.id);
                context.startService(memo);
                abortBroadcast();
                MediaPlayer mediaPlayer = MediaPlayer.create(MemosChatActivity.this, R.raw.incoming_memo_chat);
                mediaPlayer.start();
            }
        }
    };

    /**
     * Init.
     */
    public void init() {
        IntentFilter filter = new IntentFilter("com.dev.dita.daystarmemo.BROADCAST_NOTIFICATION");
        filter.setPriority(1);
        registerReceiver(notificationReceiver, filter);

        realm = Realm.getDefaultInstance();

        // Get the user for the chat
        Bundle extras = getIntent().getExtras();
        username = null;
        if (extras != null) {
            username = extras.getString("username");
        }
        // fetch all memos for this user
        memos = realm.where(Memo.class).equalTo("connection", username).equalTo("isFile", false).findAllSortedAsync("date");
        memos.addChangeListener(listener);
        adapter = new MemosChatListAdapter(this, null, true);
        adapter.setClickListener(new MemosChatListAdapter.ClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                if (actionMode != null) {
                    toggleSelection(position);
                }
            }

            @Override
            public boolean onItemLongClick(View view, int position) {
                if (actionMode == null) {
                    actionMode = startActionMode(actionModeCallback);
                }

                toggleSelection(position);
                return true;
            }
        });
        // fetch connection
        final Connection connection = realm.where(Connection.class).equalTo("username", username).findFirstAsync();
        connection.addChangeListener(new RealmChangeListener<Connection>() {
            @Override
            public void onChange(Connection element) {
                if (element.image != null) {
                    adapter.setImage(ImageUtils.decodeBitmapFromByteArray(element.image));
                }
            }
        });
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        layoutManager.setStackFromEnd(true);
        listView.setLayoutManager(layoutManager);
        listView.setAdapter(adapter);
        setTitle(username);

        // Change memo status to read as soon as chat is opened
        Memo.markAsReadByUsername(username);

        // Initialize emojis
        EmojIconActions actions = new EmojIconActions(this, rootView, editText, emojiButton);
        actions.ShowEmojIcon();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_memos_chat);
        ButterKnife.bind(this);
        EventBus.getDefault().register(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        startService(new Intent(this, MemoService.class));
        init();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_memos_chat, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_file:
                return true;
            case R.id.action_info:
                Intent intent = new Intent(this, ConnectionDetailsActivity.class);
                intent.putExtra("username", username);
                startActivity(intent);
                return true;
            case R.id.action_memo_clear:
                return true;
            default:
                return false;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        init();
    }

    @Override
    protected void onPause() {
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        unregisterReceiver(notificationReceiver);
        super.onPause();
    }


    @Override
    protected void onDestroy() {
        memos.removeChangeListeners();
        realm.close();
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        UIUtils.hideKeyboard(this);
        NavUtils.navigateUpFromSameTask(this);
    }

    /**
     * On send button clicked.
     */
    @OnClick(R.id.memos_chat_send_button)
    public void onSendButtonClicked() {
        if (!TextUtils.isEmpty(editText.getText().toString())) {
            if (!ConnectionUtils.isNetworkAvailable(this)) {
                Toast.makeText(this, R.string.cant_connect, Toast.LENGTH_SHORT).show();
                return;
            }
            String text = editText.getText().toString().trim();
            String[] recipient = {
                    username
            };
            MemoService.sendMemo(text, true, recipient);
            editText.getText().clear();
        }
    }

    @Subscribe
    public void onEvent(MemoBus.SendMemoResult result) {
        if (result.error) {
            if (editText.getText().toString().isEmpty()) {
                editText.setText(result.memo.body);
            }
            Memo.deleteMemoById(result.memo.id);
            Toast.makeText(this, R.string.memo_failed, Toast.LENGTH_SHORT).show();
        } else {
            MediaPlayer mediaPlayer = MediaPlayer.create(this, R.raw.outgoing_memo_chat);
            mediaPlayer.start();
        }
    }

    @Subscribe
    public void onEvent(MemoBus.ScrollToBottomEvent event) {
        listView.scrollToPosition(adapter.getItemCount() - 1);
    }

    private void toggleSelection(int position) {
        adapter.toggleSelection(position);
        int count = adapter.getSelectedItemCount();

        if (count == 0) {
            actionMode.finish();
        } else {
            actionMode.setTitle(String.valueOf(count));
            actionMode.invalidate();
        }
    }

    private class ActionModeCallback implements ActionMode.Callback {

        @Override
        public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
            actionMode.getMenuInflater().inflate(R.menu.memos_chat_contextual_menu, menu);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(final ActionMode mode, MenuItem menuItem) {
            int itemId = menuItem.getItemId();

            switch (itemId) {
                case R.id.action_delete:
                    String message;
                    int count = adapter.getSelectedItemCount();
                    if (count == 1) {
                        message = getString(R.string.delete_message_from, username);
                    } else {
                        message = getString(R.string.delete_multiple_messages_from, count, username);
                    }

                    new AlertDialog.Builder(MemosChatActivity.this)
                            .setMessage(message)
                            .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    adapter.removeItems(adapter.getSelectedItems());
                                    mode.finish();
                                }
                            })
                            .setNegativeButton(R.string.no, null)
                            .show();

                    return true;
                case R.id.action_copy:
                    adapter.copySelectedItems(adapter.getSelectedItems());
                    mode.finish();
                    return true;
                default:
                    return false;
            }
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            adapter.clearSelection();
            actionMode = null;
        }
    }

}
