package com.dev.dita.daystarmemo.controller.bus;


import com.baasbox.android.BaasUser;

import java.util.List;

/**
 * A class containing all classes used by the EventBus when dealing with a user
 */
public class UserBus {
    public static class LoginEvent {
        public String username;
        public String password;

        public LoginEvent(String username, String password) {
            this.username = username;
            this.password = password;
        }

        @Override
        public String toString() {
            return "LoginBus{" +
                    "username='" + username + '\'' +
                    ", password='" + password + '\'' +
                    '}';
        }
    }

    public static class LoginResult {
        public Boolean error;
        public String message;
    }

    public static class LogoutResult {
        public Boolean error;
        public String message;
    }

    public static class RegisterEvent {
        public String username;
        public String email;
        public String password;

        public RegisterEvent(String username, String email, String password) {
            this.username = username;
            this.email = email;
            this.password = password;
        }

        @Override
        public String toString() {
            return "RegisterEvent{" +
                    "username='" + username + '\'' +
                    ", email='" + email + '\'' +
                    ", password='" + password + '\'' +
                    '}';
        }
    }

    public static class RegisterResult {
        public Boolean error;
        public String message;
    }

    public static class Notify {
    }

    public static class ProfileUpdatedEvent {
    }

    public static class ProfileUpdatedRemoteResult {
        public Boolean error;
        public String message;
    }

    public static class PasswordChangeResult {
        public Boolean error;
    }

    public static class GetAllUsersResult {
        public List<BaasUser> users;
        public Boolean error;
    }

    public static class AddConnectionToDbResult {
        public Boolean error;
    }

    public static class AddConnectionResult {
        public Boolean error;
        public BaasUser user;
    }

    public static class RemoveConnectionFromDbResult {
        public Boolean error;
    }

    public static class RemoveConnectionResult {
        public Boolean error;
        public String username;
    }

    public static class GetFollowingResult {
        public Boolean error;
        public List<BaasUser> users;
    }

    public static class GetFollowersResult {
        public boolean error;
        public List<BaasUser> users;
    }

    public static class ScheduleUpdated {
    }

    public static class ScheduleUpdatedOnProfile {
        public boolean error;
    }

    public static class DownloadSchedule {

    }

    public static class SaveSchedule {

    }
}
