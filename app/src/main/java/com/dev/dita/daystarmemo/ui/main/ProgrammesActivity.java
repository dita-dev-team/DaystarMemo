package com.dev.dita.daystarmemo.ui.main;

import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.widget.TextView;

import com.dev.dita.daystarmemo.R;

import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProgrammesActivity extends AppCompatActivity {

    private static final float PERCENTAGE_TO_SHOW_TITLE_AT_TOOLBAR = 0.9f;

    @BindView(R.id.app_bar)
    AppBarLayout appBarLayout;
    @BindView(R.id.collapsing_toolbar)
    CollapsingToolbarLayout collapsingToolbarLayout;
    @BindView(R.id.content_summary)
    TextView summary;
    @BindView(R.id.undergraduate_list)
    TextView undergraduateList;
    @BindView(R.id.posgraduate_list)
    TextView postgraduateList;

    private Toolbar toolbar;

    private void init() {
        List<String> postgraduate = Arrays.asList(getResources().getStringArray(R.array.postgraduate_programmes_list));
        List<String> undergraduate = Arrays.asList(getResources().getStringArray(R.array.undergraduate_programmes_list));
        StringBuilder builder = new StringBuilder();
        for (String programme : postgraduate) {
            builder.append(programme).append('\n');
        }
        postgraduateList.setText(builder.toString());
        builder = new StringBuilder();
        for (String programme : undergraduate) {
            builder.append(programme).append('\n');
        }
        undergraduateList.setText(builder.toString());
        CharSequence summaryText;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            summaryText = Html.fromHtml(getString(R.string.programmes_content_summary), Html.TO_HTML_PARAGRAPH_LINES_INDIVIDUAL);
        } else {
            summaryText = Html.fromHtml(getString(R.string.programmes_content_summary));
        }
        summary.setText(summaryText);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_programmes);
        ButterKnife.bind(this);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                int maxScroll = appBarLayout.getTotalScrollRange();
                float percentage = (float) Math.abs(verticalOffset) / (float) maxScroll;


                if (percentage >= PERCENTAGE_TO_SHOW_TITLE_AT_TOOLBAR) {
                    collapsingToolbarLayout.setTitle(getString(R.string.title_activity_programmes));
                    //mediaPlayer.pause();
                } else {
                    collapsingToolbarLayout.setTitle("");
                    //mediaPlayer.start();
                }
            }
        });
        init();
    }

}
