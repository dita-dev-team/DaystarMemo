package com.dev.dita.daystarmemo.ui.memosgroups.groups;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dev.dita.daystarmemo.R;
import com.dev.dita.daystarmemo.model.database.Group;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import hani.momanii.supernova_emoji_library.Helper.EmojiconTextView;
import io.realm.OrderedRealmCollection;
import io.realm.RealmRecyclerViewAdapter;

public class GroupsListAdapter extends RealmRecyclerViewAdapter<Group, GroupsListAdapter.ViewHolder> {

    public GroupsListAdapter(Context context, OrderedRealmCollection<Group> data, boolean autoUpdate) {
        super(context, data, autoUpdate);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.username)
        public TextView name;
        @BindView(R.id.details)
        public EmojiconTextView body;
        @BindView(R.id.date)
        public TextView date;
        @BindView(R.id.thumbnail)
        public CircleImageView thumbnail;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
