package com.dev.dita.daystarmemo.model.database;


import com.baasbox.android.BaasDocument;
import com.baasbox.android.json.JsonArray;
import com.dev.dita.daystarmemo.controller.bus.GroupBus;

import org.greenrobot.eventbus.EventBus;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Group extends RealmObject {
    public static final int ACADEMIC = 1;
    public static final int SCHOOL = 2;
    public static final int COMMITTEE = 3;
    public static final int OPEN = 4;
    public static final int CLOSED = 5;
    public static final int INFORMATIVE = 6;
    public static final int INTERACTIVE = 7;

    @PrimaryKey
    public String name;
    public int type;
    public int privacy;
    public int interaction;
    public RealmList<GroupMember> owners;
    public RealmList<GroupMember> members;

    public static void saveGroup(final BaasDocument document) {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                Group group = new Group();
                group.name = document.getString("name");
                group.type = stringToInt(document.getString("type"));
                group.privacy = stringToInt(document.getString("privacy"));
                group.interaction = stringToInt(document.getString("interaction"));
                JsonArray owners = document.getArray("owners", new JsonArray());
                JsonArray members = document.getArray("members", new JsonArray());
                for (Object object : owners) {
                    String username = (String) object;
                    group.owners.add(new GroupMember(username));
                }

                for (Object object : members) {
                    String username = (String) object;
                    group.members.add(new GroupMember(username));
                }

                realm.copyToRealmOrUpdate(group);
            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                EventBus.getDefault().post(new GroupBus.SaveGroupResult());
            }
        });
    }

    public static int stringToInt(String string) {
        if (string != null) {
            switch (string.toLowerCase()) {
                case "academic":
                    return ACADEMIC;
                case "school":
                    return SCHOOL;
                case "committee":
                    return COMMITTEE;
                case "open":
                    return OPEN;
                case "closed":
                    return CLOSED;
                case "informative":
                    return INFORMATIVE;
                case "interactive":
                    return INTERACTIVE;
            }
        }
        return -1;
    }

    public static String intToString(int integer) {
        if (integer > 0) {
            switch (integer) {
                case ACADEMIC:
                    return "academic";
                case SCHOOL:
                    return "school";
                case COMMITTEE:
                    return "committee";
                case OPEN:
                    return "open";
                case CLOSED:
                    return "closed";
                case INFORMATIVE:
                    return "informative";
                case INTERACTIVE:
                    return "interactive";
            }
        }
        return "";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Group group = (Group) o;

        if (type != group.type) return false;
        if (privacy != group.privacy) return false;
        if (interaction != group.interaction) return false;
        if (!name.equals(group.name)) return false;
        if (owners != null ? !owners.equals(group.owners) : group.owners != null) return false;
        return members != null ? members.equals(group.members) : group.members == null;

    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + type;
        result = 31 * result + privacy;
        result = 31 * result + interaction;
        result = 31 * result + (owners != null ? owners.hashCode() : 0);
        result = 31 * result + (members != null ? members.hashCode() : 0);
        return result;
    }
}
