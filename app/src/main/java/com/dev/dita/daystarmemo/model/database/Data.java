package com.dev.dita.daystarmemo.model.database;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.dev.dita.daystarmemo.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Random;

import io.realm.Realm;

public class Data {
    final int DATA_SIZE = 50;
    final String[] status = {
            "read",
            "unread"
    };
    final ArrayList<Connection> connections = new ArrayList<>();
    String src;

    public Data(Context context) {
        src = context.getResources().getString(R.string.large_text);
    }

    public void fillData() {
        Realm realm = Realm.getDefaultInstance();
        //realm.beginTransaction();
        //realm.deleteAll();
        //realm.commitTransaction();
        if (realm.where(Connection.class).count() == 0) {
            for (int i = 0; i < 15; i++) {
                String number = Integer.toString(i + 1);
                String name = "Default" + number;
                realm.beginTransaction();
                Connection connection = new Connection();
                connection.username = name;
                connection.name = name;
                connection = realm.copyToRealm(connection);
                realm.commitTransaction();
                connections.add(connection);
            }
        }

        long count = realm.where(Memo.class).count();
        if (count < DATA_SIZE) {
            Log.i("TAG", String.valueOf(count));
            int size = (int) (DATA_SIZE - count);
            String[] words = src.split(" ");
            List<String> sentences = new ArrayList<>(Arrays.asList(src.split("\n")));
            for (int i = 0; i < sentences.size(); i++) {
                String s = sentences.get(i).trim();
                if (TextUtils.isEmpty(s)) {
                    sentences.remove(i);
                }
            }
            Random rand = new Random();

            for (int i = 0; i < size; i++) {
                String word = words[rand.nextInt(words.length)];
                String sentence = sentences.get(rand.nextInt(sentences.size())).trim();
                Connection connection = connections.get(rand.nextInt(connections.size()));
                if (rand.nextBoolean()) {
                    sendMemo(realm, word, sentence, connection);
                } else {
                    receiveMemo(realm, word, sentence, connection);
                }
            }
            count = realm.where(Memo.class).count();
            Log.i("TAG", String.valueOf(count));
        }
        realm.close();
    }

    public void sendMemo(Realm realm, String subject, String body, Connection connection) {
        realm.beginTransaction();
        Memo memo = new Memo();
        memo.sentByMe = true;
        memo.connection = connection.username;
        memo.latest = true;
        memo.subject = subject;
        memo.body = body;
        //memo.status = "toBeSent";
        memo.date = new Date();
        //memo.connection.memos.add(memo);
        realm.commitTransaction();
    }

    public void receiveMemo(Realm realm, String subject, String body, Connection connection) {
        realm.beginTransaction();
        Memo memo = new Memo();
        memo.sentByMe = false;
        memo.connection = connection.username;
        memo.latest = true;
        memo.subject = subject;
        memo.body = body;
        //memo.status = status[new Random().nextInt(status.length)];
        memo.date = new Date();
        //memo.connection.memos.add(memo);
        realm.commitTransaction();
    }

}
