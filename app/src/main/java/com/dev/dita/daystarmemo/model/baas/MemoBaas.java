package com.dev.dita.daystarmemo.model.baas;


import android.content.Context;
import android.util.Log;

import com.baasbox.android.BaasACL;
import com.baasbox.android.BaasBox;
import com.baasbox.android.BaasCloudMessagingService;
import com.baasbox.android.BaasDocument;
import com.baasbox.android.BaasFile;
import com.baasbox.android.BaasHandler;
import com.baasbox.android.BaasResult;
import com.baasbox.android.BaasUser;
import com.baasbox.android.DataStreamHandler;
import com.baasbox.android.Grant;
import com.baasbox.android.Rest;
import com.baasbox.android.json.JsonArray;
import com.baasbox.android.json.JsonObject;
import com.crashlytics.android.Crashlytics;
import com.dev.dita.daystarmemo.controller.DaystarMemoNotification;
import com.dev.dita.daystarmemo.controller.bus.MemoBus;
import com.dev.dita.daystarmemo.controller.utils.DateUtils;
import com.dev.dita.daystarmemo.controller.utils.FileUtils;
import com.dev.dita.daystarmemo.controller.utils.TransferManager;
import com.dev.dita.daystarmemo.model.database.Memo;
import com.dev.dita.daystarmemo.model.database.MemoPendingDownload;
import com.dev.dita.daystarmemo.model.database.Schedule;
import com.dev.dita.daystarmemo.model.database.Transfer;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.io.FileOutputStream;
import java.text.ParseException;
import java.text.ParsePosition;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import io.realm.internal.android.ISO8601Utils;

public class MemoBaas {
    /**
     * The constant TAG.
     */
    private static final String TAG = MemoBaas.class.getName();

    public static void sendMemo(final Memo memo, final String username) {
        JsonObject object = new JsonObject();
        JsonArray recipients = new JsonArray();
        recipients.add(username);
        object.put("sender", BaasUser.current().getName());
        object.put("body", memo.body);
        object.put("recipients", recipients);
        BaasBox.rest().async(Rest.Method.POST, "plugin/memos.script", object, new BaasHandler<JsonObject>() {
            @Override
            public void handle(BaasResult<JsonObject> baasResult) {
                MemoBus.SendMemoResult result = new MemoBus.SendMemoResult();
                if (baasResult.isSuccess()) {
                    JsonObject data = baasResult.value().getObject("data");
                    memo.remoteId = data.getString("id");
                    Date date;
                    try {
                        date = ISO8601Utils.parse(data.getString("_creation_date"), new ParsePosition(0));
                    } catch (ParseException e) {
                        Crashlytics.logException(e);
                        date = null;
                    }
                    memo.date = date;
                    memo.isFile = false;
                    memo.sentByMe = true;
                    memo.status = Memo.SENT;
                    memo.connection = username;
                    result.memo = memo;
                    result.error = false;
                } else {
                    Log.i(TAG, "FAILED TO SEND MEMO");
                    Log.i(TAG, baasResult.error().toString());
                    result.error = true;
                    result.memo = memo;
                }
                EventBus.getDefault().post(result);
            }
        });
    }

    public static void sendMemo(final Memo memo, final String... usernames) {
        JsonObject object = new JsonObject();
        JsonArray recipients = JsonArray.of(usernames);
        object.put("sender", BaasUser.current().getName());
        object.put("body", memo.body);
        object.put("recipients", recipients);
        BaasBox.rest().async(Rest.Method.POST, "plugin/memos.script", object, new BaasHandler<JsonObject>() {
            @Override
            public void handle(BaasResult<JsonObject> baasResult) {
                MemoBus.SendBroadcastMemoResult result = new MemoBus.SendBroadcastMemoResult();
                if (baasResult.isSuccess()) {
                    JsonObject data = baasResult.value().getObject("data");
                    memo.remoteId = data.getString("id");
                    Date date;
                    try {
                        date = ISO8601Utils.parse(data.getString("_creation_date"), new ParsePosition(0));
                    } catch (ParseException e) {
                        Crashlytics.logException(e);
                        date = null;
                    }
                    memo.date = date;
                    memo.isFile = false;
                    memo.sentByMe = true;
                    memo.status = Memo.SENT;

                    ArrayList<Memo> memos = new ArrayList<>();
                    for (String username : usernames) {
                        memo.id = UUID.randomUUID().toString();
                        memo.connection = username;
                        memos.add(memo);
                    }
                    result.error = false;
                    result.memos = memos;
                } else {
                    Log.i(TAG, "FAILED TO SEND MEMO");
                    Log.i(TAG, baasResult.error().toString());
                    result.error = true;
                }
                EventBus.getDefault().post(result);
            }
        });
    }

    public static void fetchSchedule(final Context context, String campus) {
        final ArrayList<Schedule> events = new ArrayList<>();
        DaystarMemoNotification.notifyProgress(context, "Schedule download");
        BaasDocument.fetchAll(campus, new BaasHandler<List<BaasDocument>>() {
            @Override
            public void handle(BaasResult<List<BaasDocument>> baasResult) {
                DaystarMemoNotification.cancelProgress(context);
                Log.e(TAG, String.valueOf(baasResult.value().size()));
                if (baasResult.isSuccess()) {
                    if (baasResult.value().isEmpty()) {
                        return;
                    }
                    for (BaasDocument event : baasResult.value()) {
                        Schedule schedule = new Schedule();
                        schedule.id = UUID.randomUUID().toString();
                        schedule.title = event.getString("title");
                        JsonObject body = event.getObject("body");
                        schedule.location = body.getString("location");
                        schedule.day = body.getString("day");
                        schedule.dayIndex = getIndex(body.getString("start_time"), body.getString("day"));
                        schedule.startTime = DateUtils.scheduleTimeParser(body.getString("start_time"));
                        schedule.endTime = DateUtils.scheduleTimeParser(body.getString("end_time"));
                        events.add(schedule);
                    }
                    Schedule.saveSchedule(events);
                    Log.e(TAG, String.valueOf(events.size()));
                } else {
                    Log.i(TAG, "fetching schedule failed");
                    baasResult.error().printStackTrace();
                }
            }
        });

    }

    public static void fetchMemo(final String documentId) {
        BaasDocument.fetch("memos", documentId, new BaasHandler<BaasDocument>() {
            @Override
            public void handle(BaasResult<BaasDocument> baasResult) {
                MemoBus.FetchMemoResult memoResult = new MemoBus.FetchMemoResult();
                if (baasResult.isSuccess()) {
                    Memo memo = new Memo();
                    memo.id = UUID.randomUUID().toString();
                    Date date;
                    try {
                        date = ISO8601Utils.parse(baasResult.value().getCreationDate(), new ParsePosition(0));
                    } catch (ParseException e) {
                        Crashlytics.logException(e);
                        date = null;
                        e.printStackTrace();
                    }
                    memo.date = date;
                    memo.body = baasResult.value().getString("body");
                    memo.sentByMe = false;
                    memo.remoteId = baasResult.value().getId();
                    memo.status = Memo.UNREAD;
                    memo.connection = baasResult.value().getString("sender");
                    memoResult.error = false;
                    Memo.saveMemo(memo);
                } else {
                    MemoPendingDownload.add(documentId);
                    memoResult.error = true;
                    Log.i(TAG, "Unable to fetch document with id: " + documentId);
                }
                EventBus.getDefault().post(memoResult);
            }
        });
    }

    private static void sendNotification(final BaasDocument document, final String username) {
        JsonObject object = new JsonObject();
        object.put("sender", BaasUser.current().getName())
                .put("id", document.getId())
                .put("message", document.getString("body"))
                .put("type", "message");


        BaasBox.messagingService()
                .newMessage()
                .profiles(BaasCloudMessagingService.DEFAULT_PROFILE)
                .text("memo")
                .extra(object)
                .to(BaasUser.withUserName(username))
                .send(new BaasHandler<Void>() {
                    @Override
                    public void handle(BaasResult<Void> baasResult) {
                        MemoBus.SendMemoResult memoResult = new MemoBus.SendMemoResult();
                        if (baasResult.isSuccess()) {
                            memoResult.error = false;
                            memoResult.memo = generateMemoByMe(document.getId(), document.getCreationDate());
                            memoResult.memo.isFile = false;
                            memoResult.memo.body = document.getString("body");
                            memoResult.memo.connection = username;
                        } else {
                            revertSentMemo(document);
                            Log.i(TAG, "FAILED TO SEND NOTIFICATION");
                            memoResult.error = true;
                        }
                        EventBus.getDefault().post(memoResult);
                    }
                });
    }

    private static void sendNotification(final BaasDocument document, final MemoBus.SendMemoResult memoResult, final String username) {
        JsonObject object = new JsonObject();
        object.put("sender", BaasUser.current().getName())
                .put("id", document.getId())
                .put("message", document.getString("body"))
                .put("type", "message");


        BaasBox.messagingService()
                .newMessage()
                .profiles(BaasCloudMessagingService.DEFAULT_PROFILE)
                .text("memo")
                .extra(object)
                .to(BaasUser.withUserName(username))
                .send(new BaasHandler<Void>() {
                    @Override
                    public void handle(BaasResult<Void> baasResult) {
                        if (baasResult.isSuccess()) {
                            memoResult.error = false;
                            Date date;
                            try {
                                date = ISO8601Utils.parse(document.getCreationDate(), new ParsePosition(0));
                            } catch (ParseException e) {
                                Crashlytics.logException(e);
                                date = null;
                            }
                            memoResult.memo.date = date;
                            memoResult.memo.remoteId = document.getId();
                            memoResult.memo.status = Memo.SENT;
                        } else {
                            revertSentMemo(document);
                            Log.i(TAG, "FAILED TO SEND NOTIFICATION");
                            memoResult.error = true;
                        }
                        EventBus.getDefault().post(memoResult);
                    }
                });
    }

    private static void sendNotification(final BaasFile file, final MemoBus.SendFileResult result, final String... usernames) {
        JsonObject object = file.getAttachedData();
        String message = String.format("[%s] %s", object.getString("extension").toUpperCase(), object.getString("name"));
        object.put("sender", BaasUser.current().getName())
                .put("id", file.getId())
                .put("message", message)
                .put("type", "file")
                .put("size", file.getContentLength());

        BaasUser[] recipients = new BaasUser[usernames.length];
        for (int i = 0; i < usernames.length; i++) {
            recipients[i] = BaasUser.withUserName(usernames[i]);
        }

        BaasBox.messagingService()
                .newMessage()
                .profiles(BaasCloudMessagingService.DEFAULT_PROFILE)
                .text("memo")
                .extra(object)
                .to(recipients)
                .send(new BaasHandler<Void>() {
                    @Override
                    public void handle(BaasResult<Void> baasResult) {
                        if (baasResult.isSuccess()) {
                            result.error = false;
                            result.memo = generateMemoByMe(file.getId(), file.getCreationDate());
                            result.memo.isFile = true;

                            for (String username : usernames) {
                                result.memo.connection = username;
                                EventBus.getDefault().post(result);
                            }
                        } else {
                            revertSentFile(file);
                            result.error = true;
                            Log.i(TAG, baasResult.error().toString());
                            Log.i(TAG, "FAILED TO SEND NOTIFICATION");
                        }
                        EventBus.getDefault().post(result);
                    }
                });
    }

    public static void revertSentMemo(BaasDocument document) {
        document.delete(new BaasHandler<Void>() {
            @Override
            public void handle(BaasResult<Void> baasResult) {
                if (baasResult.isSuccess()) {
                    Log.i(TAG, "DOCUMENT DELETED SUCCESSFULLY");
                } else {
                    Log.i(TAG, "FAILED TO DELETE DOCUMENT");
                }
            }
        });
    }

    public static void sendFile(File file, final String transferId, final String... username) {
        JsonObject object = new JsonObject();
        object.put("filename", file.getName());
        object.put("name", FileUtils.removeExtension(file.getName()));
        object.put("extension", FileUtils.getExtension(file));
        BaasFile baasFile = BaasFile.create(object);
        BaasACL baasACL = BaasACL.builder()
                .users(Grant.READ, username)
                .build();

        baasFile.upload(baasACL, file, new BaasHandler<BaasFile>() {
            @Override
            public void handle(BaasResult<BaasFile> baasResult) {
                MemoBus.SendFileResult sendFileResult = new MemoBus.SendFileResult();
                sendFileResult.transferId = transferId;
                if (baasResult.isSuccess()) {
                    sendNotification(baasResult.value(), sendFileResult, username);
                } else {
                    sendFileResult.error = true;
                    EventBus.getDefault().post(sendFileResult);
                    baasResult.error().printStackTrace();
                }

            }
        });
    }

    private static void revertSentFile(BaasFile file) {
        file.delete(new BaasHandler<Void>() {
            @Override
            public void handle(BaasResult<Void> baasResult) {
                if (baasResult.isSuccess()) {
                    Log.i(TAG, "FILE DELETED SUCCESSFULLY");
                } else {
                    Log.i(TAG, "FAILED TO DELETE FILE");
                }
            }
        });
    }

    private static Memo generateMemoByMe(String id, String dateString) {
        Memo memo = new Memo();

        Date date;
        try {
            date = ISO8601Utils.parse(dateString, new ParsePosition(0));
        } catch (ParseException e) {
            Crashlytics.logException(e);
            date = null;
        }
        memo.date = date;
        memo.sentByMe = true;
        memo.remoteId = id;
        memo.status = Memo.SENT;
        return memo;
    }

    private static Memo generateMemoReceived(String id, String dateString) {
        Memo memo = new Memo();

        Date date;
        try {
            date = ISO8601Utils.parse(dateString, new ParsePosition(0));
        } catch (ParseException e) {
            Crashlytics.logException(e);
            date = null;
            e.printStackTrace();
        }
        memo.date = date;
        memo.sentByMe = false;
        memo.remoteId = id;
        memo.status = Memo.UNREAD;
        return memo;
    }

    public static int getIndex(String startTime, String day) {
        // Compute the index for each day and the corresponding time
        Map<String, Integer> days = new HashMap<>();
        days.put("monday", 0);
        days.put("tuesday", 2);
        days.put("wednesday", 4);
        days.put("thursday", 6);
        days.put("friday", 8);
        days.put("saturday", 10);

        Map<String, Integer> time = new HashMap<>();
        time.put("8:00am", 1);
        time.put("8:45am", 1);
        time.put("10:00am", 1);
        time.put("5:30pm", 1);
        time.put("6:30pm", 1);
        time.put("11:05am", 2);
        time.put("12:30pm", 2);
        time.put("1:30pm", 2);
        time.put("2:10pm", 3);

        return days.get(day.toLowerCase()) + time.get(startTime.toLowerCase());
    }

    public static void downloadFile(final Transfer transfer) {
        BaasFile.stream(transfer.remoteId, new DataStreamHandler<BaasFile>() {
            String filepath;
            BaasFile baasFile;
            FileOutputStream outputStream;
            long total = 0;
            @Override
            public void startData(String s, long l, String s1) throws Exception {
                BaasResult<BaasFile> result = BaasFile.fetchSync(transfer.remoteId);
                baasFile = result.get();
                filepath = FileUtils.prepareFile(baasFile.getAttachedData().getString("filename"));
                outputStream = new FileOutputStream(filepath);
                Log.i(getClass().getName(), "START: " + baasFile.getContentLength() + " : " + s1);
            }

            @Override
            public void onData(byte[] bytes, int i) throws Exception {
                total += i;
                Log.i(getClass().getName(), "PROGRESS: " + total);
                TransferManager.manager.update(transfer.id, total);
                outputStream.write(bytes, 0, i);
                //Log.i(getClass().getName(), "CHUNK: "+bytes.length+" : "+i);
            }

            @Override
            public BaasFile endData(String s, long l, String s1) throws Exception {
                Log.i(getClass().getName(), "END: " + l + " : " + s1);
                outputStream.close();
                return baasFile;
            }

            @Override
            public void finishStream(String s) {
                transfer.filepath = filepath;
                Log.i(getClass().getName(), "FINISH: " + s + " : " + new File(filepath).length());
            }
        }, new BaasHandler<BaasFile>() {
            @Override
            public void handle(BaasResult<BaasFile> baasResult) {
                TransferManager.manager.remove(transfer.id);
                if (baasResult.isSuccess()) {
                    transfer.status = Transfer.DONE;
                    Transfer.setCompletedTransfer(transfer.id, transfer.filepath, Transfer.DONE);
                    Memo memo = generateMemoReceived(baasResult.value().getId(), baasResult.value().getCreationDate());
                    memo.isFile = true;
                    memo.id = UUID.randomUUID().toString();
                    memo.filepath = transfer.filepath;
                    memo.date = new Date();
                    memo.connection = baasResult.value().getAuthor();
                    Memo.saveMemo(memo);
                    Log.i(getClass().getName(), "DOWNLOADED SUCCESSFULLY");
                } else {
                    Transfer.setCompletedTransfer(transfer.id, "", Transfer.FAILED);
                    Log.i(getClass().getName(), "DOWNLOADED FAILED: " + baasResult.error().toString());
                }
            }
        });
    }
}
