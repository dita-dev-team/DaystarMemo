package com.dev.dita.daystarmemo.ui.main;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.dev.dita.daystarmemo.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainAdapter extends RecyclerView.Adapter<MainAdapter.ViewHolder> {
    final Context context;
    final LayoutInflater inflater;
    final ArrayList<Item> items;
    final int[] drawableIds = {
            R.drawable.ic_info_variant,
            R.drawable.ic_memo_white,
            R.drawable.ic_calendar_white,
            R.drawable.ic_academic
    };
    final String[] titles = {
            "About",
            "Messages",
            "Events",
            "Academic programmes"
    };
    OnItemClickListener itemClickListener;
    private boolean isGuest;
    private Drawable background1;
    private Drawable background2;

    public MainAdapter(Context context, boolean isGuest) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.isGuest = isGuest;
        background1 = ContextCompat.getDrawable(context, R.drawable.circular_background1);
        background2 = ContextCompat.getDrawable(context, R.drawable.circular_background2);
        items = new ArrayList<>();

        for (int i = 0; i < titles.length; i++) {
            if (isGuest && titles[i].equalsIgnoreCase("messages")) {
            } else {
                items.add(new Item(titles[i], drawableIds[i]));
            }
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.main_list_item, parent, false);
        final ViewHolder viewHolder = new ViewHolder(view);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemClickListener.onItemClick(view, viewHolder.getLayoutPosition());
            }
        });
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Item item = items.get(position);
        holder.title.setText(item.label);
        Drawable drawable = ContextCompat.getDrawable(context, item.drawableId);
        holder.thumbnail.setImageDrawable(drawable);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {

            if (position == 0) {
                holder.thumbnail.setBackground(background1);
            } else {
                if (position % 2 == 0) {
                    holder.thumbnail.setBackground(background1);
                } else {
                    holder.thumbnail.setBackground(background2);
                }
            }
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public ArrayList<Item> getItems() {
        return items;
    }

    public void setItemClickListener(OnItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.title)
        TextView title;
        @BindView(R.id.thumbnail)
        ImageView thumbnail;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    static class Item {
        public String label;
        public int drawableId;

        public Item(String label, int drawableId) {
            this.label = label;
            this.drawableId = drawableId;
        }
    }
}
