package com.dev.dita.daystarmemo.model.database;


import android.util.Log;

import com.dev.dita.daystarmemo.controller.bus.MemoBus;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmResults;
import io.realm.annotations.Ignore;
import io.realm.annotations.Index;
import io.realm.annotations.PrimaryKey;

public class Memo extends RealmObject {
    @Ignore
    public static final int SENT = 1;
    @Ignore
    public static final int UNREAD = 2;
    @Ignore
    public static final int READ = 3;
    @Ignore
    public static final int PENDING = 4;
    private static final String TAG = Memo.class.getName();
    @PrimaryKey
    public String id = "";
    @Index
    public String connection = "";
    public String subject = "";
    public String body = "";
    public int status = 0;
    public Date date;
    @Index
    public Boolean latest = false;
    public Boolean sentByMe = false;
    public String remoteId = "";
    public Boolean isFile = false;
    public String filepath = "";

    public static void saveMemo(final Memo memo) {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                Connection connection = realm.where(Connection.class).equalTo("username", memo.connection).findFirst();
                connection.memos.add(memo);
            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                EventBus.getDefault().post(new MemoBus.ScrollToBottomEvent());
                Log.i(TAG, "MEMO SAVED SUCCESSFULLY");
            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                Log.e(TAG, "FAILED TO SAVE MEMO");
            }
        });
    }

    public static void saveMemos(final ArrayList<Memo> memos) {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                for (Memo memo : memos) {
                    Connection connection = realm.where(Connection.class).equalTo("username", memo.connection).findFirst();
                    connection.memos.add(memo);
                }
            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                EventBus.getDefault().post(new MemoBus.ScrollToBottomEvent());
                EventBus.getDefault().post(new MemoBus.SendMemoEvent());
                Log.i(TAG, "MEMOS SAVED SUCCESSFULLY");
            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                Log.e(TAG, "FAILED TO SAVE MEMOS", error);
            }
        });
    }

    public static void deleteMemoByUsername(final String username) {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                RealmResults<Memo> memos = realm.where(Memo.class).equalTo("isFile", false).equalTo("connection", username).findAll();
                for (Memo memo : memos) {
                    memo.deleteFromRealm();
                }
            }
        });
    }

    public static void deleteMemoById(final String id) {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                Memo memo = realm.where(Memo.class).equalTo("id", id).findFirst();
                memo.deleteFromRealm();
            }
        });
    }

    public static void deleteMemosById(final List<String> ids) {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                for (String id : ids) {
                    Memo memo = realm.where(Memo.class).equalTo("id", id).findFirst();
                    memo.deleteFromRealm();
                }
            }
        });
    }

    public static void deleteMemo() {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                RealmResults<Memo> memos = realm.where(Memo.class).equalTo("isFile", false).findAll();
                for (Memo memo : memos) {
                    memo.deleteFromRealm();
                }
            }
        });
    }

    public static void deleteReceivedMemoDocuments() {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                RealmResults<Memo> results = realm.where(Memo.class).equalTo("isFile", true).equalTo("sentByMe", false).findAll();
                results.deleteAllFromRealm();
            }
        });
    }

    public static void deleteSentMemoDocuments() {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                RealmResults<Memo> results = realm.where(Memo.class).equalTo("isFile", true).equalTo("sentByMe", true).findAll();
                results.deleteAllFromRealm();
            }
        });
    }

    public static void updatePendingMemo(final Memo memo) {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                Memo memo1 = realm.where(Memo.class).equalTo("id", memo.id).findFirst();
                memo1.date = memo.date;
                memo1.remoteId = memo.remoteId;
                memo1.status = memo.status;
            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                EventBus.getDefault().post(new MemoBus.PendingMemoUpdatedResult());
            }
        });
    }

    public static void deletePendingMemo(final String username) {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                RealmResults<Memo> memos = realm.where(Memo.class).equalTo("connection", username).equalTo("status", Memo.PENDING).findAll();
                for (Memo memo : memos) {
                    memo.deleteFromRealm();
                }
            }
        });
    }

    public static void markAsRead(final String memoId) {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                Memo memo = realm.where(Memo.class).equalTo("id", memoId).findFirst();
                memo.status = Memo.READ;
            }
        });
    }

    public static void markAsReadByUsername(final String username) {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                RealmResults<Memo> memos = realm.where(Memo.class).equalTo("connection", username).equalTo("status", Memo.UNREAD).findAll();
                Log.i(TAG, "FOUND: " + memos.size());
                for (Memo memo : memos) {
                    Log.i(TAG, "STATUS: " + memo.status);
                    memo.status = Memo.READ;
                }
            }
        });
    }

    public static void markAsRead() {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                RealmResults<Memo> memos = realm.where(Memo.class).equalTo("status", Memo.UNREAD).findAll();
                for (Memo memo : memos) {
                    memo.status = Memo.READ;
                }
            }
        });
    }


}
