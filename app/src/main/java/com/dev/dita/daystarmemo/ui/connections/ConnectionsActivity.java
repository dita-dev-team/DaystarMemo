package com.dev.dita.daystarmemo.ui.connections;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.dev.dita.daystarmemo.R;
import com.dev.dita.daystarmemo.controller.bus.UserBus;
import com.dev.dita.daystarmemo.model.baas.UserBaas;
import com.dev.dita.daystarmemo.model.database.Connection;
import com.dev.dita.daystarmemo.ui.BaseActivity;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;

public class ConnectionsActivity extends BaseActivity {
    final String TAG = getClass().getName();
    @BindView(R.id.connections_empty)
    TextView empty;
    @BindView(R.id.connections_list_view)
    RecyclerView listView;
    @BindView(R.id.connections_new_connection)
    FloatingActionButton newConnectionButton;
    ProgressDialog progressDialog;

    private Realm realm;
    private RealmResults<Connection> connections;
    private ConnectionsListAdapter adapter;
    private final RealmChangeListener<RealmResults<Connection>> listener = new RealmChangeListener<RealmResults<Connection>>() {
        @Override
        public void onChange(RealmResults<Connection> element) {
            empty.setVisibility(adapter.getItemCount() == 0 ? View.VISIBLE : View.GONE);
        }
    };

    private ActionMode actionMode;

    private DrawerLayout drawerLayout;

    private void init() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.removing_connection) + "...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        realm = Realm.getDefaultInstance();
        connections = realm.where(Connection.class).findAllSortedAsync("username");
        connections.addChangeListener(listener);
        adapter = new ConnectionsListAdapter(this, connections, true);
        adapter.setOnItemClickListener(new ConnectionsListAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Intent intent = new Intent(ConnectionsActivity.this, ConnectionDetailsActivity.class);
                intent.putExtra("username", adapter.getItem(position).username);
                startActivity(intent);
            }
        });
        adapter.setOnItemLongClickListener(new ConnectionsListAdapter.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(View view, int position) {
                if (actionMode != null) {
                    return false;
                }

                actionMode = startActionMode(buildCallback(position));
                return true;
            }
        });
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        listView.setLayoutManager(layoutManager);
        listView.setAdapter(adapter);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            listView.setOnScrollChangeListener(new View.OnScrollChangeListener() {
                @Override
                public void onScrollChange(View view, int i, int i1, int i2, int i3) {

                }
            });
        } else {
            listView.setOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                    if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                        newConnectionButton.setVisibility(View.VISIBLE);
                    } else {
                        newConnectionButton.setVisibility(View.INVISIBLE);
                    }
                }
            });
        }

        empty.setVisibility(adapter.getItemCount() == 0 ? View.VISIBLE : View.GONE);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connections);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        ButterKnife.bind(this);
        EventBus.getDefault().register(this);
        Toolbar toolbar = getActionBarToolbar();
        toolbar.setNavigationIcon(R.drawable.ic_back_white);
        toolbar.setNavigationContentDescription(R.string.close_and_go_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                navigateUpOrBack(ConnectionsActivity.this, null);
            }
        });
        setSupportActionBar(toolbar);
        init();
    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
        connections.removeChangeListeners();
        realm.close();
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout != null && drawerLayout.isDrawerOpen(GravityCompat.END)) {
            drawerLayout.closeDrawer(GravityCompat.END);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected int getSelfNavDrawerItem() {
        return NAVDRAWER_ITEM_CONNECTIONS;
    }

    @OnClick(R.id.connections_new_connection)
    public void onNewButtonClicked() {
        startActivity(new Intent(this, NewConnectionActivity.class));
    }

    private ActionMode.Callback buildCallback(final int position) {
        return new ActionMode.Callback() {
            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                MenuInflater inflater = mode.getMenuInflater();
                inflater.inflate(R.menu.connections_list_contextual_menu, menu);
                return true;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                Connection connection = adapter.getItem(position);
                final String username = connection.username;
                switch (item.getItemId()) {
                    case R.id.connections_list_contextual_delete:
                        new AlertDialog.Builder(ConnectionsActivity.this)
                                .setTitle(R.string.delete)
                                .setMessage(R.string.are_you_sure)
                                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        progressDialog.show();
                                        UserBaas.removeConnectionFromProfile(username);
                                    }
                                })
                                .setNegativeButton(R.string.no, null)
                                .show();
                        actionMode.finish();
                        return true;
                    default:
                        return false;
                }
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {
                actionMode = null;
            }
        };
    }

    @Subscribe
    public void onEvent(UserBus.RemoveConnectionResult removeConnectionResult) {
        if (!removeConnectionResult.error) {
            Connection.removeUser(removeConnectionResult.username);
        } else {
            progressDialog.dismiss();
            Toast.makeText(this, getString(R.string.unable_to_remove, getString(R.string.connection)), Toast.LENGTH_SHORT).show();
        }
    }

    @Subscribe
    public void onEvent(UserBus.RemoveConnectionFromDbResult removeConnectionFromDbResult) {
        progressDialog.dismiss();
        if (!removeConnectionFromDbResult.error) {
            Toast.makeText(this, String.format("%s %s", R.string.connection, getString(R.string.removed_successfully)), Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, getString(R.string.unable_to_remove, getString(R.string.connection)), Toast.LENGTH_SHORT).show();
        }
    }
}
