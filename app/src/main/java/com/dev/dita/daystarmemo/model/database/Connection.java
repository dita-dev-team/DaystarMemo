package com.dev.dita.daystarmemo.model.database;

import android.util.Log;

import com.baasbox.android.BaasUser;
import com.baasbox.android.json.JsonObject;
import com.dev.dita.daystarmemo.controller.bus.UserBus;

import org.greenrobot.eventbus.EventBus;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.RealmResults;
import io.realm.annotations.PrimaryKey;

public class Connection extends RealmObject {
    @PrimaryKey
    public String username;
    public String name;
    public byte[] image;
    public String email;
    public String major;
    public RealmList<Memo> memos;

    public static void addUser(final BaasUser user) {
        final Realm realm = Realm.getDefaultInstance();
        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {

                Connection newConnection = new Connection();
                newConnection.username = user.getName();
                JsonObject extraData = user.getScope(BaasUser.Scope.FRIEND);
                if (extraData != null) {
                    newConnection.name = extraData.getString("name", "");
                    newConnection.email = extraData.getString("email", "");
                    newConnection.major = extraData.getString("major", "");
                    newConnection.image = extraData.getBinary("image", null);
                }
                realm.copyToRealm(newConnection);
            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                UserBus.AddConnectionToDbResult userResult = new UserBus.AddConnectionToDbResult();
                userResult.error = false;
                EventBus.getDefault().post(userResult);

                if (!realm.isInTransaction()) {
                    realm.close();
                }
            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                UserBus.AddConnectionToDbResult userResult = new UserBus.AddConnectionToDbResult();
                userResult.error = true;
                EventBus.getDefault().post(userResult);

                if (!realm.isInTransaction()) {
                    realm.close();
                }
            }
        });
    }

    public static void addUserWithoutCallback(final BaasUser user) {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                Connection newConnection = new Connection();
                newConnection.username = user.getName();
                JsonObject extraData = user.getScope(BaasUser.Scope.FRIEND);
                if (extraData != null) {
                    newConnection.name = extraData.getString("name", "");
                    newConnection.email = extraData.getString("email", "");
                    newConnection.major = extraData.getString("major", "");
                    newConnection.image = extraData.getBinary("image", null);
                }
                realm.copyToRealmOrUpdate(newConnection);
                Log.i(getClass().getName(), "DONE");
            }
        });
    }

    public static void removeUser(final String username) {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.where(Connection.class).equalTo("username", username).findFirst().deleteFromRealm();

                realm.where(Memo.class)
                        .equalTo("connection", username)
                        .findAll().deleteAllFromRealm();

            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                UserBus.RemoveConnectionFromDbResult result = new UserBus.RemoveConnectionFromDbResult();
                result.error = false;
                EventBus.getDefault().post(result);
            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                UserBus.RemoveConnectionFromDbResult result = new UserBus.RemoveConnectionFromDbResult();
                result.error = true;
                EventBus.getDefault().post(result);
            }
        });
    }

    public static void removeUsers() {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                RealmResults<Connection> results = realm.where(Connection.class).findAll();
                for (Connection connection : results) {
                    connection.deleteFromRealm();
                }
            }
        });
    }

    public static boolean exists(String username) {
        Realm realm = Realm.getDefaultInstance();
        Connection connection = realm.where(Connection.class).equalTo("username", username).findFirst();
        return connection != null;
    }

    public static void putImage(final String username, final byte[] image) {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                Connection connection = realm.where(Connection.class).equalTo("username", username).findFirst();
                connection.image = image;
            }
        });
    }
}
