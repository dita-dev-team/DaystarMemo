package com.dev.dita.daystarmemo.ui.memosgroups;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Arrays;
import java.util.Comparator;

public class SectionedRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int SECTION_TYPE = 0;
    private final Context context;
    private boolean valid = true;
    private LayoutInflater inflater;
    private int sectionResourceId;
    private int textResourceId;
    private RecyclerView.Adapter baseAdapter;
    private SparseArray<Section> sections = new SparseArray<>();

    public SectionedRecyclerViewAdapter(Context context, int sectionResourceId, int textResourceId, final RecyclerView.Adapter baseAdapter) {
        this.context = context;
        this.inflater = LayoutInflater.from(context);
        this.sectionResourceId = sectionResourceId;
        this.textResourceId = textResourceId;
        this.baseAdapter = baseAdapter;

        this.baseAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onChanged() {
                valid = baseAdapter.getItemCount() > 0;
                notifyDataSetChanged();
            }

            @Override
            public void onItemRangeChanged(int positionStart, int itemCount) {
                valid = baseAdapter.getItemCount() > 0;
                notifyItemRangeChanged(positionStart, itemCount);
            }

            @Override
            public void onItemRangeInserted(int positionStart, int itemCount) {
                valid = baseAdapter.getItemCount() > 0;
                notifyItemRangeInserted(positionStart, itemCount);
            }

            @Override
            public void onItemRangeRemoved(int positionStart, int itemCount) {
                valid = baseAdapter.getItemCount() > 0;
                notifyItemRangeRemoved(positionStart, itemCount);
            }
        });
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == SECTION_TYPE) {
            final View view = inflater.inflate(sectionResourceId, parent, false);
            return new ViewHolder(view, textResourceId);
        } else {
            return baseAdapter.onCreateViewHolder(parent, viewType - 1);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (isSectionHeaderPosition(position)) {
            ((ViewHolder) holder).title.setText(sections.get(position).title);
        } else {
            baseAdapter.onBindViewHolder(holder, sectionedPositionToPosition(position));
        }
    }

    @Override
    public int getItemCount() {
        return (valid ? baseAdapter.getItemCount() + sections.size() : 0);
    }

    @Override
    public int getItemViewType(int position) {
        return isSectionHeaderPosition(position) ? SECTION_TYPE :
                baseAdapter.getItemViewType(sectionedPositionToPosition(position));
    }

    @Override
    public long getItemId(int position) {
        return isSectionHeaderPosition(position) ?
                Integer.MAX_VALUE - sections.indexOfKey(position) :
                baseAdapter.getItemId(sectionedPositionToPosition(position));
    }


    public void setSections(Section[] sections1) {
        sections.clear();

        Arrays.sort(sections1, new Comparator<Section>() {
            @Override
            public int compare(Section section, Section t1) {
                return (section.firstPosition == t1.firstPosition) ? 0
                        : ((section.firstPosition < t1.firstPosition) ? -1 : 1);
            }
        });

        int offset = 0;
        for (Section section : sections1) {
            section.sectionedPosition = section.firstPosition + offset;
            sections.append(section.sectionedPosition, section);
            ++offset;
        }

        notifyDataSetChanged();
    }

    public int positionToSectionPosition(int position) {
        int offset = 0;
        for (int i = 0; i < sections.size(); i++) {
            if (sections.valueAt(i).firstPosition > position) {
                break;
            }
            ++offset;
        }

        return position + offset;
    }

    public int sectionedPositionToPosition(int sectionedPosition) {
        if (isSectionHeaderPosition(sectionedPosition)) {
            return RecyclerView.NO_POSITION;
        }

        int offset = 0;
        for (int i = 0; i < sections.size(); i++) {
            if (sections.valueAt(i).sectionedPosition > sectionedPosition) {
                break;
            }
            --offset;
        }
        return sectionedPosition + offset;
    }

    public boolean isSectionHeaderPosition(int position) {
        return sections.get(position) != null;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView title;

        public ViewHolder(View view, int textResourceId) {
            super(view);
            title = (TextView) view.findViewById(textResourceId);
        }
    }

    public static class Section {
        public CharSequence title;
        int firstPosition;
        int sectionedPosition;

        public Section(int firstPosition, CharSequence title) {
            this.firstPosition = firstPosition;
            this.title = title;
        }
    }
}
