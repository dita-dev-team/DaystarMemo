package com.dev.dita.daystarmemo.model.database;

import android.util.Log;

import com.dev.dita.daystarmemo.controller.bus.UserBus;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmObject;
import io.realm.RealmResults;
import io.realm.annotations.Index;
import io.realm.annotations.PrimaryKey;

public class Schedule extends RealmObject {
    private static final String TAG = Schedule.class.getName();
    public String id = "";
    @Index
    @PrimaryKey
    public String title = "";
    public String location = "";
    public String day = "";
    public Date startTime;
    public Date endTime;
    @Index
    public Boolean attending = false;
    @Index
    public int dayIndex;

    public static void saveSchedule(final ArrayList<Schedule> schedules) {
        System.out.println("Schedules Fetched");
        Log.e(TAG, String.valueOf(schedules.size()));
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                RealmResults<Schedule> results = realm.where(Schedule.class).findAll();
                results.deleteAllFromRealm();
                List<Schedule> schedule = realm.copyToRealmOrUpdate(schedules);
            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                Log.i(TAG, "Schedules added");
                EventBus.getDefault().post(new UserBus.SaveSchedule());
            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                Log.e(TAG, "Schedules not added: " + error.getCause());
            }
        });
    }

    public static ArrayList<Schedule> getAll() {
        final ArrayList<Schedule> list = new ArrayList<>();
        Realm realm = Realm.getDefaultInstance();
        final RealmResults<Schedule> schedules = realm.where(Schedule.class).findAllAsync();
        schedules.addChangeListener(new RealmChangeListener<RealmResults<Schedule>>() {
            @Override
            public void onChange(RealmResults<Schedule> element) {
                if (element.isLoaded()) {
                    for (Schedule schedule : element) {
                        list.add(schedule);
                    }
                }
            }
        });
        return list;
    }


    public static void setAttending(final List<String> ids, final boolean isAttending) {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                long count = realm.where(Schedule.class).count();
                if (count == 0) {
                    EventBus.getDefault().post(new UserBus.DownloadSchedule());
                    return;
                }
                for (String id : ids) {
                    Schedule schedule = realm.where(Schedule.class).equalTo("title", id).findFirst();
                    schedule.attending = isAttending;
                }
            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                EventBus.getDefault().post(new UserBus.ScheduleUpdated());
            }
        });
    }

    public static void removeSchedule(final String title) {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                Schedule event = realm.where(Schedule.class).equalTo("title", title).findFirst();
                event.attending = false;
            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                EventBus.getDefault().post(new UserBus.ScheduleUpdated());
            }
        });
    }

}
