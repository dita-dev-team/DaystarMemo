package com.dev.dita.daystarmemo.model.database;

import io.realm.RealmObject;

public class GroupMember extends RealmObject {
    public String username;
    public String name;

    public GroupMember() {
    }

    public GroupMember(String username) {
        this.username = username;
        this.name = "";
    }
}
