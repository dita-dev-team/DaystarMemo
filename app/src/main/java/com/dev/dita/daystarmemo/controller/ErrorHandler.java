package com.dev.dita.daystarmemo.controller;

import com.baasbox.android.json.JsonException;

import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

public class ErrorHandler {

    public static String getMessage(Throwable throwable) {
        String message = null;
        if (throwable.getCause() instanceof SocketTimeoutException || throwable.getCause() instanceof UnknownHostException || throwable.getCause() instanceof JsonException) {
            message = "Unable to connect";
        } else if (throwable instanceof NullPointerException) {
            message = "Session expired";
        } else {
            message = localizeMessage(throwable.toString());
        }
        return message;
    }

    private static String localizeMessage(String message) {
        System.err.println(message);
        String localMessage = null;

        if (message.contains("unauthorized") && message.contains("/login")) {
            localMessage = "Invalid username or password";
        } else if (message.contains("Error signing up") && message.contains("/user")) {
            localMessage = "User already exists";
        } else if (message.contains("not a valid collection name") && message.contains("group")) {
            localMessage = "No groups found";
        }
        return localMessage;
    }
}
