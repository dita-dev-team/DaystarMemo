package com.dev.dita.daystarmemo.ui.documents;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.text.format.Formatter;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.dev.dita.daystarmemo.R;
import com.dev.dita.daystarmemo.controller.bus.MemoBus;
import com.dev.dita.daystarmemo.controller.utils.FileUtils;
import com.dev.dita.daystarmemo.controller.utils.TransferManager;
import com.dev.dita.daystarmemo.model.baas.MemoBaas;
import com.dev.dita.daystarmemo.model.database.Transfer;
import com.github.rahatarmanahmed.cpv.CircularProgressView;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

class TransfersAdapter extends RecyclerView.Adapter<TransfersAdapter.ViewHolder> {
    private final LayoutInflater inflater;
    private final Context context;
    private ArrayList<Transfer> data;

    public TransfersAdapter(Context context, ArrayList<Transfer> data) {
        this.context = context;
        this.data = data;
        inflater = LayoutInflater.from(this.context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.transfer_item, parent, false);
        final ViewHolder viewHolder = new ViewHolder(view);
        viewHolder.download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onDownloadClicked(viewHolder.getLayoutPosition());
                //simulateDownload(viewHolder.getLayoutPosition());
                Toast.makeText(context, "Clicked", Toast.LENGTH_SHORT).show();
            }
        });
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final Transfer transfer = data.get(position);
        File file = new File(transfer.filepath);
        if ((transfer.type == Transfer.DOWNLOAD && transfer.status == Transfer.DONE && !file.exists()) ||
                (transfer.type == Transfer.UPLOAD && !file.exists())) {

            holder.title.setText("No file found");
            holder.title.setTextColor(ContextCompat.getColor(context, R.color.baseColor2));
            holder.thumbnail.setVisibility(View.GONE);
            holder.progressView.setVisibility(View.GONE);
            holder.refresh.setVisibility(View.GONE);
            holder.upload.setVisibility(View.GONE);
            holder.download.setVisibility(View.GONE);
            holder.done.setVisibility(View.GONE);
            holder.info.setVisibility(View.GONE);
            holder.size.setVisibility(View.GONE);

        } else {
            if (TextUtils.isEmpty(transfer.name)) {
                holder.title.setText(FileUtils.removeExtension(file.getName()));
            } else {
                holder.title.setText(transfer.name);
            }

            holder.info.setText(FileUtils.getExtension(file));
            String size = Formatter.formatShortFileSize(context, transfer.total);
            holder.size.setText(size);
            holder.thumbnail.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_file_document));

            // set active transfer icon
            switch (transfer.status) {
                case Transfer.DONE:
                    setVisibility(holder.done, holder);
                    break;
                case Transfer.DONE_PENDING:
                    if (!holder.progressView.isIndeterminate()) {
                        holder.progressView.setIndeterminate(true);
                        holder.progressView.resetAnimation();
                    }
                    setVisibility(holder.progressView, holder);
                    break;
                case Transfer.IN_PROGRESS:
                    if (holder.progressView.isIndeterminate()) {
                        holder.progressView.setIndeterminate(false);
                    }
                    setVisibility(holder.progressView, holder);
                    holder.progressView.setMaxProgress(100);
                    holder.progressView.setProgress(transfer.progress);
                    break;
                case Transfer.WAITING:
                    if (!holder.progressView.isIndeterminate()) {
                        holder.progressView.setIndeterminate(true);
                    }
                    setVisibility(holder.progressView, holder);
                    break;
                case Transfer.FAILED:
                    setVisibility(holder.refresh, holder);
                    break;
                case Transfer.PENDING:
                    setVisibility(transfer.type == Transfer.UPLOAD ? holder.upload : holder.download, holder);
                    break;
            }

            // set date view visibility
            if (transfer.status == Transfer.IN_PROGRESS || transfer.status == Transfer.DONE_PENDING) {
                holder.date.setVisibility(View.GONE);
            } else {
                holder.date.setVisibility(View.VISIBLE);
                holder.date.setText(DateUtils.formatDateTime(context, transfer.date.getTime(), DateUtils.FORMAT_SHOW_TIME | DateUtils.FORMAT_SHOW_DATE));
            }
        }

        final PopupMenu popupMenu = new PopupMenu(context, holder.popup);
        popupMenu.inflate(R.menu.menu_transfers_popup);
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.transfers_popup_delete:
                        Transfer.removeTransfer(transfer.id);
                        notifyDataSetChanged();
                        //notifyItemRemoved(position);
                        Toast.makeText(context, "Removed successfully", Toast.LENGTH_SHORT).show();
                        return true;
                    default:
                        return false;
                }
            }
        });
        holder.popup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popupMenu.show();
            }
        });
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public int getItemIndex(String id) {
        for (int i = 0; i < data.size(); i++) {
            if (data.get(i).id.equals(id)) {
                return i;
            }
        }

        return -1;
    }

    public ArrayList<Transfer> getData() {
        return data;
    }

    public void setData(ArrayList<Transfer> data) {
        this.data = data;
    }

    public boolean containsTransfer(String id) {
        for (Transfer transfer : data) {
            if (transfer.id.equals(id)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Hides and shows active and inactive views
     *
     * @param view
     * @param viewHolder
     */
    private void setVisibility(View view, ViewHolder viewHolder) {
        viewHolder.progressView.setVisibility(view.getId() == R.id.transfer_progress ? View.VISIBLE : View.GONE);
        viewHolder.refresh.setVisibility(view.getId() == R.id.transfer_refresh_button ? View.VISIBLE : View.GONE);
        viewHolder.upload.setVisibility(view.getId() == R.id.transfer_upload_button ? View.VISIBLE : View.GONE);
        viewHolder.download.setVisibility(view.getId() == R.id.transfer_download_button ? View.VISIBLE : View.GONE);
        viewHolder.done.setVisibility(view.getId() == R.id.transfer_done ? View.VISIBLE : View.GONE);
    }

    private void onDownloadClicked(int position) {
        Transfer transfer = data.get(position);
        //Transfer.changeStatus(transfer.id, Transfer.WAITING);
        data.get(position).status = Transfer.WAITING;
        notifyItemChanged(position);

        MemoBus.TransferProgress transferProgress = new MemoBus.TransferProgress();
        transferProgress.id = transfer.id;
        transferProgress.current = 0;
        transferProgress.total = transfer.total;
        TransferManager.manager.put(transfer.id, transferProgress);
        MemoBaas.downloadFile(transfer);
    }

    private void simulateDownload(int position) {
        final Transfer transfer = data.get(position);
        data.get(position).status = Transfer.WAITING;
        notifyItemChanged(position);

        new AsyncTask<Void, Void, Void>() {
            MemoBus.TransferProgress transferProgress;

            @Override
            protected void onPreExecute() {
                transferProgress = new MemoBus.TransferProgress();
                transferProgress.id = transfer.id;
                transferProgress.current = 0;
                transferProgress.total = 100;
            }

            @Override
            protected Void doInBackground(Void... voids) {
                while (transferProgress.current < 100) {
                    transferProgress.current += 2;
                    transferProgress.progress = (int) transferProgress.current;
                    if (transferProgress.current == 100) {
                        transferProgress.isDone = true;
                    }
                    EventBus.getDefault().post(transferProgress);
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

                return null;
            }
        }.execute();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.transfer_thumbnail)
        ImageView thumbnail;
        @BindView(R.id.transfer_title)
        TextView title;
        @BindView(R.id.transfer_progress)
        CircularProgressView progressView;
        @BindView(R.id.transfer_refresh_button)
        ImageButton refresh;
        @BindView(R.id.transfer_upload_button)
        ImageButton upload;
        @BindView(R.id.transfer_download_button)
        ImageButton download;
        @BindView(R.id.transfer_done)
        ImageView done;
        @BindView(R.id.transfer_popup)
        ImageButton popup;
        @BindView(R.id.transfer_info_extension)
        TextView info;
        @BindView(R.id.transfer_info_size)
        TextView size;
        @BindView(R.id.transfer_info_date)
        TextView date;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
