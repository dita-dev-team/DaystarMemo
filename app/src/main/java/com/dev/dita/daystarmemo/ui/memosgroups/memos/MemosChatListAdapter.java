package com.dev.dita.daystarmemo.ui.memosgroups.memos;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.baasbox.android.BaasUser;
import com.dev.dita.daystarmemo.R;
import com.dev.dita.daystarmemo.model.database.Memo;
import com.dev.dita.daystarmemo.ui.customviews.RealmSelectableAdapter;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import io.realm.OrderedRealmCollection;

/**
 * The type Memos chat list adapter.
 */
public class MemosChatListAdapter extends RealmSelectableAdapter<Memo, MemosChatListAdapter.ViewHolder> {
    private static final int TIME_SECTION = 1;
    private static final int NO_TIME_SECTION = 2;
    private static final int DATE_SECTION = 3;
    private static final int NO_DATE_SECTION = 4;
    /**
     * The Tag.
     */
    final private String TAG = getClass().getName();
    private final int color1;
    private final int color2;
    private final int color3;
    private Bitmap image;
    private int[] rowDateStates;
    private int[] rowTimeStates;
    private ClickListener clickListener;

    /**
     * Instantiates a new Memos chat list adapter.
     *
     * @param context the context
     * @param data    the data
     */
    public MemosChatListAdapter(Context context, OrderedRealmCollection<Memo> data, boolean autoUpdate) {
        super(context, data, autoUpdate);
        color1 = ContextCompat.getColor(context, R.color.baseColor1);
        color2 = ContextCompat.getColor(context, R.color.baseColor2);
        color3 = ContextCompat.getColor(context, R.color.baseColor3);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.memos_chat_list_item, parent, false);
        final ViewHolder viewHolder = new ViewHolder(view);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (clickListener != null) {
                    clickListener.onItemClick(view, viewHolder.getLayoutPosition());
                }

            }
        });
        view.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                if (clickListener != null) {
                    return clickListener.onItemLongClick(view, viewHolder.getLayoutPosition());
                }

                return false;
            }
        });
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Memo memo = getItem(position);
        setAlignment(holder, memo.sentByMe);
        holder.message.setText(memo.body);

        ViewGroup.MarginLayoutParams layoutParams;
        switch (memo.status) {
            case Memo.PENDING:
                holder.mainContent.setCardBackgroundColor(color3);
                layoutParams = (ViewGroup.MarginLayoutParams) holder.mainContent.getLayoutParams();
                layoutParams.bottomMargin = R.dimen.memo_chat_pending_margin;
                holder.time.setVisibility(View.GONE);
                break;
            case Memo.SENT:
                holder.mainContent.setCardBackgroundColor(color2);
                layoutParams = (ViewGroup.MarginLayoutParams) holder.mainContent.getLayoutParams();
                layoutParams.bottomMargin = 0;
                holder.time.setVisibility(View.VISIBLE);
                break;
            default:
                holder.mainContent.setCardBackgroundColor(color1);
                break;
        }

        if (memo.status == Memo.UNREAD) {
            Memo.markAsRead(memo.id);
        }

        boolean hideTime = false;
        switch (rowTimeStates[position]) {
            case TIME_SECTION:
                hideTime = false;
                break;
            case NO_TIME_SECTION:
                hideTime = true;
                break;
            default:
                int count = getItemCount();
                if (!(memo.status == Memo.PENDING)) {
                    if (position != count - 1) {
                        Memo temp = getItem(position + 1);
                        if (!(temp.status == Memo.PENDING) && memo.sentByMe == temp.sentByMe) {
                            if (com.dev.dita.daystarmemo.controller.utils.DateUtils.timesEqual(memo.date, temp.date)) {
                                hideTime = true;
                            }
                        }
                    }
                }
                rowTimeStates[position] = hideTime ? NO_TIME_SECTION : TIME_SECTION;
        }

        boolean hideDate = false;
        switch (rowDateStates[position]) {
            case DATE_SECTION:
                hideDate = false;
                break;
            case NO_DATE_SECTION:
                hideDate = true;
                break;
            default:
                if (position == 0) {
                    hideDate = false;
                } else {
                    Memo temp = getItem(position - 1);
                    if (com.dev.dita.daystarmemo.controller.utils.DateUtils.datesEqual(memo.date, temp.date)) {
                        hideDate = true;
                    }
                }
                rowDateStates[position] = hideDate ? NO_DATE_SECTION : DATE_SECTION;
        }

        if (hideTime) {
            holder.time.setVisibility(View.GONE);
            layoutParams = (ViewGroup.MarginLayoutParams) holder.mainContent.getLayoutParams();
            layoutParams.bottomMargin = 10;

            if (!(memo.status == Memo.SENT)) {
                holder.image.setVisibility(View.INVISIBLE);
            }
        } else {
            holder.time.setVisibility(View.VISIBLE);
            layoutParams = (ViewGroup.MarginLayoutParams) holder.mainContent.getLayoutParams();
            layoutParams.bottomMargin = 0;
            if (!(memo.status == Memo.SENT) && !(memo.status == Memo.PENDING)) {
                holder.image.setVisibility(View.VISIBLE);
            }
        }

        if (hideDate) {
            holder.date.setVisibility(View.GONE);
        } else {
            holder.date.setVisibility(View.VISIBLE);
        }


        holder.time.setText(DateUtils.formatDateTime(context, memo.date.getTime(), DateUtils.FORMAT_SHOW_TIME));
        if (DateUtils.isToday(memo.date.getTime())) {
            holder.date.setText(R.string.today);
        } else if (com.dev.dita.daystarmemo.controller.utils.DateUtils.isYesterday(memo.date)) {
            holder.date.setText(R.string.yesterday);
        } else {
            holder.date.setText(DateUtils.formatDateTime(context, memo.date.getTime(), DateUtils.FORMAT_SHOW_DATE));
        }
        if (image != null) {
            holder.image.setImageBitmap(image);
        }

        holder.overlay.setVisibility(isSelected(position) ? View.VISIBLE : View.INVISIBLE);
    }

    @Override
    public void updateData(OrderedRealmCollection<Memo> data) {
        super.updateData(data);
        rowDateStates = data == null ? null : new int[data.size()];
        rowTimeStates = data == null ? null : new int[data.size()];
    }


    /**
     * Sets the alignment and color of memeo based on sender
     *
     * @param holder a ViewHolder
     * @param isMe   a boolean
     */
    private void setAlignment(ViewHolder holder, Boolean isMe) {
        if (!isMe) {
            holder.image.setVisibility(View.VISIBLE);


            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) holder.image.getLayoutParams();
            layoutParams.addRule(RelativeLayout.ALIGN_LEFT);
            holder.image.setLayoutParams(layoutParams);

            LinearLayout.LayoutParams layoutParams1 = (LinearLayout.LayoutParams) holder.mainContent.getLayoutParams();
            layoutParams1.gravity = Gravity.LEFT;
            holder.mainContent.setLayoutParams(layoutParams1);

            layoutParams1 = (LinearLayout.LayoutParams) holder.time.getLayoutParams();
            layoutParams1.gravity = Gravity.LEFT;
            holder.time.setLayoutParams(layoutParams1);
        } else {
            holder.image.setVisibility(View.GONE);


            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) holder.content.getLayoutParams();
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
            holder.content.setLayoutParams(layoutParams);

            LinearLayout.LayoutParams layoutParams1 = (LinearLayout.LayoutParams) holder.mainContent.getLayoutParams();
            layoutParams1.gravity = Gravity.RIGHT;
            holder.mainContent.setLayoutParams(layoutParams1);

            layoutParams1 = (LinearLayout.LayoutParams) holder.time.getLayoutParams();
            layoutParams1.gravity = Gravity.RIGHT;
            holder.time.setLayoutParams(layoutParams1);
        }
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }

    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public void removeItem(int position) {
        Log.i(TAG, "removing single");
        Memo.deleteMemoById(getItem(position).id);
        //notifyItemRemoved(position);
    }

    public void removeItems(List<Integer> positions) {
        Collections.sort(positions, new Comparator<Integer>() {
            @Override
            public int compare(Integer integer, Integer t1) {
                return t1 - integer;
            }
        });

        while (!positions.isEmpty()) {
            if (positions.size() == 1) {
                removeItem(positions.get(0));
                positions.remove(0);
            } else {
                int count = 1;
                while (positions.size() > count && positions.get(count).equals(positions.get(count - 1) - 1)) {
                    count++;
                }

                if (count == 1) {
                    removeItem(positions.get(0));
                } else {
                    removeRange(positions.get(count - 1), count);
                }

                for (int i = 0; i < count; i++) {
                    positions.remove(0);
                }
            }
        }
    }

    private void removeRange(int positionStart, int itemCount) {
        Log.i(TAG, "removing range");
        List<String> ids = new ArrayList<>();
        for (int i = 0; i < itemCount; i++) {
            ids.add(getItem(positionStart + i).id);
        }

        Memo.deleteMemosById(ids);

        //notifyItemRangeRemoved(positionStart, itemCount);
    }

    public void copySelectedItems(List<Integer> positions) {
        Collections.sort(positions);
        Calendar calendar = Calendar.getInstance();
        StringBuilder builder = new StringBuilder();
        for (Integer integer : positions) {
            Log.i(TAG, integer.toString());
            Memo memo = getItem(integer);
            calendar.setTime(memo.date);
            String date = String.format(Locale.getDefault(), "[%d/%d, %d:%d] ",
                    calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH),
                    calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE));
            String name = memo.sentByMe ? BaasUser.current().getName() : memo.connection;
            builder.append(date).append(name).append(": ").append(memo.body).append('\n');
        }

        ClipboardManager clipboardManager = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clipData = ClipData.newPlainText(context.getString(R.string.app_name), builder.toString());
        clipboardManager.setPrimaryClip(clipData);
        Toast.makeText(context, R.string.copied_to_clipboard, Toast.LENGTH_SHORT).show();
    }

    public interface ClickListener {
        void onItemClick(View view, int position);

        boolean onItemLongClick(View view, int position);
    }

    /**
     * The type View holder.
     */
    public static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.date)
        public TextView date;
        /**
         * The Message.
         */
        @BindView(R.id.message)
        public TextView message;
        /**
         * The Info.
         */
        @BindView(R.id.time)
        public TextView time;
        /**
         * The Content.
         */
        @BindView(R.id.content)
        public LinearLayout content;
        /**
         * The Main content.
         */
        @BindView(R.id.main_content)
        public CardView mainContent;
        /**
         * The Image.
         */
        @BindView(R.id.image)
        CircleImageView image;

        @BindView(R.id.selected_overlay)
        View overlay;


        /**
         * Instantiates a new View holder.
         *
         * @param view the view
         */
        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
