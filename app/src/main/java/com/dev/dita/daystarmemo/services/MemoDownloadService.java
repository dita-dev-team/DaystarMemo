package com.dev.dita.daystarmemo.services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.dev.dita.daystarmemo.controller.bus.MemoBus;
import com.dev.dita.daystarmemo.model.baas.MemoBaas;
import com.dev.dita.daystarmemo.model.database.MemoPendingDownload;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

public class MemoDownloadService extends Service {

    @Override
    public void onCreate() {
        super.onDestroy();
        EventBus.getDefault().register(this);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        String id = intent.getStringExtra("remoteId");
        // Download memo if queue is empty
        if (MemoPendingDownload.isEmpty()) {
            MemoBaas.fetchMemo(id);
        } else {
            // Add id to the back of the queue if queue is not empty
            MemoPendingDownload.add(id);
            // Start Memo Download service if its not running
            if (!MemoPendingDownloadService.isRunning) {
                Intent intent1 = new Intent(this, MemoPendingDownloadService.class);
                startService(intent1);
            }
        }
        return super.onStartCommand(intent, flags, startId);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(priority = 1)
    public void onEvent(MemoBus.FetchMemoResult result) {
        EventBus.getDefault().cancelEventDelivery(result);
        stopSelf();
    }
}
