package com.dev.dita.daystarmemo.model.database;

import io.realm.DynamicRealm;
import io.realm.FieldAttribute;
import io.realm.RealmMigration;
import io.realm.RealmObjectSchema;
import io.realm.RealmSchema;

public class DaystarMemoMigration implements RealmMigration {
    @Override
    public void migrate(DynamicRealm realm, long oldVersion, long newVersion) {
        RealmSchema schema = realm.getSchema();

        // Migrate to version 1 : change status field from String to int
        if (oldVersion == 0) {
            schema.get("Memo")
                    .removeField("status")
                    .addField("memoStatus", int.class);
            oldVersion++;
        }

        // Migrate to version 2 : rename field
        if (oldVersion == 1) {
            schema.get("Memo")
                    .renameField("memoStatus", "status");

            oldVersion++;
        }

        /*
            Version 3

            class GroupMember
                String username;
                String name;

            class Group
                @PrimaryKey
                String name;
                int type;
                int privacy;
                int interaction;
                RealmList<GroupMember> owners;
                RealmList<GroupMember> members;

         */

        // Migrate to version 3
        if (oldVersion == 2) {
            RealmObjectSchema memberSchema;
            if (!schema.contains("GroupMember")) {
                memberSchema = schema.create("GroupMember")
                        .addField("username", String.class, FieldAttribute.PRIMARY_KEY)
                        .addField("name", String.class);
            } else {
                memberSchema = schema.get("GroupMember");
            }


            if (!schema.contains("Group")) {
                schema.create("Group")
                        .addField("name", String.class, FieldAttribute.PRIMARY_KEY)
                        .addField("type", int.class)
                        .addField("privacy", int.class)
                        .addField("interaction", int.class)
                        .addRealmListField("owners", memberSchema)
                        .addRealmListField("members", memberSchema);
            }

            oldVersion++;
        }

        // Migrate to version 4
        if (oldVersion == 3) {
            schema.get("Schedule")
                    .addField("dayIndex", int.class, FieldAttribute.INDEXED);
            oldVersion++;
        }
    }
}
