/*
package com.dev.dita.daystarmemo.ui.main;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.baasbox.android.BaasUser;
import com.baasbox.android.json.JsonObject;
import com.dev.dita.daystarmemo.PrefSettings;
import com.dev.dita.daystarmemo.R;
import com.dev.dita.daystarmemo.controller.bus.UserBus;
import com.dev.dita.daystarmemo.controller.utils.ImageUtils;
import com.dev.dita.daystarmemo.controller.utils.UIUtils;
import com.dev.dita.daystarmemo.model.baas.UserBaas;
import com.dev.dita.daystarmemo.services.ConnectionService;
import com.dev.dita.daystarmemo.services.RegistrationService;
import com.dev.dita.daystarmemo.ui.connections.ConnectionsActivity;
import com.dev.dita.daystarmemo.ui.documents.DocumentsActivity;
import com.dev.dita.daystarmemo.ui.memos.MemoGroupActivity;
import com.dev.dita.daystarmemo.ui.profile.ProfileActivity;
import com.dev.dita.daystarmemo.ui.settings.SettingsActivity;
import com.dev.dita.daystarmemo.ui.welcome.WelcomeActivity;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import io.realm.Realm;

*/
/**
 * The type Main activity.
 * <p/>
 * The User name.
 * <p/>
 * The User email.
 * <p/>
 * The User image.
 * <p/>
 * The Swipe refresh layout.
 * <p/>
 * Init.
 * <p/>
 * On event.
 *
 * @param logoutResult the logout result
 * <p/>
 * On event.
 * @param profileUpdatedEvent the profile updated event
 * <p/>
 * Init user.
 *//*

public class MainActivity_backup extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private static final String TAG = MainActivity_backup.class.getName();

    */
/**
 * The User name.
 *//*

    TextView userName;
    */
/**
 * The User email.
 *//*

    TextView userEmail;
    */
/**
 * The User image.
 *//*

    CircleImageView userImage;

    */
/**
 * The Swipe refresh layout.
 *//*

    @BindView(R.id.main_refresh_animation)
    SwipeRefreshLayout swipeRefreshLayout;


    */
/**
 * Init.
 *//*

    public void init() {
        if (!ConnectionService.isRunning) {
            startService(new Intent(MainActivity_backup.this, ConnectionService.class));
        }
        swipeRefreshLayout.setColorSchemeResources(R.color.baseColor1, R.color.baseColor2);
        UIUtils.setAnimation(swipeRefreshLayout, false);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View headerView = navigationView.getHeaderView(0);

        userImage = (CircleImageView) headerView.findViewById(R.id.nav_profile_image);
        userImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity_backup.this, ProfileActivity.class));
            }
        });

        userName = (TextView) headerView.findViewById(R.id.nav_name);
        userEmail = (TextView) headerView.findViewById(R.id.nav_email);
        */
/*initUser();
        Intent intent = new Intent(this, RegistrationService.class);
        intent.putExtra("isRegistering", true);
        startService(intent);*//*

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        EventBus.getDefault().register(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (!PrefSettings.isLoggedIn(this)) {
            startActivity(new Intent(this, WelcomeActivity.class));
            finish();
            return;
        }

        init();
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_memo) {
            startActivity(new Intent(MainActivity_backup.this, MemoGroupActivity.class));
        } else if (id == R.id.nav_connections) {
            startActivity(new Intent(MainActivity_backup.this, ConnectionsActivity.class));
        } else if (id == R.id.nav_documents) {
            startActivity(new Intent(MainActivity_backup.this, DocumentsActivity.class));
        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_settings) {
            startActivity(new Intent(MainActivity_backup.this, SettingsActivity.class));
        } else if (id == R.id.nav_logout) {
            // Show confirmation dialog to confirm logging out
            new AlertDialog.Builder(this)
                    .setTitle("Logging out")
                    .setMessage("Are you sure?")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            UIUtils.setAnimation(swipeRefreshLayout, true);
                            UserBaas.logoutUser();

                        }
                    })
                    .setNegativeButton("No", null)
                    .show();

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();

        Intent intent = new Intent(this, RegistrationService.class);
        intent.putExtra("isRegistering", true);
        startService(intent);
        initUser();
        Log.i(TAG, "RESUMED");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i(TAG, "PAUSED");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i(TAG, "STOPPED");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
        Log.i(TAG, "DESTROYED");
    }

    */
/**
 * On event.
 *
 * @param logoutResult the logout result
 *//*

    @Subscribe
    public void onEvent(UserBus.LogoutResult logoutResult) {
        UIUtils.setAnimation(swipeRefreshLayout, false);
        if (logoutResult.error) {
            Toast.makeText(this, "Unable to logout", Toast.LENGTH_SHORT).show();
        } else {
            //PrefSettings.setLoggedIn(this, false);
            Intent intent = new Intent(this, RegistrationService.class);
            intent.putExtra("isRegistering", false);
            startService(intent);
            PrefSettings.clear(this);
            Realm realm = Realm.getDefaultInstance();
            realm.beginTransaction();
            realm.deleteAll();
            realm.commitTransaction();
            startActivity(new Intent(this, WelcomeActivity.class));
            finish();
        }
    }

    */
/**
 * On event.
 *
 * @param profileUpdatedEvent the profile updated event
 *//*

    @Subscribe
    public void onEvent(UserBus.ProfileUpdatedEvent profileUpdatedEvent) {
        initUser();
    }

    */
/**
 * Init user.
 *//*

    public void initUser() {
        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... params) {
                Context context = MainActivity_backup.this;
                // Set the default settings if none exist yet
                if (!PrefSettings.keyExists(context)) {
                    JsonObject details = BaasUser.current().getScope(BaasUser.Scope.FRIEND);
                    PrefSettings.setValue(context, "username", BaasUser.current().getName());
                    PrefSettings.setValue(context, "name", details.getString("name", getString(R.string.default_name)));
                    PrefSettings.setValue(context, "email", details.getString("email", getString(R.string.default_email)));
                    PrefSettings.setValue(context, "password", BaasUser.current().getPassword());
                    PrefSettings.setValue(context, "token", BaasUser.current().getToken());
                    byte[] imageArray = details.getBinary("image", null);
                    if (imageArray != null) {
                        PrefSettings.setValue(context, "image", ImageUtils.byteArrayToString(imageArray));
                    } else {
                        PrefSettings.setValue(context, "image", "");
                    }
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                Context context = MainActivity_backup.this;
                // Load the profile from setting
                userName.setText(PrefSettings.getStringValue(context, "name"));
                userEmail.setText(PrefSettings.getStringValue(context, "email"));
                String imageSrc = PrefSettings.getStringValue(context, "image");
                if (!TextUtils.isEmpty(imageSrc)) {
                    Bitmap image = ImageUtils.decodeBitmapFromString(imageSrc);
                    if (image != null) {
                        userImage.setImageBitmap(image);
                    }
                } else {
                    userImage.setImageDrawable(getResources().getDrawable(R.drawable.default_profile));
                }
            }
        }.execute();
    }
}
*/
