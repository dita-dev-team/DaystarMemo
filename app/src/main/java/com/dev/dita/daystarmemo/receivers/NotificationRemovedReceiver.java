package com.dev.dita.daystarmemo.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.dev.dita.daystarmemo.controller.DaystarMemoNotification;

public class NotificationRemovedReceiver extends BroadcastReceiver {
    public NotificationRemovedReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        // This method is called when the BroadcastReceiver is receiving
        // an Intent broadcast.
        DaystarMemoNotification.memoNotificationsNo = 0;
        DaystarMemoNotification.notifications.clear();
        DaystarMemoNotification.sendersList.clear();
    }
}
