package com.dev.dita.daystarmemo.ui.connections;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.baasbox.android.BaasUser;
import com.dev.dita.daystarmemo.R;
import com.dev.dita.daystarmemo.controller.utils.ImageUtils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class NewConnectionsListAdapter extends RecyclerView.Adapter<NewConnectionsListAdapter.ViewHolder> {

    final Context context;
    final LayoutInflater inflater;
    OnItemClickListener itemClickListener;
    private HashSet<String> connections;
    private HashSet<String> followers;
    private List<BaasUser> data;
    private List<BaasUser> dataSearchList;
    private int addedColor;

    public NewConnectionsListAdapter(Context context, List<BaasUser> objects) {
        this.context = context;
        inflater = LayoutInflater.from(this.context);
        data = objects;
        dataSearchList = new ArrayList<>();
        dataSearchList.addAll(objects);
        addedColor = Color.parseColor("#c4c4c4");
    }

    public void setConnections(HashSet<String> connections) {
        this.connections = connections;
    }

    public void setFollowers(HashSet<String> followers) {
        this.followers = followers;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.new_connection_list_item, parent, false);
        final ViewHolder viewHolder = new ViewHolder(view);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemClickListener.onItemClick(view, viewHolder.getLayoutPosition());
            }
        });
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        BaasUser user = data.get(position);
        String username = user.getName();
        String name = "";
        byte[] imageSrc = null;

        if (user.getScope(BaasUser.Scope.FRIEND) != null) {
            name = user.getScope(BaasUser.Scope.FRIEND).getString("name", "");
            imageSrc = user.getScope(BaasUser.Scope.FRIEND).getBinary("image", null);
        }

        holder.username.setText(username);
        holder.name.setText(name);

        if (imageSrc != null) {
            Bitmap image = ImageUtils.decodeBitmapFromByteArray(imageSrc);
            holder.image.setImageBitmap(image);
        } else {
            holder.image.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.default_profile));
        }

        holder.username.setTextColor(Color.BLACK);

        if (followers != null && followers.contains(username)) {
            holder.username.setText(username + " (follows you)");
        }

        if (connections.contains(username)) {
            holder.username.setText(username + " (added)");
            holder.username.setTextColor(addedColor);
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setOnItemClickListener(OnItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    public BaasUser getItem(int position) {
        return data.get(position);
    }

    public void filter(String text) {
        text = text.toLowerCase();

        data.clear();
        if (text.length() == 0) {
            data.addAll(dataSearchList);
        } else {
            for (BaasUser user : dataSearchList) {
                String username = user.getName();
                String name = null;
                if (user.getScope(BaasUser.Scope.FRIEND) != null) {
                    name = user.getScope(BaasUser.Scope.FRIEND).getString("name", "");
                }

                if (username.length() != 0 && username.toLowerCase().contains(text)) {
                    data.add(user);
                } else if (name != null) {
                    if (name.length() != 0 && name.toLowerCase().contains(text)) {
                        data.add(user);
                    }
                }
            }
        }
        notifyDataSetChanged();
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.username)
        TextView username;
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.thumbnail)
        CircleImageView image;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
