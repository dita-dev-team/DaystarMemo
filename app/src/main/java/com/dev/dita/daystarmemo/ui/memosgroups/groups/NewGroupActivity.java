package com.dev.dita.daystarmemo.ui.memosgroups.groups;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.baasbox.android.BaasDocument;
import com.dev.dita.daystarmemo.PrefSettings;
import com.dev.dita.daystarmemo.R;
import com.dev.dita.daystarmemo.controller.bus.GroupBus;
import com.dev.dita.daystarmemo.model.baas.GroupBaas;
import com.dev.dita.daystarmemo.model.database.Group;
import com.dev.dita.daystarmemo.model.objects.GroupContainer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.HashSet;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;

public class NewGroupActivity extends AppCompatActivity {
    private final String TAG = getClass().getName();

    @BindView(R.id.create_button)
    Button createButton;
    @BindView(R.id.list_view)
    RecyclerView listView;
    @BindView(R.id.empty)
    TextView empty;
    ProgressDialog progressDialog;

    NewGroupsListAdapter adapter;
    RealmResults<Group> groups;
    HashSet<String> localGroups;
    RealmChangeListener<RealmResults<Group>> listener = new RealmChangeListener<RealmResults<Group>>() {
        @Override
        public void onChange(RealmResults<Group> element) {
            if (adapter != null) {
                localGroups = new HashSet<>();
                for (Group group : groups) {
                    localGroups.add(group.name);
                }
                adapter.setGroups(localGroups);
                adapter.notifyDataSetChanged();
            }
        }
    };

    List<BaasDocument> groupList;

    private void init() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Fetching Groups ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);

        Realm realm = Realm.getDefaultInstance();
        groups = realm.where(Group.class).findAllAsync();

        String studentType = PrefSettings.getStringValue(this, "studentType");

        if (studentType.equals(getString(R.string.none)) || studentType.isEmpty()) {
            String message = getString(R.string.select_student_type);
            empty.setText(message);
            empty.setVisibility(View.VISIBLE);
        } else {
            GroupBaas.getAllGroups(studentType.toLowerCase());
            progressDialog.show();
        }

        setTitle("New group");

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_group);
        ButterKnife.bind(this);
        EventBus.getDefault().register(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        init();
    }

    @Subscribe
    public void onEvent(GroupBus.GetAllGroupsResult event) {
        progressDialog.dismiss();

        if (!event.error) {
            adapter = new NewGroupsListAdapter(this, event.groups);
            groupList = event.groups;
            localGroups = new HashSet<>();
            for (Group group : groups) {
                localGroups.add(group.name);
            }
            adapter.setGroups(localGroups);
            adapter.setOnItemClickListener(new NewGroupsListAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(View view, int position) {
                    final BaasDocument document = adapter.getItem(position);
                    final String groupName = document.getString("name", "");

                    if (!localGroups.contains(groupName)) {
                        new AlertDialog.Builder(NewGroupActivity.this)
                                .setMessage(String.format("Join %s?", groupName))
                                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        progressDialog.setMessage(String.format("Joining %s ...", groupName));
                                        progressDialog.show();
                                        GroupBaas.joinGroup(NewGroupActivity.this, document);
                                    }
                                })
                                .setNegativeButton(R.string.no, null)
                                .show();
                    }
                }
            });
            LinearLayoutManager layoutManager = new LinearLayoutManager(this);
            layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            listView.setLayoutManager(layoutManager);
            listView.setAdapter(adapter);
            empty.setVisibility(adapter.getItemCount() == 0 ? View.VISIBLE : View.GONE);
        } else {
            Toast.makeText(this, event.message, Toast.LENGTH_SHORT).show();
        }
    }

    @Subscribe
    public void onEvent(GroupBus.JoinGroupResult event) {
        if (!event.error) {
            Group.saveGroup(event.document);
        } else {
            progressDialog.dismiss();
            Toast.makeText(this, "Failed to join group", Toast.LENGTH_SHORT).show();
        }
    }

    @Subscribe
    public void onEvent(GroupBus.SaveGroupResult event) {
        progressDialog.dismiss();
        Toast.makeText(this, "Joined group successfully", Toast.LENGTH_SHORT).show();
        // TODO: 25/09/16 Launch group details activity
    }

    @OnClick(R.id.create_button)
    public void onCreateButtonClicked() {
        Intent intent = new Intent(NewGroupActivity.this, CreateGroupActivity.class);
        intent.putExtra("groups", new GroupContainer(groupList));
        startActivity(intent);
        finish();
//        JsonArray array = new JsonArray();
//        array.add(BaasUser.current().getName());
//
//        JsonObject object = new JsonObject();
//        object.put("name", "TEST101");
//        object.put("owners", array);
//        object.put("members", array);
//        object.put("privacy", "open");
//        object.put("type", "academic");
//        object.put("interaction", "interactive");
//        GroupBaas.createGroup(this, object);
//        //Toast.makeText(this, "To be implemented", Toast.LENGTH_SHORT).show();
    }
}
