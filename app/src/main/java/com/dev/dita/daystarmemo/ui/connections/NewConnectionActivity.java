package com.dev.dita.daystarmemo.ui.connections;

import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.baasbox.android.BaasUser;
import com.dev.dita.daystarmemo.R;
import com.dev.dita.daystarmemo.controller.bus.UserBus;
import com.dev.dita.daystarmemo.model.baas.UserBaas;
import com.dev.dita.daystarmemo.model.database.Connection;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.HashSet;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;

public class NewConnectionActivity extends AppCompatActivity {

    final String TAG = getClass().getName();
    @BindView(R.id.new_connections_list_view)
    RecyclerView listView;
    @BindView(R.id.new_connections_empty)
    TextView empty;
    ProgressDialog progressDialog;

    NewConnectionsListAdapter adapter;
    Realm realm;
    RealmResults<Connection> connectionResults;
    HashSet<String> connections;
    HashSet<String> followers;
    RealmChangeListener<RealmResults<Connection>> listener = new RealmChangeListener<RealmResults<Connection>>() {
        @Override
        public void onChange(RealmResults<Connection> element) {
            if (adapter != null) {
                connections = new HashSet<>();
                for (Connection connection : element) {
                    connections.add(connection.username);
                }
                adapter.setConnections(connections);
                adapter.notifyDataSetChanged();
            }
        }
    };

    public void init() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Fetching Connections ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);

        realm = Realm.getDefaultInstance();
        connectionResults = realm.where(Connection.class).findAllAsync();
        connectionResults.addChangeListener(listener);
        setTitle("Connections");

        UserBaas.getAllUsers();
        UserBaas.getFollowers();
        progressDialog.show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_connection);
        ButterKnife.bind(this);
        EventBus.getDefault().register(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.new_connections_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        init();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
        realm.close();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        NavUtils.navigateUpFromSameTask(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_new_connections, menu);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        //searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                adapter.filter(newText.trim());
                listView.invalidate();
                return true;
            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_refresh:
                UserBaas.getAllUsers();
                progressDialog.setMessage("Fetching Connections ...");
                progressDialog.show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Subscribe
    public void onEvent(UserBus.GetAllUsersResult result) {
        progressDialog.dismiss();
        if (!result.error) {
            // Remove your username from the list of fetched users
            for (BaasUser user : result.users) {
                if (user.getName().equals(BaasUser.current().getName())) {
                    result.users.remove(user);
                    break;
                }
            }
            adapter = new NewConnectionsListAdapter(this, result.users);
            connections = new HashSet<>();
            for (Connection connection : connectionResults) {
                connections.add(connection.username);
            }
            adapter.setConnections(connections);
            if (followers != null) {
                adapter.setFollowers(followers);
            }
            adapter.setOnItemClickListener(new NewConnectionsListAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(View view, int position) {
                    final BaasUser user = adapter.getItem(position);
                    if (!connections.contains(user.getName())) {
                        new AlertDialog.Builder(NewConnectionActivity.this)
                                .setMessage(String.format("Add %s to your connections?", user.getName()))
                                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        progressDialog.setMessage("Adding Connection ...");
                                        progressDialog.show();
                                        UserBaas.addConnectionToProfile(user);
                                    }
                                })
                                .setNegativeButton(R.string.no, null)
                                .show();
                    }
                }
            });
            LinearLayoutManager layoutManager = new LinearLayoutManager(this);
            layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            listView.setLayoutManager(layoutManager);
            listView.setAdapter(adapter);
            empty.setVisibility(adapter.getItemCount() == 0 ? View.VISIBLE : View.GONE);
        } else {
            Toast.makeText(this, "Unable to connect", Toast.LENGTH_SHORT).show();
        }
    }

    @Subscribe
    public void onEvent(UserBus.AddConnectionResult connectionResult) {
        if (!connectionResult.error) {
            Connection.addUser(connectionResult.user);
        } else {
            progressDialog.dismiss();
            Toast.makeText(this, "Failed to add connection", Toast.LENGTH_SHORT).show();
        }
    }

    @Subscribe
    public void onEvent(UserBus.AddConnectionToDbResult connectionToDbResult) {
        progressDialog.dismiss();
        if (!connectionToDbResult.error) {
            adapter.setConnections(connections);
            Toast.makeText(this, "New connection added", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Failed to add connection", Toast.LENGTH_SHORT).show();
        }
    }

    @Subscribe
    public void onEvent(UserBus.GetFollowersResult result) {
        if (!result.error) {
            followers = new HashSet<>();
            for (BaasUser user : result.users) {
                followers.add(user.getName());
            }
            if (adapter != null) {
                adapter.setFollowers(followers);
                new Handler(Looper.getMainLooper()).post(
                        new Runnable() {
                            @Override
                            public void run() {
                                adapter.notifyDataSetChanged();
                            }
                        }
                );
            }
        }
    }
}
