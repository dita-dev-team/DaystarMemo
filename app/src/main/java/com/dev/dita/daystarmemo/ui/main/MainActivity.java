package com.dev.dita.daystarmemo.ui.main;

import android.content.Intent;
import android.graphics.Point;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.widget.TextView;
import android.widget.Toast;

import com.dev.dita.daystarmemo.PrefSettings;
import com.dev.dita.daystarmemo.R;
import com.dev.dita.daystarmemo.controller.bus.AssetBus;
import com.dev.dita.daystarmemo.controller.utils.DateUtils;
import com.dev.dita.daystarmemo.model.baas.AssetBaas;
import com.dev.dita.daystarmemo.model.database.Schedule;
import com.dev.dita.daystarmemo.services.ConnectionService;
import com.dev.dita.daystarmemo.services.RegistrationService;
import com.dev.dita.daystarmemo.services.ScheduleService;
import com.dev.dita.daystarmemo.ui.BaseActivity;
import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.Target;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Case;
import io.realm.Realm;
import io.realm.RealmResults;

/**
 * The type Main activity.
 */
public class MainActivity extends BaseActivity {

    private static final float PERCENTAGE_TO_SHOW_TITLE_AT_TOOLBAR = 0.9f;
    private static final float PERCENTAGE_TO_HIDE_TITLE_DETAILS = 0.3f;
    private static final int ALPHA_ANIMATIONS_DURATION = 200;

    private static final String TAG = MainActivity.class.getName();
    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    @BindView(R.id.app_bar)
    AppBarLayout appBarLayout;
    @BindView(R.id.collapsing_toolbar)
    CollapsingToolbarLayout collapsingToolbarLayout;
    @BindView(R.id.title_expanded)
    TextView titleExpanded;
    @BindView(R.id.surface_view)
    SurfaceView surfaceView;
    @BindView(R.id.schedule_view)
    CardView scheduleView;
    @BindView(R.id.list_view)
    RecyclerView listView;
    @BindView(R.id.schedule)
    TextView schedule;

    private SurfaceHolder surfaceHolder;
    private MediaPlayer mediaPlayer;
    private Toolbar toolbar;
    private FileInputStream inputStream;
    private MainAdapter adapter;
    private ShowcaseView showcaseView;

    public static void startAlphaAnimation(View view, long duration, int visibility) {
        AlphaAnimation alphaAnimation = visibility == View.VISIBLE ? new AlphaAnimation(0f, 1f) : new AlphaAnimation(1f, 0f);
        alphaAnimation.setDuration(duration);
        alphaAnimation.setFillAfter(true);
        view.startAnimation(alphaAnimation);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        EventBus.getDefault().register(this);
        toolbar = getActionBarToolbar();
        toolbar.setNavigationIcon(R.drawable.ic_back_white);
        toolbar.setNavigationContentDescription(R.string.close_and_go_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                navigateUpOrBack(MainActivity.this, null);
            }
        });
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        drawerLayout.addDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {

            }

            @Override
            public void onDrawerOpened(View drawerView) {

            }

            @Override
            public void onDrawerClosed(View drawerView) {

            }

            @Override
            public void onDrawerStateChanged(int newState) {
                if (showcaseView != null) {
                    showcaseView.hide();
                }
            }
        });
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                int maxScroll = appBarLayout.getTotalScrollRange();
                float percentage = (float) Math.abs(verticalOffset) / (float) maxScroll;


                if (percentage >= PERCENTAGE_TO_SHOW_TITLE_AT_TOOLBAR) {
                    collapsingToolbarLayout.setTitle(getString(R.string.app_name));
                    //mediaPlayer.pause();
                } else {
                    collapsingToolbarLayout.setTitle("");
                    //mediaPlayer.start();
                }
            }
        });
        surfaceHolder = surfaceView.getHolder();

        surfaceHolder.addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder surfaceHolder) {
                if (PrefSettings.getBooleanValue(MainActivity.this, "enable_homescreen_video")) {
                    prepareVideo();
                }

            }

            @Override
            public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {

            }

            @Override
            public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
                if (mediaPlayer != null) {
                    if (mediaPlayer.isPlaying()) {
                        mediaPlayer.stop();
                    }
                }
            }
        });
        if (!isGuestSession) {
            startService(new Intent(MainActivity.this, ConnectionService.class));
            startService(new Intent(MainActivity.this, ScheduleService.class));
        } else {
            scheduleView.setVisibility(View.GONE);
        }

        GridLayoutManager layoutManager = new GridLayoutManager(this, 3);
        listView.setLayoutManager(layoutManager);
        adapter = new MainAdapter(this, isGuestSession);
        adapter.setItemClickListener(new MainAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                MainAdapter.Item item = adapter.items.get(position);

                if (item.label.equalsIgnoreCase("about")) {
                    startActivity(new Intent(MainActivity.this, AboutActivity.class));
                } else if (item.label.equalsIgnoreCase("messages")) {
                    onNavDrawerItemClicked(NAVDRAWER_ITEM_MEMOS);
                } else if (item.label.equalsIgnoreCase("events")) {
                    Toast.makeText(MainActivity.this, "To be implemented", Toast.LENGTH_SHORT).show();
                } else if (item.label.equalsIgnoreCase("academic programmes")) {
                    startActivity(new Intent(MainActivity.this, ProgrammesActivity.class));
                }

            }
        });
        listView.setAdapter(adapter);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.main, menu);
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (!isGuestSession) {
            Intent intent = new Intent(this, RegistrationService.class);
            intent.putExtra("isRegistering", true);
            startService(intent);
        }

        initUser();
        getSchedule();
        showHelpScreen();
        Log.i(TAG, "RESUMED");
    }

    @Override
    protected void onPause() {
        super.onPause();
/*
        if (mediaPlayer.isPlaying()) {
            mediaPlayer.pause();
        }
*/
        Log.i(TAG, "PAUSED");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
        if (mediaPlayer != null && mediaPlayer.isPlaying()) {
            mediaPlayer.stop();
        }

        Log.i(TAG, "DESTROYED");
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout != null && drawerLayout.isDrawerOpen(GravityCompat.END)) {
            drawerLayout.closeDrawer(GravityCompat.END);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected int getSelfNavDrawerItem() {
        return NAVDRAWER_ITEM_HOME;
    }

    private void prepareVideo() {
        Log.i(TAG, "PREPPING VIDEO");
        try {
            inputStream = openFileInput(AssetBaas.videoFileName);

            if (mediaPlayer == null) {
                mediaPlayer = new MediaPlayer();
            }

            if (!inputStream.getFD().valid()) {
                Log.i(TAG, "NOT VALID");
                inputStream.close();
                AssetBaas.getMainVideoFile(this);
                return;
            }
            mediaPlayer.reset();
            mediaPlayer.setDataSource(inputStream.getFD());

            mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mediaPlayer) {
                    Log.i(TAG, "PLAYING");
                    mediaPlayer.setVolume(0, 0);
                    mediaPlayer.setLooping(true);
                    mediaPlayer.start();
                }
            });
            mediaPlayer.setDisplay(surfaceHolder);
            mediaPlayer.prepareAsync();
            inputStream.close();
        } catch (FileNotFoundException e) {
            AssetBaas.getMainVideoFile(this);
        } catch (IOException e) {
            Log.i(TAG, "SOMETHING IS WRONG WITH THE VIDEO FILE");
            if (!PrefSettings.mainVideoDownloaded(this)) {
                AssetBaas.getMainVideoFile(this);
            }
            e.printStackTrace();
        }
    }

    @Subscribe
    public void onEvent(AssetBus.VideoFileFetched event) {
        prepareVideo();
    }

    private void getSchedule() {
        if (isGuestSession) {
            return;
        }

        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... voids) {
                Realm realm = Realm.getDefaultInstance();
                String day = DateUtils.getCurrentDay();
                RealmResults<Schedule> schedules = realm.where(Schedule.class).equalTo("attending", true).equalTo("day", day, Case.INSENSITIVE).findAllSorted("dayIndex");
                Date now = new Date();
                if (!schedules.isEmpty()) {
                    StringBuilder builder = new StringBuilder();

                    for (Schedule schedule : schedules) {
                        if (DateUtils.compareTimes(schedule.endTime, now) == -1) {
                            System.out.println("CONTINUING");
                            continue;
                        }

                        if (DateUtils.compareTimes(schedule.startTime, now) <= 0 && DateUtils.compareTimes(schedule.endTime, now) >= 0) {
                            builder.append("<b>CURRENT</b>: ")
                                    .append(schedule.title)
                                    .append('\n');
                        } else {
                            String startTime = DateUtils.getTimeStringFromDate(schedule.startTime);
                            builder.append("<b>").append(startTime).append("</b>: ")
                                    .append(schedule.title)
                                    .append('\n');
                        }
                    }
                    if (!builder.toString().isEmpty()) {
                        return builder.toString();
                    }

                }
                return null;
            }

            @Override
            protected void onPostExecute(String string) {
                if (string == null) {
                    return;
                }
                schedule.setText(Html.fromHtml(string));
                //schedule.setText(string);
                //if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                //    schedule.setText(Html.escapeHtml(string));
                //} else {
                //
                //}
            }
        }.execute();
    }

    private void showHelpScreen() {
        String studentType = PrefSettings.getStringValue(this, "studentType");
        if (!isGuestSession && studentType.equals(getString(R.string.none))) {
            final Target homeTarget = new Target() {
                @Override
                public Point getPoint() {
                    int actionBarSize = toolbar.getHeight();
                    int x = actionBarSize / 2;
                    int y = actionBarSize / 2;
                    return new Point(x, y);
                }
            };

            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    showcaseView = new ShowcaseView.Builder(MainActivity.this)
                            .setTarget(homeTarget)
                            .setContentTitle("Profile")
                            .setContentText("Please select student type in your profile")
                            .setStyle(R.style.ShowcaseViewTheme)
                            .withNewStyleShowcase()
                            .hideOnTouchOutside()
                            .build();
                    showcaseView.show();
                }
            }, 2000);

        }
    }

}
