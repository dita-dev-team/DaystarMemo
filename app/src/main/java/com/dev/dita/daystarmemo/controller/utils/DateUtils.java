package com.dev.dita.daystarmemo.controller.utils;


import com.crashlytics.android.Crashlytics;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DateUtils {

    public static boolean timesEqual(Date date1, Date date2) {
        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(date1);
        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(date2);
        return datesEqual(cal1, cal2) && cal1.get(Calendar.HOUR) == cal2.get(Calendar.HOUR) &&
                cal1.get(Calendar.MINUTE) == cal2.get(Calendar.MINUTE);
    }

    public static boolean datesEqual(Date date1, Date date2) {
        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(date1);
        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(date2);
        return cal1.get(Calendar.DATE) == cal2.get(Calendar.DATE);
    }

    private static boolean datesEqual(Calendar cal1, Calendar cal2) {
        return cal1.get(Calendar.DATE) == cal2.get(Calendar.DATE);
    }

    public static boolean isYesterday(Date date) {
        Calendar now = Calendar.getInstance();
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);

        return now.get(Calendar.DATE) - cal.get(Calendar.DATE) == 1;
    }

    public static Date scheduleTimeParser(String time) {
        Date date = new Date();
        SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mma", Locale.getDefault());
        try {
            date = parseFormat.parse(time);
        } catch (ParseException e) {
            Crashlytics.logException(e);
            e.printStackTrace();
        }
        return date;
    }

    public static String getTimeStringFromDate(Date date) {
        String time = null;
        SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mma", Locale.getDefault());
        time = parseFormat.format(date);
        return time;
    }

    public static String getCurrentDay() {
        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_WEEK);

        switch (day) {
            case Calendar.SUNDAY:
                return "sunday";
            case Calendar.MONDAY:
                return "monday";
            case Calendar.TUESDAY:
                return "tuesday";
            case Calendar.WEDNESDAY:
                return "wednesday";
            case Calendar.THURSDAY:
                return "thursday";
            case Calendar.FRIDAY:
                return "friday";
            case Calendar.SATURDAY:
                return "saturday";
            default:
                return "";
        }
    }

    public static int compareTimes(Date date1, Date date2) {
        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(date1);
        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(date2);
        int returnValue = 0;
        System.out.println("VALUE1: " + cal1.get(Calendar.HOUR_OF_DAY));
        System.out.println("VALUE2: " + cal2.get(Calendar.HOUR_OF_DAY));
        if (cal1.get(Calendar.HOUR_OF_DAY) == cal2.get(Calendar.HOUR_OF_DAY)) {
            if (cal1.get(Calendar.MINUTE) > cal2.get(Calendar.MINUTE)) {
                returnValue = 1;
            } else if (cal1.get(Calendar.MINUTE) < cal2.get(Calendar.MINUTE)) {
                returnValue = -1;
            } else if (cal1.get(Calendar.MINUTE) == cal2.get(Calendar.MINUTE)) {
                returnValue = 0;
            }
        } else {
            if (cal1.get(Calendar.HOUR_OF_DAY) > cal2.get(Calendar.HOUR_OF_DAY)) {
                returnValue = 1;
            } else if (cal1.get(Calendar.HOUR_OF_DAY) < cal2.get(Calendar.HOUR_OF_DAY)) {
                returnValue = -1;
            }
        }
        System.out.println("VALUE: " + returnValue);
        return returnValue;
        /*

        int time1;
        int time2;

        time1 = (int) (date1.getTime() % (24 * 60 * 60 * 1000L));
        time2 = (int) (date2.getTime() % (24 * 60 * 60 * 1000L));
        System.err.println("TIME1: "+(time1));
        System.err.println("TIME2: "+(time2));
        System.err.println(String.valueOf(time1-time2));
        return time1 - time2;
*/

    }
}
