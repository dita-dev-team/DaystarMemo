package com.dev.dita.daystarmemo.model.baas;


import android.content.Context;
import android.util.Log;

import com.baasbox.android.BaasAsset;
import com.baasbox.android.BaasHandler;
import com.baasbox.android.BaasResult;
import com.baasbox.android.DataStreamHandler;
import com.crashlytics.android.Crashlytics;
import com.dev.dita.daystarmemo.PrefSettings;
import com.dev.dita.daystarmemo.controller.bus.AssetBus;
import com.dev.dita.daystarmemo.controller.utils.ConnectionUtils;

import org.greenrobot.eventbus.EventBus;

import java.io.FileOutputStream;

public class AssetBaas {
    public static final String TAG = AssetBaas.class.getName();
    public static final String videoFileName = "video.mp4";
    public static String videoId = "app_video";
    private static boolean isRunning = false;

    public static void getMainVideoFile(final Context context) {
        if (isRunning) {
            return;
        }

        boolean wifiOnly = PrefSettings.getBooleanValue(context, "download_video_on_wifi_only");

        if (wifiOnly) {
            if (!ConnectionUtils.isWifiConnected(context)) {
                return;
            }
        }

        isRunning = true;
        Log.i(TAG, "GETTING VIDEO FILE");
        BaasAsset.streamAsset(videoId, new DataStreamHandler<BaasAsset>() {
            long total = 0;
            private FileOutputStream outputStream;

            @Override
            public void startData(String s, long l, String s1) throws Exception {
                outputStream = context.openFileOutput(videoFileName, Context.MODE_PRIVATE);
            }

            @Override
            public void onData(byte[] bytes, int i) throws Exception {
                total += i;
                outputStream.write(bytes, 0, i);
                Log.i(TAG, String.valueOf(i));
            }

            @Override
            public BaasAsset endData(String s, long l, String s1) throws Exception {
                outputStream.close();
                return null;
            }

            @Override
            public void finishStream(String s) {
                PrefSettings.setMainVideoDownloaded(context, true);
            }
        }, new BaasHandler<BaasAsset>() {
            @Override
            public void handle(BaasResult<BaasAsset> baasResult) {
                isRunning = false;
                if (baasResult.isSuccess()) {
                    Log.i(TAG, "VIDEO FILE FETCHED SUCCESSFULLY");
                    EventBus.getDefault().post(new AssetBus.VideoFileFetched());
                } else {
                    context.deleteFile(videoFileName);
                    Crashlytics.log(Log.DEBUG, TAG, baasResult.error().toString());
                }
            }
        });
    }
}
