package com.dev.dita.daystarmemo.model.objects;

import java.io.Serializable;

public class Notification implements Serializable {
    public final static int MESSAGE = 0;
    public final static int FILE = 1;
    public String sender;
    public String message;
    public String extraMessage;
    public String id;
    public int type;
    public long size;

    public Notification() {
    }

    public Notification(String sender, String message, String id) {
        this.sender = sender;
        this.message = message;
        this.id = id;
    }
}
