package com.dev.dita.daystarmemo.controller.utils;

public class StringUtils {

    public static String parseStringToCorrectJsonString(String src) {
        if (src != null) {
            int openingBraceIndex, closingBraceIndex;
            openingBraceIndex = src.lastIndexOf('{');
            closingBraceIndex = src.lastIndexOf('}');
            return src.substring(openingBraceIndex, closingBraceIndex);
        }
        return null;
    }
}
