package com.dev.dita.daystarmemo.ui.documents;

import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.dev.dita.daystarmemo.R;
import com.dev.dita.daystarmemo.controller.bus.MemoBus;
import com.dev.dita.daystarmemo.controller.utils.FileUtils;
import com.dev.dita.daystarmemo.model.database.Connection;
import com.dev.dita.daystarmemo.model.database.Transfer;
import com.dev.dita.daystarmemo.model.objects.Notification;
import com.dev.dita.daystarmemo.model.objects.Recipient;
import com.dev.dita.daystarmemo.services.TransfersService;
import com.dev.dita.daystarmemo.ui.BaseActivity;
import com.dev.dita.daystarmemo.ui.customviews.RecipientsCompletionView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

public class DocumentsActivity extends BaseActivity {

    public static final int RECEIVED = 0;

    public static final int SENT = 1;

    public static final int TRANSFERS = 2;
    private static final int FILE_SELECT_CODE = 0;
    private final BroadcastReceiver notificationReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Notification notification = (Notification) intent.getExtras().getSerializable("notification");
            assert notification != null;
            if (notification.type == Notification.FILE) {
                Transfer.addPendingDownload(notification);
                abortBroadcast();
                MediaPlayer mediaPlayer = MediaPlayer.create(DocumentsActivity.this, R.raw.incoming_memo_chat);
                mediaPlayer.start();
            }

        }
    };
    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;
    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;
    private DrawerLayout drawerLayout;
    private File file;
    private ArrayList<Recipient> recipientsList = new ArrayList<>();

    private void init() {
        IntentFilter filter = new IntentFilter("com.dev.dita.daystarmemo.BROADCAST_NOTIFICATION");
        filter.setPriority(1);
        registerReceiver(notificationReceiver, filter);
        //NewMemoNotification.cancel(this);
        sendBroadcast(new Intent("com.dev.dita.daystarmemo.BROADCAST_NOTIFICATION_REMOVED"));
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            int section = extras.getInt("section");
            mViewPager.setCurrentItem(section, true);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_documents);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        EventBus.getDefault().register(this);
        Toolbar toolbar = getActionBarToolbar();
        toolbar.setNavigationIcon(R.drawable.ic_back_white);
        toolbar.setNavigationContentDescription(R.string.close_and_go_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                navigateUpOrBack(DocumentsActivity.this, null);
            }
        });
        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.documents_container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.documents_tabs);
        tabLayout.setupWithViewPager(mViewPager);
        initUser();
    }

    @Override
    protected void onResume() {
        super.onResume();
        init();
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(notificationReceiver);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_documents, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (id) {
            case R.id.action_add:
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("*/*");
                intent.addCategory(Intent.CATEGORY_OPENABLE);

                try {
                    // Fetch connections list while user selects file
                    fetchConnections();
                    startActivityForResult(
                            Intent.createChooser(intent, getString(R.string.select_a_file)),
                            FILE_SELECT_CODE
                    );
                } catch (ActivityNotFoundException ex) {
                    Toast.makeText(this, R.string.no_file_manager_found, Toast.LENGTH_SHORT).show();
                }
                return true;
            case R.id.action_clear:
                final int section = mViewPager.getCurrentItem();
                switch (section) {
                    case 0:
                        EventBus.getDefault().post(new ClearReceivedDocuments());
                        break;
                    case 1:
                        EventBus.getDefault().post(new ClearSentDocuments());
                        break;
                    case 2:
                        EventBus.getDefault().post(new ClearTransfers());
                        break;
                }
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout != null && drawerLayout.isDrawerOpen(GravityCompat.END)) {
            drawerLayout.closeDrawer(GravityCompat.END);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected int getSelfNavDrawerItem() {
        return NAVDRAWER_ITEM_DOCUMENTS;
    }

    private Fragment getFragment(int position) {
        switch (position) {
            case 0:
                return new ReceivedDocumentsFragment();
            case 1:
                return new SentDocumentsFragment();
            case 2:
                return new TransfersFragment();
            default:
                return null;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case FILE_SELECT_CODE:
                if (resultCode == RESULT_OK) {
                    String mimeType = FileUtils.getMimeType(this, data.getData());
                    if (!FileUtils.isSupported(mimeType)) {
                        Toast.makeText(this, R.string.unsupported_file_format, Toast.LENGTH_SHORT).show();
                    } else {
                        showPreviewDialog(data.getData());
                    }
                }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Subscribe
    public void onEvent(MemoBus.StartTransferService start) {
        if (!TransfersService.isRunning) {
            startService(new Intent(this, TransfersService.class));
        }
    }

    private void showPreviewDialog(Uri uri) {
        String type = FileUtils.getGenericType(this, uri);
        String filepath = FileUtils.getPath(this, uri);
        assert filepath != null;
        file = new File(filepath);
        final Drawable drawable = FileUtils.getDrawableByFileType(this, type);
        LayoutInflater inflater = getLayoutInflater();
        View dialogLayout = inflater.inflate(R.layout.document_preview_with_recepient, null);
        TextView title = (TextView) dialogLayout.findViewById(R.id.document_title);
        ImageView icon = (ImageView) dialogLayout.findViewById(R.id.document_thumbnail);
        final RecipientsCompletionView recipients = (RecipientsCompletionView) dialogLayout.findViewById(R.id.recipients);
        recipients.performBestGuess(false);
        // Setup recipients
        if (recipientsList == null) {
            Log.i(getClass().getName(), "IS NULL");
        }
        recipients.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, recipientsList));
        title.setText(file.getName());
        icon.setImageDrawable(drawable);
        new AlertDialog.Builder(DocumentsActivity.this)
                .setTitle(R.string.send_document)
                .setView(dialogLayout)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        List<Recipient> recipientsList = recipients.getObjects();
                        if (recipientsList.isEmpty()) {
                            Toast.makeText(DocumentsActivity.this, R.string.no_recipients, Toast.LENGTH_SHORT).show();
                            return;
                        }
                        String[] usernames = new String[recipientsList.size()];
                        for (int i = 0; i < usernames.length; i++) {
                            usernames[i] = recipientsList.get(i).username;
                        }
                        TransfersService.addUpload(file, true, usernames);
                        dialog.dismiss();
                    }
                })
                .setNegativeButton(R.string.no, null)
                .show();
    }

    private void fetchConnections() {
        if (recipientsList.isEmpty()) {
            new AsyncTask<Void, Void, Void>() {
                @Override
                protected Void doInBackground(Void... voids) {
                    Realm realm = Realm.getDefaultInstance();
                    RealmResults<Connection> results = realm.where(Connection.class).findAll();
                    recipientsList = new ArrayList<>();
                    for (Connection connection : results) {
                        recipientsList.add(new Recipient(connection.username, connection.name));
                    }
                    return null;
                }

            }.execute();
        }
    }

    static class ClearSentDocuments {
    }

    static class ClearReceivedDocuments {
    }

    static class ClearTransfers {
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return getFragment(position);
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return getString(R.string.received);
                case 1:
                    return getString(R.string.sent);
                case 2:
                    return getString(R.string.transfers);
            }
            return null;
        }
    }
}
