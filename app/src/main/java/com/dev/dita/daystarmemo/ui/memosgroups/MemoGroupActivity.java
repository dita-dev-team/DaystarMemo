package com.dev.dita.daystarmemo.ui.memosgroups;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.dev.dita.daystarmemo.R;
import com.dev.dita.daystarmemo.controller.DaystarMemoNotification;
import com.dev.dita.daystarmemo.model.database.Memo;
import com.dev.dita.daystarmemo.model.database.Transfer;
import com.dev.dita.daystarmemo.model.objects.Notification;
import com.dev.dita.daystarmemo.services.MemoDownloadService;
import com.dev.dita.daystarmemo.ui.BaseActivity;
import com.dev.dita.daystarmemo.ui.documents.DocumentsActivity;
import com.dev.dita.daystarmemo.ui.memosgroups.groups.GroupsFragment;
import com.dev.dita.daystarmemo.ui.memosgroups.groups.NewGroupActivity;
import com.dev.dita.daystarmemo.ui.memosgroups.memos.MemosFragment;
import com.dev.dita.daystarmemo.ui.memosgroups.memos.NewMemoActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MemoGroupActivity extends BaseActivity {

    public static final int MEMO = 0;
    public static final int GROUP = 1;

    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;

    @BindView(R.id.main_content)
    CoordinatorLayout coordinatorLayout;

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    private BroadcastReceiver notificationReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Intent memo = new Intent(context, MemoDownloadService.class);
            Notification notification = (Notification) intent.getExtras().getSerializable("notification");
            if (notification.type == Notification.MESSAGE) {
                memo.putExtra("remoteId", notification.id);
                context.startService(memo);
            } else {
                Transfer.addPendingDownload(notification);
                showContextualNotification("You have received a new document");
            }
            abortBroadcast();
            MediaPlayer mediaPlayer = MediaPlayer.create(MemoGroupActivity.this, R.raw.incoming_memo_chat);
            mediaPlayer.start();
        }
    };

    private void init() {
        IntentFilter filter = new IntentFilter("com.dev.dita.daystarmemo.BROADCAST_NOTIFICATION");
        filter.setPriority(1);
        registerReceiver(notificationReceiver, filter);
        DaystarMemoNotification.cancel(this);
        sendBroadcast(new Intent("com.dev.dita.daystarmemo.BROADCAST_NOTIFICATION_REMOVED"));
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            mViewPager.setCurrentItem(extras.getInt("section"), true);
        }

        setTitle("");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_memo_group);
        ButterKnife.bind(this);
        Toolbar toolbar = getActionBarToolbar();
        toolbar.setNavigationIcon(R.drawable.ic_back_white);
        toolbar.setNavigationContentDescription(R.string.close_and_go_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                navigateUpOrBack(MemoGroupActivity.this, null);
            }
        });
        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                invalidateOptionsMenu();
                if (position == 0) {
                    setSelectedNavDrawerItem(NAVDRAWER_ITEM_MEMOS);
                } else {
                    setSelectedNavDrawerItem(NAVDRAWER_ITEM_GROUPS);
                }
            }

            @Override
            public void onPageSelected(int position) {
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);
        init();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_memo_group, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (id) {
            case R.id.action_new_memo:
                startActivity(new Intent(MemoGroupActivity.this, NewMemoActivity.class));
                return true;
            case R.id.action_memo_mark:
                Memo.markAsRead();
                return true;
            case R.id.action_memo_clear:
                new AlertDialog.Builder(MemoGroupActivity.this)
                        .setMessage(R.string.delete_all_memos)
                        .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                Memo.deleteMemo();
                            }
                        })
                        .setNegativeButton(R.string.no, null)
                        .show();
                return true;
            case R.id.action_new_group:
                startActivity(new Intent(MemoGroupActivity.this, NewGroupActivity.class));
                return true;
            case R.id.action_group_clear:
                Toast.makeText(this, "To be implemented", Toast.LENGTH_SHORT).show();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        int section = mViewPager.getCurrentItem();
        if (section == 0) {
            getMenuInflater().inflate(R.menu.menu_memos, menu);
        } else {
            getMenuInflater().inflate(R.menu.menu_groups, menu);
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    protected void onResume() {
        super.onResume();
        init();
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(notificationReceiver);
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout != null && drawerLayout.isDrawerOpen(GravityCompat.END)) {
            drawerLayout.closeDrawer(GravityCompat.END);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected int getSelfNavDrawerItem() {
        if (mViewPager.getCurrentItem() == 0) {
            return NAVDRAWER_ITEM_MEMOS;
        } else {
            return NAVDRAWER_ITEM_GROUPS;
        }
    }

    public Fragment getFragment(int position) {
        switch (position) {
            case 0:
                return new MemosFragment();
            case 1:
                return new GroupsFragment();
            default:
                return null;
        }
    }

    private void showContextualNotification(String message) {
        Snackbar snackbar = Snackbar.make(coordinatorLayout, message, Snackbar.LENGTH_LONG)
                .setAction("VIEW", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(MemoGroupActivity.this, DocumentsActivity.class);
                        intent.putExtra("section", DocumentsActivity.TRANSFERS);
                        startActivity(intent);
                    }
                });
        snackbar.setActionTextColor(ContextCompat.getColor(this, R.color.baseColor1));
        View view = snackbar.getView();
        view.setBackgroundColor(Color.WHITE);
        TextView textView = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.BLACK);
        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) view.getLayoutParams();
        params.gravity = Gravity.TOP;
        view.setLayoutParams(params);
        snackbar.show();
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return getFragment(position);
        }

        @Override
        public int getCount() {
            // Show 2 total pages.
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "MEMOS";
                case 1:
                    return "GROUPS";
            }
            return null;
        }
    }
}
