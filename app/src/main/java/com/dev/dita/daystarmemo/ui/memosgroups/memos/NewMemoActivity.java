package com.dev.dita.daystarmemo.ui.memosgroups.memos;

import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.baasbox.android.RequestToken;
import com.crashlytics.android.Crashlytics;
import com.dev.dita.daystarmemo.R;
import com.dev.dita.daystarmemo.controller.bus.MemoBus;
import com.dev.dita.daystarmemo.controller.utils.FileUtils;
import com.dev.dita.daystarmemo.controller.utils.UIUtils;
import com.dev.dita.daystarmemo.model.database.Connection;
import com.dev.dita.daystarmemo.model.objects.Recipient;
import com.dev.dita.daystarmemo.services.MemoService;
import com.dev.dita.daystarmemo.services.TransfersService;
import com.dev.dita.daystarmemo.ui.customviews.RecipientsCompletionView;
import com.dev.dita.daystarmemo.ui.documents.DocumentsActivity;
import com.tokenautocomplete.TokenCompleteTextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import hani.momanii.supernova_emoji_library.Actions.EmojIconActions;
import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;
import io.realm.Realm;
import io.realm.RealmResults;

/**
 * The type New memo activity.
 */
public class NewMemoActivity extends AppCompatActivity implements TokenCompleteTextView.TokenListener<Recipient> {

    private final int FILE_SELECT_CODE = 0;

    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.recipient)
    RecipientsCompletionView recipients;
    @BindView(R.id.edit)
    EmojiconEditText editText;
    @BindView(R.id.emoji_button)
    ImageButton emojiButton;
    @BindView(R.id.file_button)
    ImageButton fileButton;
    @BindView(R.id.send_button)
    ImageButton sendButton;
    @BindView(R.id.edit_view)
    View rootView;
    @BindView(R.id.coordinator_layout)
    CoordinatorLayout coordinatorLayout;

    private String filepath;
    private File file;

    private RequestToken requestToken;

    /**
     * Init.
     */
    private void init() {
        swipeRefreshLayout.setColorSchemeResources(R.color.baseColor1, R.color.baseColor2);
        UIUtils.setAnimation(swipeRefreshLayout, false);
        // Load all users from database
        Realm realm = Realm.getDefaultInstance();
        RealmResults<Connection> connections = realm.where(Connection.class).findAll();
        ArrayList<Recipient> recipientsList = new ArrayList<>();
        for (Connection connection : connections) {
            recipientsList.add(new Recipient(connection.username, connection.name));
        }
        recipients.performBestGuess(false);
        // Setup recipients
        recipients.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, recipientsList));
        recipients.setTokenListener(this);

        Bundle extras = getIntent().getExtras();
        String username;
        if (extras != null) {
            username = extras.getString("username");
            recipients.addObject(new Recipient(username, null));
            editText.requestFocus();
        }

        // Setup emojis
        EmojIconActions actions = new EmojIconActions(this, rootView, editText, emojiButton);
        actions.ShowEmojIcon();

        emojiButton.setEnabled(false);
        fileButton.setEnabled(false);
        sendButton.setEnabled(false);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_memo);
        ButterKnife.bind(this);
        EventBus.getDefault().register(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // Setup custom toolbar
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);

        LayoutInflater inflater = LayoutInflater.from(this);
        View view = inflater.inflate(R.layout.memos_actionbar, null);
        ImageButton closeButton = (ImageButton) view.findViewById(R.id.new_memo_close);
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO: 20/07/16 Implement close button functionality while in sending memo
                finish();
            }
        });

        getSupportActionBar().setCustomView(view);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        startService(new Intent(this, MemoService.class));
        init();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onTokenAdded(Recipient token) {
        // Enable buttons when a recipient is added
        emojiButton.setEnabled(true);
        fileButton.setEnabled(true);
        sendButton.setEnabled(true);
    }

    @Override
    public void onTokenRemoved(Recipient token) {
        // Disable buttons if recipient list is empty
        if (!hasRecipient()) {
            emojiButton.setEnabled(false);
            fileButton.setEnabled(false);
            sendButton.setEnabled(false);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case FILE_SELECT_CODE:
                if (resultCode == RESULT_OK) {
                    UIUtils.hideKeyboard(this);
                    String mimeType = FileUtils.getMimeType(this, data.getData());
                    if (!FileUtils.isSupported(mimeType)) {
                        Toast.makeText(this, R.string.unsupported_file_format, Toast.LENGTH_SHORT).show();
                    } else {
                        showPreviewDialog(data.getData());
                    }
                }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @OnClick(R.id.file_button)
    public void onFileButtonClicked() {
        UIUtils.hideKeyboard(this);
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("*/*");
        intent.addCategory(Intent.CATEGORY_OPENABLE);

        try {
            startActivityForResult(
                    Intent.createChooser(intent, getString(R.string.select_a_file)),
                    FILE_SELECT_CODE
            );
        } catch (ActivityNotFoundException ex) {
            Crashlytics.logException(ex);
            Toast.makeText(this, R.string.no_file_manager_found, Toast.LENGTH_SHORT).show();
        }
    }

    @OnClick(R.id.send_button)
    public void onSendButtonClicked() {
        final String text = editText.getText().toString();

        if (!TextUtils.isEmpty(text)) {
            UIUtils.setAnimation(swipeRefreshLayout, true);
            List<Recipient> recipientsList = recipients.getObjects();
            String[] usernames = new String[recipientsList.size()];
            for (int i = 0; i < recipientsList.size(); i++) {
                usernames[i] = recipientsList.get(i).username;
            }
            MemoService.sendMemo(text, false, usernames);
        }
    }


    @Subscribe
    public void onEvent(MemoBus.SendBroadcastMemoResult result) {
        UIUtils.setAnimation(swipeRefreshLayout, false);
        if (result.error) {
            Toast.makeText(this, R.string.memo_failed, Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, R.string.memo_sent, Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    private boolean hasRecipient() {
        return recipients.getObjects().size() > 0;
    }

    private void showPreviewDialog(Uri uri) {
        String type = FileUtils.getGenericType(this, uri);
        filepath = FileUtils.getPath(this, uri);
        file = new File(filepath);
        final Drawable drawable = FileUtils.getDrawableByFileType(this, type);
        LayoutInflater inflater = getLayoutInflater();
        View dialogLayout = inflater.inflate(R.layout.document_preview, null);
        TextView title = (TextView) dialogLayout.findViewById(R.id.document_title);
        ImageView icon = (ImageView) dialogLayout.findViewById(R.id.document_thumbnail);
        title.setText(file.getName());
        icon.setImageDrawable(drawable);
        new AlertDialog.Builder(NewMemoActivity.this)
                .setTitle(R.string.send_document)
                .setView(dialogLayout)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // prepare upload
                        List<Recipient> recipientsList = recipients.getObjects();
                        String[] usernames = new String[recipientsList.size()];
                        for (int i = 0; i < recipientsList.size(); i++) {
                            usernames[i] = recipientsList.get(i).username;
                        }
                        TransfersService.addUpload(file, true, usernames);
                        showContextualNotification(getString(R.string.document_is_being_sent));
                    }
                })
                .setNegativeButton(R.string.no, null)
                .show();
    }

    private void showContextualNotification(String message) {
        Snackbar snackbar = Snackbar.make(coordinatorLayout, message, Snackbar.LENGTH_LONG)
                .setAction(R.string.view, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(NewMemoActivity.this, DocumentsActivity.class);
                        intent.putExtra("section", DocumentsActivity.TRANSFERS);
                        startActivity(intent);
                    }
                });
        snackbar.setActionTextColor(ContextCompat.getColor(this, R.color.baseColor1));
        View view = snackbar.getView();
        view.setBackgroundColor(Color.WHITE);
        TextView textView = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.BLACK);
        snackbar.show();
    }
}
