package com.dev.dita.daystarmemo.controller.utils;

import android.content.Context;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.webkit.MimeTypeMap;

import com.crashlytics.android.Crashlytics;
import com.dev.dita.daystarmemo.R;

import java.io.File;
import java.net.MalformedURLException;

public class FileUtils {

    public static final String[] VALID_MIME_TYPES = {
            "application/msword",
            "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
            "application/vnd.ms-powerpoint",
            "application/vnd.openxmlformats-officedocument.presentationml.presentation",
            "application/vnd.ms-excel",
            "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
            "application/pdf"
    };

    public static String getPath(Context context, Uri uri) {
        if ("content".equalsIgnoreCase(uri.getScheme())) {
            String[] projection = {"_data"};
            Cursor cursor = null;

            try {
                cursor = context.getContentResolver().query(uri, projection, null, null, null);
                int columnIndex = cursor.getColumnIndexOrThrow("_data");
                if (cursor.moveToFirst()) {
                    return cursor.getString(columnIndex);
                }
            } catch (Exception ex) {
                Crashlytics.logException(ex);
            } finally {
                cursor.close();
            }

        } else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    public static String getMimeType(Context context, Uri uri) {
        return getMimeType(uri.toString());
    }

    public static String getMimeType(String filename) {
        String extension = MimeTypeMap.getFileExtensionFromUrl(filename);
        return MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
    }

    public static boolean isSupported(String mimetype) {
        boolean supported = false;

        if (mimetype != null) {
            Log.i(FileUtils.class.getName(), mimetype);
        }


        for (String type : VALID_MIME_TYPES) {
            if (type.equals(mimetype)) {
                supported = true;
                break;
            }
        }

        return supported;
    }

    public static String getGenericType(Context context, Uri uri) {
        String mimeType = getMimeType(context, uri);

        if (mimeType.equals("application/vnd.openxmlformats-officedocument.wordprocessingml.document") ||
                mimeType.equals("application/msword")) {
            return "word";
        } else if (mimeType.equals("application/vnd.openxmlformats-officedocument.presentationml.presentation") ||
                mimeType.equals("application/vnd.ms-powerpoint")) {
            return "powerpoint";
        } else if (mimeType.equals("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet") ||
                mimeType.equals("application/vnd.ms-excel")) {
            return "excel";
        } else if (mimeType.equals("application/pdf")) {
            return "pdf";
        } else {
            return null;
        }
    }

    public static Drawable getDrawableByFileType(Context context, String type) {
        switch (type) {
            case "word":
                return ContextCompat.getDrawable(context, R.drawable.ic_file_word);
            case "powerpoint":
                return ContextCompat.getDrawable(context, R.drawable.ic_file_powerpoint);
            case "excel":
                return ContextCompat.getDrawable(context, R.drawable.ic_file_excel);
            case "pdf":
                return ContextCompat.getDrawable(context, R.drawable.ic_file_pdf);
            default:
                return ContextCompat.getDrawable(context, R.drawable.ic_file_document);
        }
    }

    public static String getExtension(File file) {
        String extension = null;
        try {
            extension = MimeTypeMap.getFileExtensionFromUrl(file.toURI().toURL().toString());
        } catch (MalformedURLException e) {
            Crashlytics.logException(e);
            e.printStackTrace();
        } finally {
            return extension;
        }
    }

    public static String removeExtension(String string) {
        if (string.contains(".")) {
            int index = string.lastIndexOf('.');
            return string.substring(0, index);
        } else {
            return string;
        }
    }

    public static String removeWhitespace(String filename) {
        return filename.replace(" ", "_");
    }

    public static void createDir(String dir) {
        File directory = new File(dir);
        if (!directory.exists()) {
            directory.mkdirs();
        }
    }

    public static String prepareFile(String filename) {
        String externalSdCardPath = Environment.getExternalStorageDirectory().getPath();
        /*List<StorageUtils.StorageInfo> storageInfoList = StorageUtils.getStorageList();

        for(StorageUtils.StorageInfo info : storageInfoList) {
            if (!info.internal && !info.readonly) {
                externalSdCardPath = info.path;
                break;
            }
        }*/
        String downloadPath = externalSdCardPath + "/DaystarMemo/Documents";
        createDir(downloadPath);
        String filepath = downloadPath + '/' + filename;
        File file = new File(filepath);
        if (file.exists()) {
            Log.i(FileUtils.class.getName(), "DELETING FOUND FILE");
            file.delete();
        }

        return downloadPath + '/' + filename;
    }
}
