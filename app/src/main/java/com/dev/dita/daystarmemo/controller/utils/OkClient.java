package com.dev.dita.daystarmemo.controller.utils;

import android.content.Context;

import com.baasbox.android.BaasBox;
import com.baasbox.android.BaasException;
import com.baasbox.android.BaasIOException;
import com.baasbox.android.net.HttpRequest;
import com.baasbox.android.net.HttpResponse;
import com.baasbox.android.net.RestClient;
import com.crashlytics.android.Crashlytics;

import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Protocol;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okhttp3.internal.Util;
import okio.BufferedSink;
import okio.Okio;
import okio.Source;


public class OkClient implements RestClient {
    private static final byte[] ZERO_BYTES = new byte[0];

    private OkHttpClient client;
    private String charset;
    private InputRequestBody.ProgressListener progressListener;

    public OkClient() {
        this(new OkHttpClient());
    }

    public OkClient(OkHttpClient client) {
        this.client = client;
    }

    @Override
    public void init(Context context, BaasBox.Config config) {
        this.charset = config.httpCharset;
        client = new OkHttpClient.Builder()
                .connectTimeout(config.httpConnectionTimeout, TimeUnit.MILLISECONDS)
                .readTimeout(config.httpSocketTimeout, TimeUnit.MILLISECONDS)
                .followSslRedirects(true)
                .build();
    }

    private RequestBody buildBody(String contentType, InputStream bodyData) {
        if (bodyData == null) {
            return RequestBody.create(MediaType.parse("application/json;charset=" + charset), "{}");
        } else {
            return new InputRequestBody(bodyData, contentType, progressListener);
        }
    }

    @Override
    public HttpResponse execute(HttpRequest request) throws BaasException {
        String contentType = request.headers.get("Content-Type");
        Request.Builder okRequestBuilder = new Request.Builder();
        boolean contentLengthSet = false;
        for (String name : request.headers.keySet()) {
            if (!contentLengthSet && "Content-Length".equals(name)) {
                contentLengthSet = true;
            }
            okRequestBuilder.addHeader(name, request.headers.get(name));
        }

        if (!contentLengthSet) {
            okRequestBuilder.addHeader("Content-Length", "0");
        }
        RequestBody requestBody;
        switch (request.method) {
            case HttpRequest.GET:
                okRequestBuilder.get();
                break;
            case HttpRequest.POST:
                if (contentType != null) {
                    if (contentType.contains("multipart/form-data")) {
                        final String id = TransferManager.transferQueue.remove();
                        progressListener = new InputRequestBody.ProgressListener(id) {
                            @Override
                            public void transferred(long num) {
                                TransferManager.manager.update(id, num);
                            }
                        };
                    }
                }
                requestBody = buildBody(contentType, request.body);
                okRequestBuilder.post(requestBody);
                break;
            case HttpRequest.PUT:
                requestBody = buildBody(contentType, request.body);
                okRequestBuilder.put(requestBody);
                break;
            case HttpRequest.DELETE:
                okRequestBuilder.delete();
                break;
            case HttpRequest.PATCH:
                requestBody = buildBody(contentType, request.body);
                okRequestBuilder.patch(requestBody);
                break;
        }

        okRequestBuilder.url(request.url);
        Request okRequest = okRequestBuilder.build();
        try {
            Response response = client.newCall(okRequest).execute();
            Protocol protocol = response.protocol();
            HttpResponse.HttpVersion version = HttpResponse.HttpVersion.get(protocol.toString());
            HttpResponse baasResponse = new HttpResponse(version, response.code(), response.message());
            OkBody body = new OkBody(response.body());
            baasResponse.setEntity(body);
            for (String name : response.headers().names()) {
                String val = response.headers().get(name);
                baasResponse.addHeader(name, val);
            }
            return baasResponse;
        } catch (IOException e) {
            Crashlytics.logException(e);
            throw new BaasIOException(e);
        }
    }

    public void setProgressListener(InputRequestBody.ProgressListener progressListener) {
        this.progressListener = progressListener;
    }

    public static class InputRequestBody extends RequestBody {
        private static final int SEGMENT_SIZE = 1024;

        private final InputStream inputStream;
        private final ProgressListener listener;
        private final String contentType;
        private Source in;

        public InputRequestBody(InputStream inputStream, String contentType, ProgressListener listener) {
            this.inputStream = inputStream;
            this.contentType = contentType;
            this.listener = listener;
            in = Okio.source(inputStream);
        }

        @Override
        public MediaType contentType() {
            return MediaType.parse(contentType);
        }

        @Override
        public void writeTo(BufferedSink sink) throws IOException {
            if (this.listener == null) {
                sink.writeAll(in);
            } else {
                try {
                    long total = 0;
                    long read;

                    while ((read = in.read(sink.buffer(), SEGMENT_SIZE)) != -1) {
                        total += read;
                        sink.flush();
                        this.listener.transferred(total);
                    }
                } finally {
                    Util.closeQuietly(in);
                }

            }
        }

        public abstract static class ProgressListener {
            String uploadId = null;

            public ProgressListener(String uploadId) {
                this.uploadId = uploadId;
            }

            public abstract void transferred(long num);
        }
    }

    private static class OkBody extends HttpResponse.Body {
        private final ResponseBody body;

        public OkBody(ResponseBody body) {
            this.body = body;
        }

        @Override
        public String contentType() {
            return body.contentType().toString();
        }

        @Override
        public long contentLength() {
            return body.contentLength();
        }

        @Override
        protected String contentString(String s) throws IOException {
            return body.string();
        }

        @Override
        public InputStream getContent() throws IOException {
            return body.byteStream();
        }

        @Override
        public void close() throws IOException {
            body.close();
        }
    }
}
