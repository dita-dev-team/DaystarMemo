package com.dev.dita.daystarmemo.services;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.gms.gcm.GcmListenerService;

/**
 * The type Gcm service. Receives gcm message
 */
public class GcmService extends GcmListenerService {
    final String TAG = getClass().getName();
    public GcmService() {
    }

    @Override
    public void onMessageReceived(String s, Bundle bundle) {
        Intent intent = new Intent(this, PushReceiverIntentService.class);
        intent.putExtras(bundle);
        startService(intent);
    }
}
