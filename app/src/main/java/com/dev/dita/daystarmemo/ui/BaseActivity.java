package com.dev.dita.daystarmemo.ui;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.NavUtils;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.baasbox.android.BaasUser;
import com.baasbox.android.json.JsonObject;
import com.crashlytics.android.Crashlytics;
import com.dev.dita.daystarmemo.PrefSettings;
import com.dev.dita.daystarmemo.R;
import com.dev.dita.daystarmemo.controller.bus.UserBus;
import com.dev.dita.daystarmemo.controller.utils.ImageUtils;
import com.dev.dita.daystarmemo.controller.utils.UIUtils;
import com.dev.dita.daystarmemo.model.baas.UserBaas;
import com.dev.dita.daystarmemo.model.database.Schedule;
import com.dev.dita.daystarmemo.services.RegistrationService;
import com.dev.dita.daystarmemo.services.ScheduleService;
import com.dev.dita.daystarmemo.ui.connections.ConnectionsActivity;
import com.dev.dita.daystarmemo.ui.documents.DocumentsActivity;
import com.dev.dita.daystarmemo.ui.main.MainActivity;
import com.dev.dita.daystarmemo.ui.memosgroups.MemoGroupActivity;
import com.dev.dita.daystarmemo.ui.profile.ProfileActivity;
import com.dev.dita.daystarmemo.ui.schedule.ScheduleActivity;
import com.dev.dita.daystarmemo.ui.settings.SettingsActivity;
import com.dev.dita.daystarmemo.ui.welcome.WelcomeActivity;

import org.greenrobot.eventbus.Subscribe;

import de.hdodenhof.circleimageview.CircleImageView;
import io.realm.Realm;

public abstract class BaseActivity extends AppCompatActivity {

    protected static final int NAVDRAWER_ITEM_HOME = 0;
    protected static final int NAVDRAWER_ITEM_MEMOS = 1;
    protected static final int NAVDRAWER_ITEM_GROUPS = 2;
    protected static final int NAVDRAWER_ITEM_CONNECTIONS = 3;
    protected static final int NAVDRAWER_ITEM_SCHEDULE = 7;
    protected static final int NAVDRAWER_ITEM_DOCUMENTS = 4;
    protected static final int NAVDRAWER_ITEM_SETTINGS = 5;
    protected static final int NAVDRAWER_ITEM_LOGOUT = 6;
    protected static final int NAVDRAWER_ITEM_INVALID = -1;
    protected static final int NAVDRAWER_ITEM_SEPARATOR = -2;
    private static final int NAVDRAWER_LAUNCH_DELAY = 250;
    private static final int MAIN_CONTENT_FADEOUT_DURATION = 150;
    private static final int MAIN_CONTENT_FADEIN_DURATION = 250;
    private final String TAG = getClass().getName();
    protected SwipeRefreshLayout swipeRefreshLayout;
    protected boolean isGuestSession = false;
    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    private Toolbar actionBarToolbar;
    // variables that control the Action Bar auto hide behavior (aka "quick recall")
    private boolean actionBarAutoHideEnabled = false;
    private int actionBarAutoHideSensitivity = 0;
    private int actionBarAutoHideMinY = 0;
    private int actionBarAutoHideSignal = 0;
    private boolean actionBarShown = true;
    // A Runnable that we should execute when the navigation drawer finishes its closing animation
    private Runnable deferredOnDrawerClosedRunnable;
    private TextView username;
    private TextView email;
    private CircleImageView imageView;
    private Handler handler;

    public static void navigateUpOrBack(Activity currentActivity,
                                        Class<? extends Activity> syntheticParentActivity) {
        // Retrieve parent activity from AndroidManifest.
        Intent intent = NavUtils.getParentActivityIntent(currentActivity);

        // Synthesize the parent activity when a natural one doesn't exist.
        if (intent == null && syntheticParentActivity != null) {
            try {
                intent = NavUtils.getParentActivityIntent(currentActivity, syntheticParentActivity);
            } catch (PackageManager.NameNotFoundException e) {
                Crashlytics.logException(e);
                e.printStackTrace();
            }
        }

        if (intent == null) {
            // No parent defined in manifest. This indicates the activity may be used by
            // in multiple flows throughout the app and doesn't have a strict parent. In
            // this case the navigation up button should act in the same manner as the
            // back button. This will result in users being forwarded back to other
            // applications if currentActivity was invoked from another application.
            currentActivity.onBackPressed();
        } else {
            if (NavUtils.shouldUpRecreateTask(currentActivity, intent)) {
                // Need to synthesize a backstack since currentActivity was probably invoked by a
                // different app. The preserves the "Up" functionality within the app according to
                // the activity hierarchy defined in AndroidManifest.xml via parentActivity
                // attributes.
                TaskStackBuilder builder = TaskStackBuilder.create(currentActivity);
                builder.addNextIntentWithParentStack(intent);
                builder.startActivities();
            } else {
                // Navigate normally to the manifest defined "Up" activity.
                NavUtils.navigateUpTo(currentActivity, intent);
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(0, 0);

        if (!PrefSettings.isLoggedIn(this)) {
            startActivity(new Intent(this, WelcomeActivity.class));
            finish();
            return;
        }

        isGuestSession = PrefSettings.isGuestSession(this);

        handler = new Handler();

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    private void trySetupSwipeRefresh() {
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        if (swipeRefreshLayout != null) {
            swipeRefreshLayout.setColorSchemeResources(R.color.baseColor1, R.color.baseColor2);
            UIUtils.setAnimation(swipeRefreshLayout, false);
        }
    }

    protected int getSelfNavDrawerItem() {
        return NAVDRAWER_ITEM_INVALID;
    }

    private void setupNavDrawer() {
        int selfItem = getSelfNavDrawerItem();

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        if (drawerLayout == null) {
            return;
        }

        navigationView = (NavigationView) findViewById(R.id.navdrawer);


        if (selfItem == NAVDRAWER_ITEM_INVALID) {
            // do not show a nav drawer
            if (navigationView != null) {
                ((ViewGroup) navigationView.getParent()).removeView(navigationView);
            }

            drawerLayout = null;
            return;
        }

        if (navigationView != null) {

            navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(MenuItem item) {
                    switch (item.getItemId()) {
                        case R.id.nav_home:
                            onNavDrawerItemClicked(NAVDRAWER_ITEM_HOME);
                            return true;
                        case R.id.nav_memo:
                            onNavDrawerItemClicked(NAVDRAWER_ITEM_MEMOS);
                            return true;
                        case R.id.nav_groups:
                            onNavDrawerItemClicked(NAVDRAWER_ITEM_GROUPS);
                            return true;
                        case R.id.nav_connections:
                            onNavDrawerItemClicked(NAVDRAWER_ITEM_CONNECTIONS);
                            return true;
                        case R.id.nav_documents:
                            onNavDrawerItemClicked(NAVDRAWER_ITEM_DOCUMENTS);
                            return true;
                        case R.id.nav_settings:
                            onNavDrawerItemClicked(NAVDRAWER_ITEM_SETTINGS);
                            return true;
                        case R.id.nav_logout:
                            onNavDrawerItemClicked(NAVDRAWER_ITEM_LOGOUT);
                            return true;
                        case R.id.nav_schedule:
                            onNavDrawerItemClicked(NAVDRAWER_ITEM_SCHEDULE);
                            return true;
                        default:
                            return false;
                    }
                }
            });

            setSelectedNavDrawerItem(getSelfNavDrawerItem());

            if (isGuestSession) {
                navigationView.getMenu().clear();
                navigationView.inflateMenu(R.menu.activity_base_guest_drawer);
            }

            View headerView = navigationView.getHeaderView(0);


            imageView = (CircleImageView) headerView.findViewById(R.id.nav_profile_image);
            username = (TextView) headerView.findViewById(R.id.nav_name);
            email = (TextView) headerView.findViewById(R.id.nav_email);

            if (isGuestSession) {
                username.setText(R.string.hello_guest);
                email.setText(R.string.welome_to_daystar_memo);
            } else {
                imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startActivity(new Intent(BaseActivity.this, ProfileActivity.class));
                        drawerLayout.closeDrawer(GravityCompat.START);
                    }
                });
            }
        }

        if (actionBarToolbar != null) {
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                    this, drawerLayout, actionBarToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
            drawerLayout.addDrawerListener(toggle);
            toggle.syncState();
        }

        drawerLayout.addDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                onNavDrawerSlide(slideOffset);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                onNavDrawerStateChanged(true, false);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                // run deferred action, if we have one
                if (deferredOnDrawerClosedRunnable != null) {
                    deferredOnDrawerClosedRunnable.run();
                    deferredOnDrawerClosedRunnable = null;
                }

                onNavDrawerStateChanged(false, false);
            }

            @Override
            public void onDrawerStateChanged(int newState) {
                onNavDrawerStateChanged(isNavDrawerOpen(), newState != DrawerLayout.STATE_IDLE);
            }
        });
    }

    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        super.setContentView(layoutResID);
        getActionBarToolbar();
    }

    protected void onNavDrawerStateChanged(boolean isOpen, boolean isAnimating) {
        if (actionBarAutoHideEnabled && isOpen) {
            autoShowOrHideActionBar(true);
        }
    }

    protected void onNavDrawerSlide(float offset) {
    }

    protected boolean isNavDrawerOpen() {
        return drawerLayout != null && drawerLayout.isDrawerOpen(GravityCompat.START);
    }

    protected void closeNavDrawer() {
        if (drawerLayout != null) {
            drawerLayout.closeDrawer(GravityCompat.START);
        }
    }

    @Override
    public void onBackPressed() {
        if (isNavDrawerOpen()) {
            closeNavDrawer();
        } else {
            super.onBackPressed();
        }
    }

    protected void setSelectedNavDrawerItem(int itemId) {
        int size = navigationView.getMenu().size();
        for (int i = 0; i < size; i++) {
            navigationView.getMenu().getItem(i).setChecked(false);
        }
        switch (itemId) {
            case NAVDRAWER_ITEM_HOME:
                navigationView.getMenu().findItem(R.id.nav_home).setChecked(true);
                break;
            case NAVDRAWER_ITEM_MEMOS:
                navigationView.getMenu().findItem(R.id.nav_memo).setChecked(true);
                break;
            case NAVDRAWER_ITEM_GROUPS:
                navigationView.getMenu().findItem(R.id.nav_groups).setChecked(true);
                break;
            case NAVDRAWER_ITEM_CONNECTIONS:
                navigationView.getMenu().findItem(R.id.nav_connections).setChecked(true);
                break;
            case NAVDRAWER_ITEM_SCHEDULE:
                navigationView.getMenu().findItem(R.id.nav_schedule).setChecked(true);
                break;
            case NAVDRAWER_ITEM_DOCUMENTS:
                navigationView.getMenu().findItem(R.id.nav_documents).setChecked(true);
                break;
            case NAVDRAWER_ITEM_SETTINGS:
                navigationView.getMenu().findItem(R.id.nav_settings).setChecked(true);
                break;
            case NAVDRAWER_ITEM_LOGOUT:
                break;
        }

    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        setupNavDrawer();

        trySetupSwipeRefresh();

        View mainContent = findViewById(R.id.main_content);
        if (mainContent != null) {
            mainContent.setAlpha(0);
            mainContent.animate().alpha(1).setDuration(MAIN_CONTENT_FADEIN_DURATION);
        } else {
            Log.i(TAG, "No view with main_content to fade in.");
        }
        initUser();
    }

    private void goToNavDrawerItem(int item) {
        Intent intent;
        switch (item) {
            case NAVDRAWER_ITEM_HOME:
                startActivity(new Intent(this, MainActivity.class));
                break;
            case NAVDRAWER_ITEM_MEMOS:
                intent = new Intent(this, MemoGroupActivity.class);
                intent.putExtra("section", MemoGroupActivity.MEMO);
                createBackStack(intent);
                break;
            case NAVDRAWER_ITEM_GROUPS:
                intent = new Intent(this, MemoGroupActivity.class);
                intent.putExtra("section", MemoGroupActivity.GROUP);
                createBackStack(intent);
                break;
            case NAVDRAWER_ITEM_CONNECTIONS:
                createBackStack(new Intent(this, ConnectionsActivity.class));
                break;
            case NAVDRAWER_ITEM_SCHEDULE:
                createBackStack(new Intent(this, ScheduleActivity.class));
                break;
            case NAVDRAWER_ITEM_DOCUMENTS:
                createBackStack(new Intent(this, DocumentsActivity.class));
                break;
            case NAVDRAWER_ITEM_SETTINGS:
                createBackStack(new Intent(this, SettingsActivity.class));
                break;
            case NAVDRAWER_ITEM_LOGOUT:
                if (isGuestSession) {
                    PrefSettings.clear(this);
                    startActivity(new Intent(this, WelcomeActivity.class));
                    finish();
                } else {
                    showLogoutDialog();
                }

                break;
        }
    }

    /**
     * Enables back navigation for activities that are launched from the NavBar.
     *
     * @param intent
     */
    private void createBackStack(Intent intent) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            TaskStackBuilder builder = TaskStackBuilder.create(this);
            builder.addNextIntentWithParentStack(intent);
            builder.startActivities();
        } else {
            startActivity(intent);
            finish();
        }
    }

    protected void onNavDrawerItemClicked(final int itemId) {
        if (itemId == getSelfNavDrawerItem()) {
            drawerLayout.closeDrawer(GravityCompat.START);
            return;
        }
        if (isSpecialItem(itemId)) {
            goToNavDrawerItem(itemId);
        } else {
            // launch the target Activity after a short delay, to allow the close animation to play
            //goToNavDrawerItem(itemId);
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    goToNavDrawerItem(itemId);
                }
            }, NAVDRAWER_LAUNCH_DELAY);
        }

        // change the active item on the list so the user can see the item changed
        //setSelectedNavDrawerItem(itemId);
        // fade out the main content
        if (itemId != NAVDRAWER_ITEM_LOGOUT) {
            View mainContent = findViewById(R.id.main_content);
            if (mainContent != null) {
                mainContent.animate().alpha(0).setDuration(MAIN_CONTENT_FADEOUT_DURATION);
            }

            drawerLayout.closeDrawer(GravityCompat.START);
        }

    }

    /**
     * Initializes the Action Bar auto-hide (aka Quick Recall) effect.
     */
    private void initActionBarAutoHide() {
        actionBarAutoHideEnabled = true;
        actionBarAutoHideMinY = getResources().getDimensionPixelSize(
                R.dimen.action_bar_auto_hide_min_y);
        actionBarAutoHideSensitivity = getResources().getDimensionPixelSize(
                R.dimen.action_bar_auto_hide_sensivity);
    }

    /**
     * Indicates that the main content has scrolled (for the purposes of showing/hiding
     * the action bar for the "action bar auto hide" effect). currentY and deltaY may be exact
     * (if the underlying view supports it) or may be approximate indications:
     * deltaY may be INT_MAX to mean "scrolled forward indeterminately" and INT_MIN to mean
     * "scrolled backward indeterminately".  currentY may be 0 to mean "somewhere close to the
     * start of the list" and INT_MAX to mean "we don't know, but not at the start of the list"
     */
    private void onMainContentScrolled(int currentY, int deltaY) {
        if (deltaY > actionBarAutoHideSensitivity) {
            deltaY = actionBarAutoHideSensitivity;
        } else if (deltaY < -actionBarAutoHideSensitivity) {
            deltaY = -actionBarAutoHideSensitivity;
        }

        if (Math.signum(deltaY) * Math.signum(actionBarAutoHideSignal) < 0) {
            // deltaY is a motion opposite to the accumulated signal, so reset signal
            actionBarAutoHideSignal = deltaY;
        } else {
            // add to accumulated signal
            actionBarAutoHideSignal += deltaY;
        }

        boolean shouldShow = currentY < actionBarAutoHideMinY ||
                (actionBarAutoHideSignal <= -actionBarAutoHideSensitivity);
        autoShowOrHideActionBar(shouldShow);
    }

    protected Toolbar getActionBarToolbar() {
        if (actionBarToolbar == null) {
            actionBarToolbar = (Toolbar) findViewById(R.id.toolbar);

            if (actionBarToolbar != null) {
                actionBarToolbar.setNavigationContentDescription(getResources().getString(R.string.navdrawer_description));
                setSupportActionBar(actionBarToolbar);
            }
        }

        return actionBarToolbar;
    }

    protected void autoShowOrHideActionBar(boolean show) {
        if (show == actionBarShown) {
            return;
        }

        actionBarShown = show;
        onActionBarAutoShowOrHide(show);
    }

    protected void enableActionBarAutoHide(final RecyclerView recyclerView) {
        initActionBarAutoHide();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            recyclerView.setOnScrollChangeListener(new View.OnScrollChangeListener() {
                @Override
                public void onScrollChange(View view, int i, int i1, int i2, int i3) {

                }
            });
        } else {
            recyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                }
            });
        }
    }

    private boolean isSpecialItem(int itemId) {
        return itemId == NAVDRAWER_ITEM_SETTINGS;
    }

    private boolean isSeparator(int itemId) {
        return itemId == NAVDRAWER_ITEM_SEPARATOR;
    }

    protected void onActionBarAutoShowOrHide(boolean shown) {

    }

    protected void initUser() {
        if (isGuestSession) {
            return;
        }

        new AsyncTask<Void, Void, Void>() {
            String name;
            String emailSrc;
            Bitmap image;
            @Override
            protected Void doInBackground(Void... params) {
                Context context = BaseActivity.this;
                // Set the default settings if none exist yet
                if (!PrefSettings.keyExists(context)) {
                    JsonObject details = BaasUser.current().getScope(BaasUser.Scope.FRIEND);
                    PrefSettings.setValue(context, "username", BaasUser.current().getName());
                    PrefSettings.setValue(context, "name", details.getString("name", getString(R.string.default_name)));
                    PrefSettings.setValue(context, "email", details.getString("email", getString(R.string.default_email)));
                    PrefSettings.setValue(context, "major", details.getString("major", getString(R.string.none)));
                    PrefSettings.setValue(context, "studentType", details.getString("studentType", getString(R.string.none)));
                    PrefSettings.setValue(context, "password", BaasUser.current().getPassword());
                    PrefSettings.setValue(context, "token", BaasUser.current().getToken());
                    byte[] imageArray = details.getBinary("image", null);
                    if (imageArray != null) {
                        PrefSettings.setValue(context, "image", ImageUtils.byteArrayToString(imageArray));
                    } else {
                        PrefSettings.setValue(context, "image", "");
                    }
                }

                name = PrefSettings.getStringValue(context, "name");
                emailSrc = PrefSettings.getStringValue(context, "email");
                String imageSrc = PrefSettings.getStringValue(context, "image");
                if (!TextUtils.isEmpty(imageSrc)) {
                    image = ImageUtils.decodeBitmapFromString(imageSrc);
                }

                // Download schedule
                Realm realm = Realm.getDefaultInstance();
                if (realm.where(Schedule.class).findAll().isEmpty()) {
                    startService(new Intent(BaseActivity.this, ScheduleService.class));
                }

                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                // Load the profile from setting
                username.setText(name);
                email.setText(emailSrc);
                if (image != null) {
                    imageView.setImageBitmap(image);
                } else {
                    imageView.setImageDrawable(ContextCompat.getDrawable(BaseActivity.this, R.drawable.default_profile));
                }
            }
        }.execute();
    }

    /**
     * On event.
     *
     * @param logoutResult the logout result
     */
    @Subscribe
    public void onEvent(UserBus.LogoutResult logoutResult) {
        UIUtils.setAnimation(swipeRefreshLayout, false);
        if (logoutResult.error) {
            Toast.makeText(this, "Unable to logout", Toast.LENGTH_SHORT).show();
        } else {
            //PrefSettings.setLoggedIn(this, false);
            cleanup();
        }
    }

    /**
     * On event.
     *
     * @param profileUpdatedEvent the profile updated event
     */
    @Subscribe
    public void onEvent(UserBus.ProfileUpdatedEvent profileUpdatedEvent) {
        initUser();
    }

    protected void showLogoutDialog() {
        new AlertDialog.Builder(this)
                .setTitle("Logging out")
                .setMessage("Are you sure?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        drawerLayout.closeDrawer(GravityCompat.START);
                        if (BaasUser.current() == null) {
                            cleanup();
                        } else {
                            if (swipeRefreshLayout != null) {
                                UIUtils.setAnimation(swipeRefreshLayout, true);
                            }
                            UserBaas.logoutUser(BaseActivity.this);
                        }


                    }
                })
                .setNegativeButton("No", null)
                .show();
    }

    private void cleanup() {
        Intent intent = new Intent(this, RegistrationService.class);
        intent.putExtra("isRegistering", false);
        startService(intent);
        PrefSettings.clear(this);
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        realm.deleteAll();
        realm.commitTransaction();
        startActivity(new Intent(this, WelcomeActivity.class));
        //deleteFile(AssetBaas.videoFileName);
        finish();
    }
}
