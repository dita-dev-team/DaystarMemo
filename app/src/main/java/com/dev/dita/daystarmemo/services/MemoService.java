package com.dev.dita.daystarmemo.services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import com.dev.dita.daystarmemo.controller.bus.MemoBus;
import com.dev.dita.daystarmemo.model.baas.MemoBaas;
import com.dev.dita.daystarmemo.model.database.Memo;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.UUID;

public class MemoService extends Service {
    private final static HashSet<String> manager = new HashSet<>();
    private final static LinkedList<Memo> queue = new LinkedList<>();
    private final HashSet<String> recipients = new HashSet<>();

    public MemoService() {
    }

    public static void sendMemo(String message, boolean isChat, String[] usernames) {
        Memo memo = new Memo();
        memo.status = Memo.PENDING;
        memo.body = message;
        memo.isFile = false;
        memo.sentByMe = true;
        memo.date = new Date();

        if (!isChat) {
            MemoBaas.sendMemo(memo, usernames);
            return;
        }

        ArrayList<Memo> memos = new ArrayList<>();

        for (String username : usernames) {
            memo.id = UUID.randomUUID().toString();
            memo.connection = username;
            memos.add(memo);
            queue.add(memo);
        }

        Memo.saveMemos(memos);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        EventBus.getDefault().register(this);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        sendMemo();
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe
    public void onEvent(MemoBus.SendMemoEvent event) {
        Log.i(getClass().getName(), "RECEIVED: " + queue.size());
        sendMemo();
    }

    @Subscribe(priority = 1)
    public void onEvent(MemoBus.SendMemoResult result) {
        if (result.error) {

        } else {
            if (manager.contains(result.memo.id)) {
                manager.remove(result.memo.id);
                recipients.remove(result.memo.connection);
                Memo.saveMemo(result.memo);
                sendMemo();
            }
        }
    }

    @Subscribe(priority = 1)
    public void onEvent(MemoBus.SendBroadcastMemoResult result) {
        if (!result.error) {
            Memo.saveMemos(result.memos);
        }
    }

    private void sendMemo() {
        ArrayList<Memo> pendingMemos = new ArrayList<>();
        while (!queue.isEmpty()) {
            Memo memo = queue.remove();
            if (recipients.contains(memo.connection)) {
                pendingMemos.add(memo);
            } else {
                recipients.add(memo.connection);
                manager.add(memo.id);
                Log.i(getClass().getName(), "SENDING TO: " + memo.connection);
                MemoBaas.sendMemo(memo, memo.connection);
            }
        }

        queue.addAll(pendingMemos);
    }
}
