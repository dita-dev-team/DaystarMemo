package com.dev.dita.daystarmemo.ui.profile;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.NavUtils;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.MultiAutoCompleteTextView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.baasbox.android.BaasUser;
import com.baasbox.android.json.JsonObject;
import com.dev.dita.daystarmemo.PrefSettings;
import com.dev.dita.daystarmemo.R;
import com.dev.dita.daystarmemo.controller.bus.UserBus;
import com.dev.dita.daystarmemo.controller.utils.ImageUtils;
import com.dev.dita.daystarmemo.controller.utils.UIUtils;
import com.dev.dita.daystarmemo.model.baas.UserBaas;
import com.dev.dita.daystarmemo.services.ScheduleService;
import com.dev.dita.daystarmemo.ui.customviews.CoordinatedCircularImageView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemSelected;
import butterknife.OnTextChanged;

/**
 * The type Profile activity.
 */
// TODO: 21/06/16 Modify optimize the activity

public class ProfileActivity extends AppCompatActivity {

    private final static String TAG = ProfileActivity.class.getName();

    private static final int IMAGE_PICK = 1;
    private static final int IMAGE_CAPTURE = 2;

    /**
     * The Swipe refresh layout.
     */
    @BindView(R.id.profile_refresh_animation)
    SwipeRefreshLayout swipeRefreshLayout;
    /**
     * The Coordinator layout.
     */
    @BindView(R.id.coordinator_layout)
    CoordinatorLayout coordinatorLayout;
    /**
     * The Profile image.
     */
    @BindView(R.id.image)
    CoordinatedCircularImageView profileImage;
    /**
     * The Bottom sheet button.
     */
    @BindView(R.id.edit_button)
    FloatingActionButton bottomSheetButton;
    /**
     * The Save button.
     */
    @BindView(R.id.save_button)
    Button saveButton;
    /**
     * The Name text view.
     */
    @BindView(R.id.name)
    TextView nameTextView;
    /**
     * The Email text view.
     */
    @BindView(R.id.email)
    TextView emailTextView;

    @BindView(R.id.major)
    TextView majorTextView;

    @BindView(R.id.student_type)
    TextView studentTypeTextView;
    /**
     * The Name edit text.
     */
    @BindView(R.id.edit_name)
    TextInputEditText nameEditText;
    /**
     * The Email edit text.
     */
    @BindView(R.id.edit_email)
    TextInputEditText emailEditText;

    @BindView(R.id.edit_major)
    MultiAutoCompleteTextView majorEditText;

    @BindView(R.id.edit_student_type)
    Spinner studentTypeSpinner;

    ArrayAdapter<CharSequence> spinnerAdapter;

    ProgressDialog progressDialog;

    private boolean noImage;
    private boolean imageChanged;
    private Bitmap newImage;
    private Uri newImageUri;
    private String newImageString;
    private byte[] newImageArray;

    /**
     * Init.
     */
    public void init() {
        noImage = true;
        imageChanged = false;
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Updating profile ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        initBottomSheet();
        loadProfile();
        majorEditText.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1));
        majorEditText.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());
        spinnerAdapter = ArrayAdapter.createFromResource(this, R.array.student_type_array, android.R.layout.simple_spinner_item);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        studentTypeSpinner.setAdapter(spinnerAdapter);
        saveButton.setEnabled(false);
        swipeRefreshLayout.setColorSchemeResources(R.color.baseColor1, R.color.baseColor2);
        UIUtils.setAnimation(swipeRefreshLayout, false);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        EventBus.getDefault().register(this);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("");
        init();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        saveButton.setEnabled(false);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case IMAGE_PICK:
                    imageFromGallery(resultCode, data);
                    break;
                case IMAGE_CAPTURE:
                    imageFromCamera(resultCode, data);
                    break;
                default:
                    break;
            }
        }
    }

    @Override
    protected void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    /**
     * Change image.
     */
    @OnClick(R.id.image)
    public void changeImage() {
        final CharSequence[] items = {
                "Take Photo",
                "Choose from Gallery",
                "Remove Photo"
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0:
                        captureImage();
                        break;
                    case 1:
                        chooseImage();
                        break;
                    case 2:
                        String image = PrefSettings.getStringValue(ProfileActivity.this, "image");
                        if (!TextUtils.isEmpty(image)) {
                            new AlertDialog.Builder(ProfileActivity.this)
                                    .setMessage("Are you sure?")
                                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            imageChanged = true;
                                            noImage = true;
                                            profileImage.setImageDrawable(ContextCompat.getDrawable(ProfileActivity.this, R.drawable.default_profile));
                                            saveButton.setEnabled(true);

                                        }
                                    })
                                    .setNegativeButton("No", null)
                                    .show();

                        }
                        break;
                }
            }
        });
        builder.show();
    }

    /**
     * Capture image.
     */
    public void captureImage() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
            String filename = "profile_image";
            ContentValues values = new ContentValues();
            values.put(MediaStore.Images.Media.TITLE, filename);
            Uri imageUri = getContentResolver().insert(
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values
            );
            intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
        }

        startActivityForResult(intent, IMAGE_CAPTURE);

    }

    /**
     * Choose image boolean.
     *
     * @return the boolean
     */
    public boolean chooseImage() {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        startActivityForResult(Intent.createChooser(intent, "Select Photo"), IMAGE_PICK);
        return true;
    }

    /**
     * Name changed.
     *
     * @param text the text
     */
    @OnTextChanged(R.id.edit_name)
    public void nameChanged(CharSequence text) {
        if (!text.equals(nameTextView.getText())) {
            saveButton.setEnabled(true);
        }
    }

    /**
     * Email changed.
     *
     * @param text the text
     */
    @OnTextChanged(R.id.edit_email)
    public void emailChanged(CharSequence text) {
        if (!text.equals(emailTextView.getText())) {
            saveButton.setEnabled(true);
        }
    }

    @OnTextChanged(R.id.edit_major)
    public void majorChanged(CharSequence text) {
        if (!text.equals(majorTextView.getText())) {
            saveButton.setEnabled(true);
        }
    }

    @OnItemSelected(R.id.edit_student_type)
    public void studentTypeChanged() {
        String type = (String) studentTypeSpinner.getSelectedItem();
        if (!type.equals(studentTypeTextView.getText())) {
            saveButton.setEnabled(true);
        }
    }

    /**
     * Change password.
     */
    @OnClick(R.id.change_password_button)
    public void changePassword() {
        LinearLayout layout = new LinearLayout(this);
        layout.setOrientation(LinearLayout.VERTICAL);
        layout.setPadding(16, 16, 16, 16);
        final EditText oldEditText = new EditText(this);
        oldEditText.setHint("Old Password");
        oldEditText.setTextSize(16);
        oldEditText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        final EditText newEditText = new EditText(this);
        newEditText.setHint("New Password");
        newEditText.setTextSize(16);
        newEditText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        layout.addView(oldEditText);
        layout.addView(newEditText);
        new AlertDialog.Builder(this)
                .setTitle("Change password")
                .setView(layout)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String oldPassword = oldEditText.getText().toString();
                        String newPassword = newEditText.getText().toString();
                        if (!oldPassword.equals(BaasUser.current().getPassword())) {
                            Toast.makeText(getApplicationContext(), "Old password doesn't match", Toast.LENGTH_SHORT).show();
                            changePassword();
                        } else {
                            UserBaas.changePassword(newPassword);
                        }
                    }
                })
                .setNegativeButton("Cancel", null)
                .show();

    }

    /**
     * Image from camera.
     *
     * @param resultCode the result code
     * @param data       the data
     */
    public void imageFromCamera(int resultCode, Intent data) {
        imageChanged = true;
        noImage = false;
        profileImage.setImageBitmap((Bitmap) data.getExtras().get("data"));
        profileImage.buildDrawingCache();
        newImage = profileImage.getDrawingCache().copy(Bitmap.Config.RGB_565, false);
        profileImage.destroyDrawingCache();
        convertImageToBytes();
    }

    /**
     * Image from gallery.
     *
     * @param resultCode the result code
     * @param data       the data
     */
    public void imageFromGallery(int resultCode, Intent data) {
        Uri selectedImage = data.getData();
        String filePath = getPathFromUri(selectedImage);
        imageChanged = true;
        noImage = false;
        profileImage.setImageBitmap(BitmapFactory.decodeFile(filePath));
        profileImage.buildDrawingCache();
        newImage = profileImage.getDrawingCache().copy(Bitmap.Config.RGB_565, false);
        profileImage.destroyDrawingCache();
        convertImageToBytes();
    }


    /**
     * Init bottom sheet.
     */
    public void initBottomSheet() {
        View bottom = coordinatorLayout.findViewById(R.id.bottom_sheet);
        final BottomSheetBehavior behavior = BottomSheetBehavior.from(bottom);
        behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {

            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
            }
        });

        bottomSheetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int state = behavior.getState();
                if (state == BottomSheetBehavior.STATE_COLLAPSED || state == BottomSheetBehavior.STATE_DRAGGING) {
                    behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                } else if (state == BottomSheetBehavior.STATE_EXPANDED || state == BottomSheetBehavior.STATE_HIDDEN) {
                    UIUtils.hideKeyboard(ProfileActivity.this);
                    behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            }
            }
        });
    }

    /**
     * Load profile.
     */
    public void loadProfile() {
        // load profile from settings
        new AsyncTask<Void, Void, Void>() {
            String name;
            String email;
            String major;
            String studentType;
            Bitmap image;

            @Override
            protected Void doInBackground(Void... voids) {
                Context context = ProfileActivity.this;
                name = PrefSettings.getStringValue(context, "name");
                email = PrefSettings.getStringValue(context, "email");
                major = PrefSettings.getStringValue(context, "major");
                studentType = PrefSettings.getStringValue(context, "studentType");
                Log.i(TAG, studentType);
                String imageSrc = PrefSettings.getStringValue(context, "image");
                if (!TextUtils.isEmpty(imageSrc)) {
                    image = ImageUtils.decodeBitmapFromString(imageSrc);
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                nameTextView.setText(name);
                emailTextView.setText(email);
                majorTextView.setText(major);
                studentTypeTextView.setText(studentType);
                nameEditText.setText(name);
                emailEditText.setText(email);
                majorEditText.setText(major);
                int position = spinnerAdapter.getPosition(studentType);
                studentTypeSpinner.setSelection(position);
                saveButton.setEnabled(false);
                if (image != null) {
                    profileImage.setImageBitmap(image);
                }
            }
        }.execute();

    }

    /**
     * Save profile.
     */
    @OnClick(R.id.save_button)
    public void saveProfile() {
        progressDialog.show();
        String name = nameEditText.getText().toString().trim();
        String email = emailEditText.getText().toString().trim();
        String major = majorEditText.getText().toString().trim().replace(',', ' ');
        String studentType = (String) studentTypeSpinner.getSelectedItem();
        JsonObject details = new JsonObject();
        details.put("name", TextUtils.isEmpty(name) ? getString(R.string.default_name) : name);
        details.put("email", TextUtils.isEmpty(email) ? getString(R.string.default_email) : email);
        details.put("major", TextUtils.isEmpty(major) || major.equalsIgnoreCase("none") ? getString(R.string.none) : major.toUpperCase());
        details.put("studentType", studentType);

        if (imageChanged) {
            if (noImage) {
                details.putNull("image");
            } else {
                details.put("image", newImageArray);
            }
        } else {
            if (TextUtils.isEmpty(PrefSettings.getStringValue(this, "image"))) {
                details.putNull("image");
            } else {
                details.put("image", BaasUser.current().getScope(BaasUser.Scope.FRIEND).getBinary("image"));
            }
        }
        UserBaas.updateUserProfile(details);
    }

    /**
     * On event.
     *
     * @param remoteResult the remote result
     */
    @Subscribe
    public void onEvent(UserBus.ProfileUpdatedRemoteResult remoteResult) {
        progressDialog.dismiss();
        if (remoteResult.error) {
            Toast.makeText(this, "Profile update failed", Toast.LENGTH_LONG).show();
        } else {
            // Fetch schedule data
            Intent intent = new Intent(ProfileActivity.this, ScheduleService.class);
            intent.putExtra("update", true);
            startService(intent);
            // Save profile to settings
            String name = nameEditText.getText().toString().trim();
            String email = emailEditText.getText().toString().trim();
            String major = majorEditText.getText().toString().trim().replace(',', ' ');
            String studentType = (String) studentTypeSpinner.getSelectedItem();
            nameTextView.setText(TextUtils.isEmpty(name) ? getString(R.string.default_name) : name);
            emailTextView.setText(TextUtils.isEmpty(email) ? getString(R.string.default_email) : email);
            majorTextView.setText(TextUtils.isEmpty(major) || major.equalsIgnoreCase("none") ? getString(R.string.none) : major.toUpperCase());
            studentTypeTextView.setText(studentType);
            if (imageChanged) {
                String image = "";
                if (!noImage) {
                    image = ImageUtils.decodeBitmapToString(newImage);
                    PrefSettings.setValue(this, "image", image);
                }
            }
            PrefSettings.setValue(this, "name", TextUtils.isEmpty(name) ? getString(R.string.default_name) : name);
            PrefSettings.setValue(this, "email", TextUtils.isEmpty(email) ? getString(R.string.default_email) : email);
            PrefSettings.setValue(this, "major", TextUtils.isEmpty(major) || major.equalsIgnoreCase("none") ? getString(R.string.none) : major.toUpperCase());
            PrefSettings.setValue(this, "studentType", TextUtils.isEmpty(studentType) ? getString(R.string.none) : studentType);
            EventBus.getDefault().post(new UserBus.ProfileUpdatedEvent());
            saveButton.setEnabled(false);
            imageChanged = false;
            newImage = null;
            Toast.makeText(this, "Profile updated", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * On event.
     *
     * @param changeResult the change result
     */
    @Subscribe
    public void onEvent(UserBus.PasswordChangeResult changeResult) {
        if (changeResult.error) {
            Toast.makeText(this, "Unable to change password", Toast.LENGTH_LONG).show();
        } else {
            PrefSettings.setValue(this, "password", BaasUser.current().getPassword());
            // login the user after password is changed to refresh token
            UserBaas.loginUser(this, PrefSettings.getStringValue(this, "username"), PrefSettings.getStringValue(this, "password"));
            Toast.makeText(this, "Password changed successfully", Toast.LENGTH_LONG).show();
        }
    }

    private void convertImageToBytes() {
        new AsyncTask<Bitmap, Void, byte[]>() {
            @Override
            protected byte[] doInBackground(Bitmap... params) {
                Bitmap bitmap = params[0];
                return ImageUtils.decodeBitmapToByteArray(bitmap);
            }

            @Override
            protected void onPostExecute(byte[] array) {
                newImageArray = array;
                saveButton.setEnabled(true);
            }
        }.execute(newImage);
    }

    private String getPathFromUri(Uri uri) {
        String[] filePathColumn = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(uri, filePathColumn, null, null, null);
        cursor.moveToFirst();
        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
        String filePath = cursor.getString(columnIndex);
        cursor.close();
        return filePath;
    }
}
