package com.dev.dita.daystarmemo.services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import com.baasbox.android.BaasUser;
import com.dev.dita.daystarmemo.controller.bus.UserBus;
import com.dev.dita.daystarmemo.model.baas.UserBaas;
import com.dev.dita.daystarmemo.model.database.Connection;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

public class ConnectionService extends Service {

    public ConnectionService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
        EventBus.getDefault().register(this);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i(getClass().getName(), "STARTING SERVICE");
        UserBaas.getFollowing();
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe
    public void onEvent(UserBus.GetFollowingResult result) {
        if (!result.error) {
            //Connection.removeUsers();
            for (BaasUser user : result.users) {
                Connection.addUserWithoutCallback(user);
            }
            stopSelf();
        }
    }
}
