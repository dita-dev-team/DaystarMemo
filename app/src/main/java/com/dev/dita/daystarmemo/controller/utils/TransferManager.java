package com.dev.dita.daystarmemo.controller.utils;


import com.dev.dita.daystarmemo.controller.bus.MemoBus;

import org.greenrobot.eventbus.EventBus;

import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.ConcurrentHashMap;

public class TransferManager {
    public final static CustomHashMap manager = new CustomHashMap();
    public final static Queue<String> transferQueue = new LinkedList<>();


    public static class CustomHashMap extends ConcurrentHashMap<String, MemoBus.TransferProgress> {

        public void update(String key, long value) {
            if (containsKey(key)) {
                MemoBus.TransferProgress progress = get(key);
                progress.current = value;
                progress.progress = (int) ((value / (float) progress.total) * 100);

                //Log.i(getClass().getName(), "PROGRESS: "+progress.progress);

                progress.isDone = progress.current == progress.total;

                EventBus.getDefault().post(progress);
            }

        }

    }
}
