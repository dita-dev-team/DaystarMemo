package com.dev.dita.daystarmemo.controller.utils;


import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.util.Base64;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * The type Image utils.
 */
public class ImageUtils {

    /**
     * Decode bitmap from string bitmap.
     *
     * @param src the src
     * @return the bitmap
     */
    public static Bitmap decodeBitmapFromString(String src) {
        Bitmap bitmap;
        byte[] imageAsBytes = Base64.decode(src.getBytes(), Base64.DEFAULT);
        bitmap = BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length);
        return bitmap;
    }

    /**
     * Decode bitmap to string string.
     *
     * @param bitmap the bitmap
     * @return the string
     */
    public static String decodeBitmapToString(Bitmap bitmap) {
        String imageString;
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 90, stream);
        byte[] image = stream.toByteArray();
        imageString = Base64.encodeToString(image, 0);
        return imageString;
    }

    public static byte[] decodeBitmapToByteArray(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 90, stream);
        byte[] image = stream.toByteArray();
        return image;
    }

    public static Bitmap decodeBitmapFromByteArray(byte[] bytes) {
        Bitmap bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
        return bitmap;
    }

    public static String byteArrayToString(byte[] bytes) {
        return Base64.encodeToString(bytes, 0);
    }

    public static Bitmap getScaledBitmap(String picturePath, int height, int width) {
        BitmapFactory.Options sizeOptions = new BitmapFactory.Options();
        sizeOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(picturePath, sizeOptions);

        int inSampleSize = calculateInSampleSize(sizeOptions, height, width);
        sizeOptions.inJustDecodeBounds = false;
        sizeOptions.inSampleSize = inSampleSize;

        return BitmapFactory.decodeFile(picturePath, sizeOptions);
    }

    public static Bitmap getScaledBitmap(Resources resource, int resId, int height, int width) {
        BitmapFactory.Options sizeOptions = new BitmapFactory.Options();
        sizeOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(resource, resId, sizeOptions);

        int inSampleSize = calculateInSampleSize(sizeOptions, height, width);
        sizeOptions.inJustDecodeBounds = false;
        sizeOptions.inSampleSize = inSampleSize;

        return BitmapFactory.decodeResource(resource, resId, sizeOptions);
    }

    private static int calculateInSampleSize(BitmapFactory.Options options, int reqHeight, int reqWidth) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        // Calculate ratios of height and width to requested height and width
        if (height > reqHeight || width > reqWidth) {
            //final int heightRatio = Math.round((float) height / (float) reqHeight);
            //final int widthRatio = Math.round((float) width / (float) reqWidth);
            final int heightRatio = height / 2;
            final int widthRatio = width / 2;


            // Choose the smallest ratio as inSampleSize value, this will guarantee
            // a final image with both dimensions larger than or equal to the
            // requested height and width
            //inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
            while ((heightRatio / inSampleSize) >= reqHeight && (widthRatio / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static File createImageFile(final Context context) throws IOException {
        String timestamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timestamp + "_";
        File storageDir = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(imageFileName, ".jpg", storageDir);
        return image;
    }
}
