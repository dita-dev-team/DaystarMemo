package com.dev.dita.daystarmemo.services;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import com.dev.dita.daystarmemo.R;
import com.dev.dita.daystarmemo.controller.bus.MemoBus;
import com.dev.dita.daystarmemo.controller.utils.TransferManager;
import com.dev.dita.daystarmemo.model.baas.MemoBaas;
import com.dev.dita.daystarmemo.model.database.Memo;
import com.dev.dita.daystarmemo.model.database.Transfer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.UUID;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;

public class TransfersService extends Service {
    public final static int UPLOAD = 0;
    public final static int DOWNLOAD = 1;
    private final static HashMap<String, Transfer> transfers = new HashMap<>();
    private final static LinkedList<Transfer> transfersQueue = new LinkedList<>();
    public static boolean isRunning;
    private Realm realm;

    public TransfersService() {
    }

    public static void addUpload(File file, boolean start, String... usernames) {
        Transfer transfer = new Transfer();
        transfer.id = UUID.randomUUID().toString();
        transfer.filepath = file.getPath();
        transfer.current = 0;
        transfer.total = file.length();
        transfer.type = Transfer.UPLOAD;
        StringBuilder builder = new StringBuilder();
        for (String username : usernames) {
            builder.append(username);
            builder.append(',');
        }
        transfer.recipients = builder.toString();
        transfer.date = new Date();

        if (start) {
            transfer.status = Transfer.WAITING;
            Log.i(TransfersService.class.getName(), "ADDED TO WAITLIST");
        } else {
            transfer.status = Transfer.PENDING;
        }
        persistTransfer(transfer);
    }

    private static void persistTransfer(final Transfer transfer) {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.copyToRealm(transfer);
            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                EventBus.getDefault().post(new MemoBus.StartTransferService());
            }
        });
    }

    @Override
    public void onCreate() {
        super.onCreate();
        EventBus.getDefault().register(this);
        isRunning = true;
    }

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

/*    @Subscribe
    public void onEvent(final MemoBus.UploadProgress progress) {
        if (transfers.containsKey(progress.id)) {
            realm = Realm.getDefaultInstance();
            realm.beginTransaction();
            // update transfer progress
            Transfer transfer = realm.where(Transfer.class).equalTo("id", progress.id).findFirst();
            transfer.current = progress.current;
            transfer.progress = progress.progress;
            transfer.status = Transfer.IN_PROGRESS;

            if (progress.isDone) {
                transfer.status = Transfer.DONE_PENDING;
            }
            realm.commitTransaction();

        }
    }*/

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        startUpload();
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        isRunning = false;
        EventBus.getDefault().unregister(this);
    }

    @Subscribe
    public void onEvent(MemoBus.SendFileResult result) {
        if (transfers.containsKey(result.transferId)) {
            EventBus.getDefault().cancelEventDelivery(result);
            realm = Realm.getDefaultInstance();
            // update transfer status
            Transfer transfer = realm.where(Transfer.class).equalTo("id", result.transferId).findFirst();
            realm.beginTransaction();
            transfer.status = result.error ? Transfer.FAILED : Transfer.DONE;
            realm.commitTransaction();

            if (!result.error) {
                // save transfer as memo
                result.memo.id = UUID.randomUUID().toString();
                result.memo.filepath = transfer.filepath;
                result.memo.date = new Date();
                Memo.saveMemo(result.memo);
            }

            // remove transfer from managers
            TransferManager.manager.remove(result.transferId);
            transfers.remove(result.transferId);

            MediaPlayer mediaPlayer = MediaPlayer.create(this, R.raw.outgoing_memo_chat);
            mediaPlayer.start();
            Toast.makeText(this, "Document sent successfully", Toast.LENGTH_SHORT).show();

            startUpload();
        }
    }

    private void startUpload() {
        if (transfers.isEmpty()) {
            realm = Realm.getDefaultInstance();
            RealmResults<Transfer> transferList = realm.where(Transfer.class).equalTo("status", Transfer.WAITING).findAllSorted("date", Sort.DESCENDING);
            if (!transferList.isEmpty()) {
                Log.i(getClass().getName(), "STARTING UPLOAD");
                Transfer transfer = transferList.first();
                File file = new File(transfer.filepath);
                MemoBus.TransferProgress transferProgress = new MemoBus.TransferProgress();
                transferProgress.id = transfer.id;
                transferProgress.current = 0;
                transferProgress.total = file.length();
                TransferManager.manager.put(transfer.id, transferProgress);
                TransferManager.transferQueue.add(transfer.id);
                String[] username = transfer.recipients.split(",");
                transfers.put(transfer.id, transfer);
                MemoBaas.sendFile(file, transfer.id, username);
            } else {
                stopSelf();
            }
        }
    }
}
