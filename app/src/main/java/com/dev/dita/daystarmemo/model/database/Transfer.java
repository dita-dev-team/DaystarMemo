package com.dev.dita.daystarmemo.model.database;

import com.dev.dita.daystarmemo.model.objects.Notification;

import java.util.Date;
import java.util.UUID;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmResults;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;

public class Transfer extends RealmObject {
    @Ignore
    public final static int DONE = 0;
    @Ignore
    public final static int PENDING = 1;
    @Ignore
    public final static int DONE_PENDING = 2;
    @Ignore
    public final static int WAITING = 3;
    @Ignore
    public final static int IN_PROGRESS = 4;
    @Ignore
    public final static int FAILED = 5;
    @Ignore
    public final static int UPLOAD = 6;
    @Ignore
    public final static int DOWNLOAD = 7;
    @PrimaryKey
    public String id = "";
    public String filepath = "";
    public long current = 0;
    public int progress = 0;
    public long total = 0;
    public int status = DONE;
    public int type = UPLOAD;
    public String recipients = "";
    public Date date = null;
    public String remoteId;
    public String name = "";


    public static void removeTransfer(final String id) {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                Transfer transfer = realm.where(Transfer.class).equalTo("id", id).findFirst();
                transfer.deleteFromRealm();
            }
        });
    }

    public static void removeCompletedTransfers() {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                RealmResults<Transfer> results = realm.where(Transfer.class).equalTo("status", Transfer.DONE).findAll();
                results.deleteAllFromRealm();
            }
        });
    }

    public static void setCompletedTransfer(final String id, final String filepath, final int status) {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                Transfer transfer = realm.where(Transfer.class).equalTo("id", id).findFirst();
                transfer.filepath = filepath;
                transfer.status = status;
            }
        });
    }

    public static void addPendingDownload(final Notification notification) {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                Transfer transfer = new Transfer();
                transfer.id = UUID.randomUUID().toString();
                transfer.date = new Date();
                transfer.remoteId = notification.id;
                transfer.type = DOWNLOAD;
                transfer.status = PENDING;
                transfer.total = notification.size;
                transfer.name = notification.extraMessage;
                realm.copyToRealm(transfer);
            }
        });
    }

    public static void changeStatus(final String id, final int status) {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                Transfer transfer = realm.where(Transfer.class).equalTo("id", id).findFirst();
                transfer.status = status;
            }
        });
    }

    public Transfer copyObject() {
        Transfer transfer = new Transfer();
        transfer.id = id;
        transfer.filepath = filepath;
        transfer.current = current;
        transfer.progress = progress;
        transfer.total = total;
        transfer.status = status;
        transfer.type = type;
        transfer.recipients = recipients;
        transfer.date = date;
        transfer.name = name;
        transfer.remoteId = remoteId;
        return transfer;
    }
}
