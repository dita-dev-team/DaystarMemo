package com.dev.dita.daystarmemo.ui.welcome;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.dev.dita.daystarmemo.PrefSettings;
import com.dev.dita.daystarmemo.R;
import com.dev.dita.daystarmemo.controller.bus.UserBus;
import com.dev.dita.daystarmemo.controller.utils.ImageUtils;
import com.dev.dita.daystarmemo.controller.utils.UIUtils;
import com.dev.dita.daystarmemo.model.baas.UserBaas;
import com.dev.dita.daystarmemo.ui.main.MainActivity;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * The type Welcome activity.
 */
public class WelcomeActivity extends AppCompatActivity {

    /**
     * The Swipe refresh layout.
     */
    @BindView(R.id.welcome_refresh_animation)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.welcome_background)
    ImageView backgroundImage;

    /**
     * Init.
     */
    public void init() {
        swipeRefreshLayout.setColorSchemeResources(R.color.baseColor1, R.color.baseColor2);
        UIUtils.setAnimation(swipeRefreshLayout, false);
        //Animation fadeInAnimation = AnimationUtils.loadAnimation(WelcomeActivity.this, R.anim.welcome_background_animation);
        setBackgroundImage();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        ButterKnife.bind(this);
        EventBus.getDefault().register(this);
        init();
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment);

        if (fragment == null) {
            // Set WelcomeFragment as the active fragment
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.setCustomAnimations(R.anim.welcome_enter, R.anim.welcome_exit, R.anim.welcome_pop_enter, R.anim.welcome_pop_exit);
            transaction.replace(R.id.fragment, new WelcomeFragment()).commit();
        }
    }

    /**
     * Show register view.
     *
     * @param view the view
     */
// Sets RegisterFragment as the active fragment
    public void showRegisterView(View view) {
        Fragment register = new RegisterFragment();
        String name = register.getClass().getName();
        if (!getSupportFragmentManager().popBackStackImmediate(name, 0)) {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.setCustomAnimations(R.anim.welcome_enter, R.anim.welcome_exit, R.anim.welcome_pop_enter, R.anim.welcome_pop_exit);
            transaction.replace(R.id.fragment, register).addToBackStack(name).commit();
        }
    }

    /**
     * Show login view.
     *
     * @param view the view
     */
// Sets LoginFragment as the active fragment
    public void showLoginView(View view) {
        Fragment login = new LoginFragment();
        String name = login.getClass().getName();
        if (!getSupportFragmentManager().popBackStackImmediate(name, 0)) {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.setCustomAnimations(R.anim.welcome_enter, R.anim.welcome_exit, R.anim.welcome_pop_enter, R.anim.welcome_pop_exit);
            transaction.replace(R.id.fragment, login).addToBackStack(name).commit();
        }
    }

    public void launchGuestSession(View view) {
        PrefSettings.setGuestSession(this);
        PrefSettings.setLoggedIn(this);
        startActivity(new Intent(WelcomeActivity.this, MainActivity.class));
        finish();
    }

    /**
     * On event.
     *
     * @param loginEvent the login event
     */
    @Subscribe
    public void onEvent(UserBus.LoginEvent loginEvent) {
        UIUtils.setAnimation(swipeRefreshLayout, true);
        UserBaas.loginUser(this, loginEvent.username, loginEvent.password);
    }

    /**
     * On event.
     *
     * @param registerEvent the register event
     */
    @Subscribe
    public void onEvent(UserBus.RegisterEvent registerEvent) {
        UIUtils.setAnimation(swipeRefreshLayout, true);
        UserBaas.createUser(this, registerEvent.username, registerEvent.email, registerEvent.password);
    }

    /**
     * On event.
     *
     * @param notify the notify
     */
    @Subscribe
    public void onEvent(UserBus.Notify notify) {
        UIUtils.setAnimation(swipeRefreshLayout, false);
    }


    @Override
    protected void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    private void setBackgroundImage() {
        new AsyncTask<Integer, Void, Bitmap>() {

            @Override
            protected Bitmap doInBackground(Integer... params) {
                Integer id = params[0];
                DisplayMetrics displayMetrics = UIUtils.getDimensions(WelcomeActivity.this);
                return ImageUtils.getScaledBitmap(getResources(), id, displayMetrics.heightPixels / 2, displayMetrics.widthPixels / 2);
            }

            @Override
            protected void onPostExecute(Bitmap bitmap) {
                Log.i(getClass().getName(), "SETTING BACKGROUND");
                backgroundImage.setImageBitmap(bitmap);
                backgroundImage.setAlpha(0.0f);
                backgroundImage.animate()
                        .setDuration(3000)
                        .alpha(1.0f)
                        .setListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                backgroundImage.setVisibility(View.VISIBLE);
                            }
                        })
                        .start();


            }
        }.execute(R.drawable.welcome);
    }
}
