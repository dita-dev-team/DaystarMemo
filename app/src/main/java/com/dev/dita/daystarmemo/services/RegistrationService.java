package com.dev.dita.daystarmemo.services;


import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import com.baasbox.android.BaasBox;
import com.baasbox.android.BaasResult;
import com.crashlytics.android.Crashlytics;
import com.dev.dita.daystarmemo.R;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

import java.io.IOException;

public class RegistrationService extends IntentService {

    private static final String TAG = RegistrationService.class.getName();
    private static String token;

    public RegistrationService() {
        super("RegistrationService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Log.i(TAG, "REGISTERATION SERVICE STARTED");
        try {
            if (intent.getBooleanExtra("isRegistering", true)) {
                register();
            } else {
                unregister();
            }
        } catch (IOException e) {
            Crashlytics.logException(e);
            e.printStackTrace();
        }
    }

    private void register() throws IOException {
        InstanceID myID = InstanceID.getInstance(this);

        String registrationToken = myID.getToken(
                getString(R.string.gcm_defaultSenderId),
                GoogleCloudMessaging.INSTANCE_ID_SCOPE,
                null
        );
        token = registrationToken;
        BaasResult<Void> result = BaasBox.messagingService().enableSync(registrationToken);
        if (result.isSuccess()) {
            Log.i(TAG, "REGISTERED NOTIFICATIONS SUCCESSFULLY");
        } else {
            Log.i(TAG, "FAILED TO REGISTER NOTIFICATIONS");
        }

    }

    private void unregister() throws IOException {
        InstanceID myID = InstanceID.getInstance(this);

        myID.deleteToken(getString(R.string.gcm_defaultSenderId), GoogleCloudMessaging.INSTANCE_ID_SCOPE);
       /* BaasResult<Void> result = BaasBox.messagingService().disableSync(registrationToken);
        if (result.isSuccess()) {
            Log.i(TAG, "UNREGISTERED NOTIFICATIONS SUCCESSFULLY");
            InstanceID.getInstance(this).deleteInstanceID();
        } else {
            Log.i(TAG, "FAILED TO UNREGISTER NOTIFICATIONS");
        }*/
    }
}
