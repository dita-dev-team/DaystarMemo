package com.dev.dita.daystarmemo.ui.customviews;


import android.support.v4.view.MotionEventCompat;
import android.view.MotionEvent;
import android.view.View;

public class DraggableTouchListener implements View.OnTouchListener {
    float x;
    float y;
    private View mainView;
    private float dX;
    private float dY;

    public DraggableTouchListener(View view) {
        this.mainView = view;
        x = view.getX();
        y = view.getY();
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        final int action = MotionEventCompat.getActionMasked(motionEvent);

        switch (action) {
            case MotionEvent.ACTION_DOWN: {
                // calculate distance to move
                dX = mainView.getX() - motionEvent.getRawX();
                dY = mainView.getY() - motionEvent.getRawY();

                break;
            }
            case MotionEvent.ACTION_MOVE: {
                mainView.animate()
                        .x(motionEvent.getRawX() + dX)
                        .y(motionEvent.getRawY() + dY)
                        .setDuration(0)
                        .start();

                break;

            }
            case MotionEvent.ACTION_UP: {
                // move back to original position
               /* mainView.animate()
                        .x(x)
                        .y(y)
                        .setDuration(200)
                        .setListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                mainView.setLayoutParams(mainView.getLayoutParams());
                            }
                        })
                        .start();*/
                break;
            }
            default:
                return false;
        }
        return true;
    }
}
