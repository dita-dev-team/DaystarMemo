package com.dev.dita.daystarmemo.ui.memosgroups.groups;


import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.baasbox.android.BaasDocument;
import com.dev.dita.daystarmemo.R;

import java.util.HashSet;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class NewGroupsListAdapter extends RecyclerView.Adapter<NewGroupsListAdapter.ViewHolder> {
    private final Context context;
    private final LayoutInflater inflater;
    private OnItemClickListener itemClickListener;
    private HashSet<String> groups;
    private List<BaasDocument> data;
    private int addedColor;

    public NewGroupsListAdapter(Context context, List<BaasDocument> objects) {
        this.context = context;
        inflater = LayoutInflater.from(this.context);
        data = objects;
        addedColor = Color.parseColor("#c4c4c4");
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.new_group_list_item, parent, false);
        final ViewHolder viewHolder = new ViewHolder(view);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemClickListener.onItemClick(view, viewHolder.getLayoutPosition());
            }
        });
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        BaasDocument document = data.get(position);
        String name = document.getString("name", "");
        String type = document.getString("type", "");

        holder.name.setText(name);
        holder.type.setText(type);

        holder.name.setTextColor(Color.BLACK);

        if (groups.contains(name)) {
            holder.name.setText(name + " (member)");
            holder.name.setText(addedColor);
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public List<BaasDocument> getData() {
        return data;
    }

    public BaasDocument getItem(int position) {
        return data.get(position);
    }

    public void setOnItemClickListener(OnItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    public void setGroups(HashSet<String> groups) {
        this.groups = groups;
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.type)
        TextView type;
        @BindView(R.id.thumbnail)
        CircleImageView image;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
