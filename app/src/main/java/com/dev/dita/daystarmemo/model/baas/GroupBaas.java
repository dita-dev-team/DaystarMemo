package com.dev.dita.daystarmemo.model.baas;

import android.content.Context;
import android.util.Log;

import com.baasbox.android.BaasBox;
import com.baasbox.android.BaasDocument;
import com.baasbox.android.BaasHandler;
import com.baasbox.android.BaasResult;
import com.baasbox.android.BaasUser;
import com.baasbox.android.Rest;
import com.baasbox.android.json.JsonObject;
import com.dev.dita.daystarmemo.PrefSettings;
import com.dev.dita.daystarmemo.controller.ErrorHandler;
import com.dev.dita.daystarmemo.controller.bus.GroupBus;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

public class GroupBaas {
    private static final String TAG = GroupBaas.class.getName();

    public static void createGroup(Context context, final JsonObject object) {
        String collection = "group-" + PrefSettings.getStringValue(context, "studentType");
        JsonObject json = new JsonObject();
        json.put("collection", collection.toLowerCase());
        json.put("doc", object);

        BaasBox.rest().async(Rest.Method.POST,
                "plugin/groups.script", json, new BaasHandler<JsonObject>() {
                    @Override
                    public void handle(BaasResult<JsonObject> baasResult) {
                        if (baasResult.isSuccess()) {
                            UserBaas.refreshProfile();
                            EventBus.getDefault().post(new GroupBus.CreateGroupResult(object, false));
                            Log.i(TAG, "SUCCESS!!!");
                        } else {
                            Log.e(TAG, baasResult.error().toString());
                            EventBus.getDefault().post(new GroupBus.CreateGroupResult(true));
                        }
                    }
                });
    }

    public static void joinGroup(Context context, final BaasDocument document) {
        String collectionName = PrefSettings.getStringValue(context, "studentType").toLowerCase();
        JsonObject object = new JsonObject();
        object.put("collection", collectionName);
        object.put("group", document.getString("name"));
        object.put("username", BaasUser.current().getName());
        object.put("operation", "membership");
        object.put("addMember", true);
        BaasBox.rest().async(Rest.Method.PUT, "plugin/groups.script", object, new BaasHandler<JsonObject>() {
            @Override
            public void handle(BaasResult<JsonObject> baasResult) {
                if (baasResult.isSuccess()) {
                    UserBaas.refreshProfile();
                    EventBus.getDefault().post(new GroupBus.JoinGroupResult(false, document));
                } else {
                    Log.e(TAG, baasResult.error().toString());
                    EventBus.getDefault().post(new GroupBus.JoinGroupResult(true, null));
                }
            }
        });
        /*JsonArray members = document.getArray("members", new JsonArray());
        members.add(BaasUser.current().getName());
        document.remove("members");
        document.put("members", members);
        document.save(new BaasHandler<BaasDocument>() {
            @Override
            public void handle(BaasResult<BaasDocument> baasResult) {
                if (baasResult.isSuccess()) {
                    UserBaas.addGroupToProfile(baasResult.value());
                } else {
                    Log.e(TAG, baasResult.error().toString());
                    EventBus.getDefault().post(new GroupBus.JoinGroupResult(true, null));
                }
            }
        });*/
    }

    public static void leaveGroup(Context context, final BaasDocument document) {
        String collectionName = PrefSettings.getStringValue(context, "studentType").toLowerCase();
        JsonObject object = new JsonObject();
        object.put("collection", collectionName);
        object.put("group", document.getString("name"));
        object.put("username", BaasUser.current().getName());
        object.put("operation", "membership");
        object.put("addMember", false);
        BaasBox.rest().async(Rest.Method.PUT, "plugin/groups.script", object, new BaasHandler<JsonObject>() {
            @Override
            public void handle(BaasResult<JsonObject> baasResult) {
                if (baasResult.isSuccess()) {
                    UserBaas.refreshProfile();
                    EventBus.getDefault().post(new GroupBus.LeaveGroupResult(false, document));
                } else {
                    Log.e(TAG, baasResult.error().toString());
                    EventBus.getDefault().post(new GroupBus.LeaveGroupResult(true, null));
                }
            }
        });
    }

    public static void getAllGroups(String studentType) {
        String collection = "group-" + studentType;

        BaasDocument.fetchAll(collection, new BaasHandler<List<BaasDocument>>() {
            @Override
            public void handle(BaasResult<List<BaasDocument>> baasResult) {
                if (baasResult.isSuccess()) {
                    Log.i(TAG, "FETCHED ALL GROUPS");
                    EventBus.getDefault().post(new GroupBus.GetAllGroupsResult(baasResult.value(), false, null));
                } else {
                    Log.e(TAG, baasResult.error().toString());
                    EventBus.getDefault().post(new GroupBus.GetAllGroupsResult(null, true, ErrorHandler.getMessage(baasResult.error())));
                }
            }
        });
    }
}
