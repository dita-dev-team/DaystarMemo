package com.dev.dita.daystarmemo.ui.connections;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dev.dita.daystarmemo.R;
import com.dev.dita.daystarmemo.controller.utils.ImageUtils;
import com.dev.dita.daystarmemo.model.database.Connection;
import com.dev.dita.daystarmemo.model.database.Memo;
import com.dev.dita.daystarmemo.ui.memosgroups.memos.MemosChatActivity;
import com.dev.dita.daystarmemo.ui.memosgroups.memos.NewMemoActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmModel;

public class ConnectionDetailsActivity extends AppCompatActivity {

    private final String TAG = getClass().getName();

    @BindView(R.id.connection_details_image)
    CircleImageView connectionImage;
    @BindView(R.id.connection_details_name_container)
    LinearLayout nameContainer;
    @BindView(R.id.connection_details_email_container)
    LinearLayout emailContainer;
    @BindView(R.id.connection_details_major_container)
    LinearLayout majorContainer;
    @BindView(R.id.connection_details_name_value)
    TextView name;
    @BindView(R.id.connection_details_email_value)
    TextView email;
    @BindView(R.id.connection_details_major_value)
    TextView major;

    private String username;

    public void init() {
        Bundle extras = getIntent().getExtras();
        username = null;
        if (extras != null) {
            username = extras.getString("username");
        }

        Realm realm = Realm.getDefaultInstance();
        Connection connection = realm.where(Connection.class).equalTo("username", username).findFirst();
        connection.addChangeListener(new RealmChangeListener<RealmModel>() {
            @Override
            public void onChange(RealmModel element) {
                Log.i(getClass().getName(), "IMAGE UPDATED");
                Connection connection = (Connection) element;
                if (connection.image != null) {
                    connectionImage.setImageBitmap(ImageUtils.decodeBitmapFromByteArray(connection.image));
                }
            }
        });

        if (connection.image != null) {
            connectionImage.setImageBitmap(ImageUtils.decodeBitmapFromByteArray(connection.image));
        }

        if (!TextUtils.isEmpty(connection.name)) {
            name.setText(connection.name);
            nameContainer.setVisibility(View.VISIBLE);
        } else {
            nameContainer.setVisibility(View.GONE);
        }

        if (!TextUtils.isEmpty(connection.email)) {
            email.setText(connection.email);
            emailContainer.setVisibility(View.VISIBLE);
        } else {
            emailContainer.setVisibility(View.GONE);
        }

        if (!TextUtils.isEmpty(connection.major)) {
            major.setText(connection.major.toUpperCase());
            majorContainer.setVisibility(View.VISIBLE);
        } else {
            majorContainer.setVisibility(View.GONE);
        }

        setTitle(username);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connection_details);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        init();
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        NavUtils.navigateUpFromSameTask(this);
    }

    @OnClick(R.id.connections_details_send_memo)
    public void onSendButtonClicked() {
        Realm realm = Realm.getDefaultInstance();
        long count = realm.where(Memo.class).equalTo("connection", username).equalTo("isFile", false).count();
        Intent intent;
        if (count == 0) {
            intent = new Intent(ConnectionDetailsActivity.this, NewMemoActivity.class);
        } else {
            intent = new Intent(ConnectionDetailsActivity.this, MemosChatActivity.class);
        }

        intent.putExtra("username", username);
        startActivity(intent);
    }

}
