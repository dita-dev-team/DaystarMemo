package com.dev.dita.daystarmemo.ui.customviews;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dev.dita.daystarmemo.R;
import com.dev.dita.daystarmemo.model.objects.Event;
import com.tokenautocomplete.TokenCompleteTextView;

import java.util.List;

public class EventCompletionView extends TokenCompleteTextView<Event> {
    public EventCompletionView(Context context) {
        super(context);
    }

    public EventCompletionView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public EventCompletionView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected View getViewForObject(Event event) {
        LayoutInflater layoutInflater = (LayoutInflater) getContext().getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        LinearLayout view = (LinearLayout) layoutInflater.inflate(R.layout.event_token, (ViewGroup) EventCompletionView.this.getParent(), false);
        ((TextView) view.findViewById(R.id.event_title)).setText(event.title);

        return view;
    }

    @Override
    protected Event defaultObject(String completionText) {
        return null;
    }

    public void removeObject(List<Event> events) {
        for (Event e : events) {
            removeObject(e);
        }
    }
}
