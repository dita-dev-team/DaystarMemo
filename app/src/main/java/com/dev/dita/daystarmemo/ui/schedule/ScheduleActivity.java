package com.dev.dita.daystarmemo.ui.schedule;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.baasbox.android.BaasUser;
import com.baasbox.android.json.JsonArray;
import com.baasbox.android.json.JsonObject;
import com.dev.dita.daystarmemo.PrefSettings;
import com.dev.dita.daystarmemo.R;
import com.dev.dita.daystarmemo.controller.bus.UserBus;
import com.dev.dita.daystarmemo.controller.utils.AnimationUtils;
import com.dev.dita.daystarmemo.controller.utils.UIUtils;
import com.dev.dita.daystarmemo.model.baas.UserBaas;
import com.dev.dita.daystarmemo.model.database.Schedule;
import com.dev.dita.daystarmemo.model.objects.Event;
import com.dev.dita.daystarmemo.services.ScheduleService;
import com.dev.dita.daystarmemo.ui.BaseActivity;
import com.dev.dita.daystarmemo.ui.customviews.DraggableTouchListener;
import com.dev.dita.daystarmemo.ui.customviews.EventCompletionView;
import com.tokenautocomplete.TokenCompleteTextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmList;
import io.realm.RealmResults;
import io.realm.Sort;

public class ScheduleActivity extends BaseActivity implements TokenCompleteTextView.TokenListener<Event> {

    private static final String TAG = ScheduleActivity.class.getName();
    @BindView(R.id.schedule_list)
    RecyclerView scheduleList;

    @BindView(R.id.edit_events)
    CardView editEvents;

    @BindView(R.id.new_events_txt)
    EventCompletionView newEventsTxt;

    @BindView(R.id.add_event_bnt)
    ImageButton addEventBtn;

    @BindView(R.id.empty)
    TextView empty;

    @BindView(R.id.drag_icon)
    ImageButton dragButton;

    private RealmResults<Schedule> attending;
    private ScheduleListAdapter adapter;
    private DrawerLayout drawerLayout;
    private boolean isActive = false;
    private boolean dataAvailable = false;
    private ArrayList<String> titles;
    private MenuItem addItem;
    private boolean fromProfile;
    private final RealmChangeListener<RealmResults<Schedule>> changeListener = new RealmChangeListener<RealmResults<Schedule>>() {
        @Override
        public void onChange(RealmResults<Schedule> element) {
            // If there's no result check profile
            if (element.size() == 0) {
                checkProfileForSchedule();
            } else {
                adapter.updateData(element);
            }
            empty.setVisibility(adapter.getItemCount() == 0 ? View.VISIBLE : View.GONE);
        }
    };

    private void init() {
        Realm realm = Realm.getDefaultInstance();
        attending = realm.where(Schedule.class).equalTo("attending", true).findAllSortedAsync("dayIndex", Sort.ASCENDING);
        attending.addChangeListener(changeListener);
        adapter = new ScheduleListAdapter(this, attending, true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        scheduleList.setLayoutManager(layoutManager);
        scheduleList.setAdapter(adapter);
        newEventsTxt.performBestGuess(false);
        newEventsTxt.setTokenListener(this);
        adapter.setOnItemClickListener(new ScheduleListAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

            }
        });
        adapter.setOnItemLongClickListener(new ScheduleListAdapter.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(View view, int position) {
                showPopup(view, position);
                return true;
            }
        });
        DraggableTouchListener draggableTouchListener = new DraggableTouchListener(editEvents);
        editEvents.setOnTouchListener(draggableTouchListener);
        dragButton.setOnTouchListener(draggableTouchListener);

        backgroundInit();
        setTitle("Schedule");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schedule);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        ButterKnife.bind(this);
        EventBus.getDefault().register(this);
        Toolbar toolbar = getActionBarToolbar();
        toolbar.setNavigationIcon(R.drawable.ic_back_white);
        toolbar.setNavigationContentDescription(R.string.close_and_go_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                navigateUpOrBack(ScheduleActivity.this, null);
            }
        });
        setSupportActionBar(toolbar);
        init();
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();
        initUser();
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout != null && drawerLayout.isDrawerOpen(GravityCompat.END)) {
            drawerLayout.closeDrawer(GravityCompat.END);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected int getSelfNavDrawerItem() {
        return NAVDRAWER_ITEM_SCHEDULE;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_schedule, menu);
        addItem = menu.findItem(R.id.action_add);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_add:
                if (!dataAvailable) {
                    return false;
                }
                if (!isActive) {
                    isActive = true;
                    item.setIcon(ContextCompat.getDrawable(this, R.drawable.ic_close));
                    AnimationUtils.translateVisibility(editEvents, 300, View.VISIBLE);
                    //editEvents.setVisibility(View.VISIBLE);
                } else {
                    isActive = false;
                    item.setIcon(ContextCompat.getDrawable(this, R.drawable.ic_add));
                    AnimationUtils.translateVisibility(editEvents, 300, View.GONE);
                    newEventsTxt.removeObject(newEventsTxt.getObjects());
                    UIUtils.hideKeyboard(this);
                    //editEvents.setVisibility(View.GONE);
                }
                return true;
            case R.id.action_refresh:
                Toast.makeText(this, R.string.to_be_implemented, Toast.LENGTH_SHORT).show();
                return true;
            case R.id.action_save:
                if (adapter.getItemCount() > 0) {
                    JsonArray array = new JsonArray();
                    for (Schedule schedule : adapter.getData()) {
                        array.add(schedule.title);
                    }
                    UserBaas.updateSchedules(array);
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

   /* @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }*/

    @Override
    public void onTokenAdded(Event token) {
        addEventBtn.setEnabled(true);
    }

    @Override
    public void onTokenRemoved(Event token) {
        if (newEventsTxt.getObjects().size() <= 0) {
            addEventBtn.setEnabled(false);
        }
    }

    @OnClick(R.id.add_event_bnt)
    public void setAttending() {
        List<Event> events = newEventsTxt.getObjects();

        if (events.size() == 0) {
            return;
        }
        titles = new ArrayList<>();
        for (final Event event : events) {
            titles.add(event.title);
        }
        Schedule.setAttending(titles, true);
        newEventsTxt.removeObject(events);
    }

    @Subscribe
    public void onEvent(UserBus.ScheduleUpdated event) {
        if (fromProfile) {
            fromProfile = false;
            return;
        }
        isActive = false;
        addItem.setIcon(ContextCompat.getDrawable(this, R.drawable.ic_add));
        AnimationUtils.translateVisibility(editEvents, 300, View.GONE);

        JsonArray array = new JsonArray();
        for (Schedule schedule : adapter.getData()) {
            array.add(schedule.title);
        }
        UserBaas.updateSchedules(array);
    }

    @Subscribe
    public void onEvent(UserBus.ScheduleUpdatedOnProfile event) {
        String message;
        if (!event.error) {
            message = getString(R.string.schedule_to_profile_success);
        } else {
            message = getString(R.string.schedule_to_profile_fail);
        }

        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Subscribe
    public void onEvent(UserBus.DownloadSchedule event) {
        Intent intent = new Intent(ScheduleActivity.this, ScheduleService.class);
        intent.putExtra("update", true);
        startService(intent);
    }

    @Subscribe
    public void onEvent(UserBus.SaveSchedule event) {
        backgroundInit();
    }

    private void showPopup(View view, int position) {
        final Schedule schedule = adapter.getItem(position);
        PopupMenu popupMenu = new PopupMenu(this, view);
        MenuInflater inflater = popupMenu.getMenuInflater();
        inflater.inflate(R.menu.popup_menu_schedule, popupMenu.getMenu());
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.schedule_remove:
                        Schedule.removeSchedule(schedule.title);
                        return true;
                    default:
                        return false;
                }
            }
        });
        popupMenu.show();
    }

    private void backgroundInit() {
        new AsyncTask<Void, Void, ArrayList<Event>>() {
            String message;
            boolean dataFetched = false;
            @Override
            protected ArrayList<Event> doInBackground(Void... voids) {
                String studentType = PrefSettings.getStringValue(ScheduleActivity.this, "studentType");
                ArrayList<Event> events = null;
                if (studentType.equals(getString(R.string.none)) || studentType.isEmpty()) {
                    message = getString(R.string.select_student_type);
                } else {
                    dataFetched = true;
                    dataAvailable = true;
                    message = getString(R.string.no_schedules_found);
                    events = new ArrayList<>();
                    Realm realm = Realm.getDefaultInstance();
                    RealmResults<Schedule> schedules = realm.where(Schedule.class).equalTo("attending", false).findAllSorted("title");
                    for (Schedule schedule : schedules) {
                        events.add(new Event(schedule.title));
                    }
                    Log.i(TAG, "SIZE: " + events.size());
                }

                return events;
            }

            @Override
            protected void onPostExecute(ArrayList<Event> events) {
                empty.setText(message);
                empty.setVisibility(adapter.getItemCount() == 0 ? View.VISIBLE : View.GONE);
                if (dataFetched) {
                    Log.i(TAG, "SETTING DATA");
                    newEventsTxt.setAdapter(new ArrayAdapter<>(ScheduleActivity.this, android.R.layout.simple_list_item_1, events));
                }
            }
        }.execute();
    }

    private void checkProfileForSchedule() {
        if (BaasUser.current() != null) {
            JsonObject object = BaasUser.current().getScope(BaasUser.Scope.PRIVATE);
            if (object != null) {
                JsonArray array = object.getArray("schedule");
                if (array != null) {
                    titles = new ArrayList<>();
                    for (Object o : array) {
                        titles.add((String) o);
                    }

                    if (titles.size() > 0) {
                        Schedule.setAttending(titles, true);
                        fromProfile = true;
                    }

                }
            }
        }
    }

    private void sortSchedule(RealmList<Schedule> queryResult) {

    }
}
