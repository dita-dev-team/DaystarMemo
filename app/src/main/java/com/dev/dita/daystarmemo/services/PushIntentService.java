package com.dev.dita.daystarmemo.services;

import android.app.IntentService;
import android.content.Intent;

import com.dev.dita.daystarmemo.controller.DaystarMemoNotification;
import com.dev.dita.daystarmemo.model.database.Transfer;
import com.dev.dita.daystarmemo.model.objects.Notification;

/**
 * The type Push intent service. Shows notification on status bar
 */
public class PushIntentService extends IntentService {

    public PushIntentService() {
        super("PushIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Notification notification = (Notification) intent.getExtras().getSerializable("notification");
        DaystarMemoNotification.notify(this, notification.sender, notification.message);

        if (notification.type == Notification.MESSAGE) {
            Intent memo = new Intent(this, MemoDownloadService.class);
            memo.putExtra("remoteId", notification.id);
            startService(memo);
        } else {
            Transfer.addPendingDownload(notification);
        }

    }
}
