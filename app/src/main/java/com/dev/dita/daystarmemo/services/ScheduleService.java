package com.dev.dita.daystarmemo.services;

import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;
import android.text.TextUtils;
import android.util.Log;

import com.dev.dita.daystarmemo.PrefSettings;
import com.dev.dita.daystarmemo.R;
import com.dev.dita.daystarmemo.model.baas.MemoBaas;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class ScheduleService extends Service {
    public ScheduleService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        boolean firstRun = PrefSettings.scheduleServiceFirstRun(getApplicationContext());

        if (!firstRun) {
            boolean fetch = intent.getBooleanExtra("update", false);
            if (!fetch) {
                stopSelf();
                return super.onStartCommand(intent, flags, startId);
            }
        }

        if (firstRun) {
            PrefSettings.setScheduleServiceFirstRun(getApplicationContext(), false);
        }



        String type = PrefSettings.getStringValue(this, "studentType");
        if (type.equals(getString(R.string.none)) || TextUtils.isEmpty(type)) {
            stopSelf();
        } else if (type.equalsIgnoreCase("athi-river")) {
            //MemoBaas.fetchSchedule(this, "athitt");
            checkIfCollectionExists("athitt");
        } else if (type.equalsIgnoreCase("nairobi")) {
            //MemoBaas.fetchSchedule(this, "nairobidaytt");
            checkIfCollectionExists("nairobidaytt");
        } else if (type.equalsIgnoreCase("evening")) {
            //MemoBaas.fetchSchedule(this, "nairobieveningtt");
            checkIfCollectionExists("nairobieveningtt");
        }

        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.e(getClass().getName(), "DESTROYING");
    }

    public void checkIfCollectionExists(final String collection) {
        new AsyncTask<Void, Void, Boolean>() {
            @Override
            protected Boolean doInBackground(Void... voids) {
                OkHttpClient client = new OkHttpClient();

                try {
                    String session = getSessionKey(client);
                    if (session == null) {
                        return false;
                    }


                    Request request = new Request.Builder()
                            .url("http://ec2-54-218-179-22.us-west-2.compute.amazonaws.com:9000/admin/collection/" + collection)
                            .addHeader("X-BB-SESSION", session)
                            .post(new FormBody.Builder().build())
                            .build();
                    Response response = client.newCall(request).execute();
                    String responseBody = response.body().string();
                    if (responseBody.contains("already exists")) {
                        return true;
                    }
                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }

                return false;
            }

            @Override
            protected void onPostExecute(Boolean aBoolean) {
                if (aBoolean) {
                    MemoBaas.fetchSchedule(ScheduleService.this, collection);
                } else {
                    stopSelf();
                }
            }

            private String getSessionKey(final OkHttpClient client) throws IOException, JSONException {
                String session = PrefSettings.getStringValue(ScheduleService.this, "session_key");
                if (session.isEmpty()) {
                    // Create form body
                    RequestBody body = new FormBody.Builder()
                            .add("username", "admin")
                            .add("password", "admin")
                            .add("appcode", "1234567890")
                            .build();

                    // Login as admin
                    Request request = new Request.Builder()
                            .url("http://ec2-54-218-179-22.us-west-2.compute.amazonaws.com:9000/login")
                            .post(body)
                            .build();

                    Response response = client.newCall(request).execute();
                    String responseBody = response.body().string();
                    Log.i(getClass().getName(), "LENGTH: " + responseBody.length());
                    if (response.code() == 200) {
                        JSONObject object = new JSONObject(responseBody);
                        session = object.getJSONObject("data").getString("X-BB-SESSION");
                        PrefSettings.setValue(ScheduleService.this, "session_key", session);
                        return session;
                    }
                } else {
                    return session;
                }

                return null;
            }
        }.execute();
    }
}
