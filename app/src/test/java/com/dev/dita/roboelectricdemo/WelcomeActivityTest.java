package com.dev.dita.roboelectricdemo;

import android.os.Build;

import com.dev.dita.daystarmemo.BuildConfig;
import com.dev.dita.daystarmemo.ui.welcome.WelcomeActivity;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

@Config(constants = BuildConfig.class, sdk = Build.VERSION_CODES.KITKAT)
@RunWith(RobolectricTestRunner.class)
public class WelcomeActivityTest {
    private WelcomeActivity activity;

    @Before
    public void setUp() {
        activity = Robolectric.setupActivity(WelcomeActivity.class);
    }

    @Test
    public void validateButtons() {

    }
}
