package com.dev.dita.daystarmemo.schedules;

import com.dev.dita.daystarmemo.model.database.Group;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;

import io.realm.Realm;
import io.realm.RealmConfiguration;

import static org.junit.Assert.assertEquals;

//@Config(constants = BuildConfig.class, sdk = Build.VERSION_CODES.KITKAT)
@RunWith(RobolectricTestRunner.class)
//@PowerMockIgnore({"org.mockito.*", "org.robolectric.*", "android.*"})
//@PrepareForTest({Realm.class, RealmConfiguration.class, RealmQuery.class, RealmResults.class, RealmCore.class})
public class ScheduleSortingTest {

    //@Rule
    //public PowerMockRule rule = new PowerMockRule();

    private Realm realm;

    @BeforeClass
    public static void setUpClass() {

    }

    @Before
    public void setUp() {
       /* mockStatic(Realm.class);
        //mockStatic(RealmConfiguration.class);
        //mockStatic(RealmCore.class);

        Realm mockRealm = mock(Realm.class);
        //final RealmConfiguration mockRealmConfig = mock(RealmConfiguration.class);
        //doNothing().when(RealmCore.class);
        //RealmCore.loadLibrary(any(Context.class));

        //when(Realm.getInstance(any(RealmConfiguration.class))).thenReturn(mockRealm);

        when(Realm.getDefaultInstance()).thenReturn(mockRealm);

        this.mockRealm = mockRealm;*/
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder(RuntimeEnvironment.application).build();
        Realm.deleteRealm(realmConfiguration);
        Realm.setDefaultConfiguration(realmConfiguration);
        realm = Realm.getDefaultInstance();
        realm.deleteAll();

    }


    @Test
    public void testCreateGroup() {
        Group group = new Group();
        group.name = "ACS101";
        group.type = Group.ACADEMIC;
        group.interaction = Group.INTERACTIVE;
        group.privacy = Group.OPEN;

        Group result = realm.copyToRealm(group);

        assertEquals(group, result);

    }

}
