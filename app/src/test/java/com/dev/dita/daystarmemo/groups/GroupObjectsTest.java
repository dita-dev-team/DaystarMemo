package com.dev.dita.daystarmemo.groups;

import android.os.Build;

import com.dev.dita.daystarmemo.BuildConfig;

import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

@Config(constants = BuildConfig.class, sdk = Build.VERSION_CODES.KITKAT)
@RunWith(RobolectricTestRunner.class)
public class GroupObjectsTest {
}
