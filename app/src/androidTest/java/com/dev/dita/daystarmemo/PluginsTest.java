package com.dev.dita.daystarmemo;

import android.support.test.runner.AndroidJUnit4;

import com.baasbox.android.BaasBox;
import com.baasbox.android.BaasDocument;
import com.baasbox.android.BaasResult;
import com.baasbox.android.BaasUser;
import com.baasbox.android.Rest;
import com.baasbox.android.json.JsonArray;
import com.baasbox.android.json.JsonObject;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@RunWith(AndroidJUnit4.class)
public class PluginsTest {
    private static final String EMU_ADDRESS = "10.0.2.2";
    private static final String USERNAME = "grouptest";
    private static final String PASSWORD = "grouptest";
    private static final String USERNAME2 = "grouptest2";
    private static final String PASSWORD2 = "grouptest2";
    private static final String COLLECTION_NAME = "group-athi-river";
    private static final String GROUP_NAME = "TEST101";

    BaasResult<JsonObject> result;

    @BeforeClass
    public static void setUpClass() {

        JsonObject object = new JsonObject();
        object.put("username", USERNAME);
        object.put("password", PASSWORD);

        //BaasBox.rest().sync(Rest.Method.POST, "plugin/users.script", object);

        BaasUser.withUserName(USERNAME)
                .setPassword(PASSWORD)
                .signupSync();

        BaasUser.withUserName(USERNAME2)
                .setPassword(PASSWORD2)
                .signupSync();

        BaasUser.withUserName(USERNAME)
                .setPassword(PASSWORD)
                .loginSync();
    }

    @AfterClass
    public static void tearDownClass() {
        BaasUser.current().logoutSync();

        BaasUser.withUserName("admin")
                .setPassword("admin")
                .loginSync();

        BaasResult<JsonObject> result = BaasBox.rest().sync(Rest.Method.DELETE, "plugin/users.script?username=" + USERNAME, null);


        result = BaasBox.rest().sync(Rest.Method.DELETE, "plugin/users.script?username=" + USERNAME2, null);

        if (result.isFailed()) {
            result.error().printStackTrace();
        }
    }

    @Before
    public void setUp() {
        createGroup();
    }

    @Ignore
    public void testUserCreated() {
        assertNotNull(BaasUser.current());
    }

    @Ignore
    public void testGroupCreated() {
        BaasResult<List<BaasDocument>> result = BaasDocument.fetchAllSync(COLLECTION_NAME);
        assertNotEquals(0, result.value().size());
    }

    @Test
    public void testGroupDeleted() {
        result = BaasBox.rest().sync(Rest.Method.DELETE, "plugin/groups.script?collection=" + COLLECTION_NAME + "&deleteAll=" + false + "&group=" + GROUP_NAME, null);

        if (result.isFailed()) {
            result.error().printStackTrace();
        }
        assertTrue(result.isSuccess());
        BaasResult<List<BaasDocument>> result = BaasDocument.fetchAllSync(COLLECTION_NAME);
        assertEquals(0, result.value().size());
    }

    @Ignore
    public void testGroupJoiningAndLeaving() {
        JsonObject object = new JsonObject();
        object.put("collection", COLLECTION_NAME);
        object.put("group", GROUP_NAME);
        object.put("username", USERNAME2);
        object.put("addMember", true);

        BaasResult<JsonObject> result = BaasBox.rest().sync(Rest.Method.PUT, "plugin/groups.script", object);

        if (result.isFailed()) {
            result.error().printStackTrace();
        }

        assertTrue(result.isSuccess());

        BaasResult<List<BaasDocument>> documents = BaasDocument.fetchAllSync(COLLECTION_NAME);
        BaasDocument document = documents.value().get(0);
        assertEquals(document.getArray("members").size(), 2);

        BaasUser.current().refreshSync();

        JsonObject object1 = BaasUser.current().getScope(BaasUser.Scope.FRIEND);
        JsonArray array = object1.getArray("groups");
        assertEquals(1, array.size());
        assertEquals(GROUP_NAME, array.getString(0));

        result = BaasBox.rest().sync(Rest.Method.PUT, "plugin/groups.script", object);

        assertTrue(result.isFailed());

        object.remove("addMember");
        object.put("addMember", false);

        result = BaasBox.rest().sync(Rest.Method.PUT, "plugin/groups.script", object);

        if (result.isFailed()) {
            result.error().printStackTrace();
        }

        assertTrue(result.isSuccess());

        documents = BaasDocument.fetchAllSync(COLLECTION_NAME);
        document = documents.value().get(0);
        assertEquals(1, document.getArray("members").size());

        BaasUser.current().refreshSync();

        object1 = BaasUser.current().getScope(BaasUser.Scope.FRIEND);
        array = object1.getArray("groups");
        assertEquals(0, array.size());

        result = BaasBox.rest().sync(Rest.Method.PUT, "plugin/groups.script", object);

        assertTrue(result.isFailed());

    }

    public void createGroup() {
        JsonObject json = new JsonObject();
        json.put("collection", COLLECTION_NAME);

        JsonArray array = new JsonArray();
        array.add(BaasUser.current().getName());

        JsonObject object = new JsonObject();
        object.put("name", GROUP_NAME);
        object.put("owners", array);
        object.put("members", array);
        object.put("privacy", "open");
        object.put("type", "academic");
        object.put("interaction", "interactive");

        json.put("doc", object);

        result = BaasBox.rest().sync(Rest.Method.POST, "plugin/groups.script", json);
    }

    @After
    public void tearDown() {
        result = BaasBox.rest().sync(Rest.Method.DELETE, "plugin/groups.script?collection=" + COLLECTION_NAME + "&deleteAll=" + true, null);

        if (result.isFailed()) {
            result.error().printStackTrace();
        }
    }
}
