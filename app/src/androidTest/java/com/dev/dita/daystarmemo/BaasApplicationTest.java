package com.dev.dita.daystarmemo;

import android.test.AndroidTestCase;
import android.util.Log;

import com.baasbox.android.BaasBox;
import com.baasbox.android.BaasException;
import com.baasbox.android.BaasFile;
import com.baasbox.android.BaasHandler;
import com.baasbox.android.BaasResult;
import com.baasbox.android.BaasUser;
import com.baasbox.android.RequestToken;
import com.dev.dita.daystarmemo.controller.utils.OkClient;

import org.greenrobot.eventbus.EventBus;


/**
 * Created by michael on 08/07/16.
 */

public class BaasApplicationTest extends AndroidTestCase {
    private final static String TEXT_FILE =
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean auctor dignissim mauris vitae iaculis. Morbi pharetra sem lorem, vel rutrum mauris facilisis sit amet. Curabitur non lacus nec lacus cursus dictum. Ut dictum scelerisque ultricies. Nulla semper nibh at tincidunt blandit. Vestibulum eget suscipit felis, nec sagittis tellus. Nunc a volutpat turpis. Pellentesque et nisi eget nisl interdum vulputate. Aliquam molestie sem nec tempus sodales.\n" +
                    "\n" +
                    "Fusce sollicitudin rhoncus lorem. Nam convallis ut tortor at tempor. Nunc pharetra interdum fringilla. Proin id cursus urna. Donec nec turpis vehicula magna aliquam gravida vel nec dolor. Sed viverra nunc tempus, bibendum velit at, porttitor odio. Proin eu nunc est. Praesent laoreet risus porta varius condimentum. Nullam tincidunt erat felis, ac facilisis nisl faucibus sit amet. Nam vulputate ipsum eu lorem pellentesque suscipit. Vestibulum nisl mauris, lacinia ut erat sit amet, vestibulum tempus lacus.\n" +
                    "\n" +
                    "Curabitur ut nunc suscipit, sodales magna at, laoreet felis. Fusce pretium volutpat lacus et suscipit. Sed et nibh tristique, vulputate quam sed, consequat lacus. Vestibulum id feugiat nulla, at blandit est. Donec sit amet porta enim. Nullam sit amet felis in metus eleifend ornare. Nulla facilisis, lacus a sagittis malesuada, dui erat aliquet mi, sed rutrum purus tellus eget mauris.\n" +
                    "\n" +
                    "Donec suscipit pellentesque gravida. Etiam a diam congue, accumsan nisl et, lobortis metus. Maecenas vel sapien sollicitudin, consequat urna sit amet, tempor nisi. Morbi id congue lorem. In rhoncus, nunc quis elementum feugiat, neque orci venenatis ante, nec blandit nibh neque consectetur sem. Nunc nec tincidunt lacus. Vivamus eu massa mollis, lobortis sem eget, ultrices nisl. Aliquam erat volutpat.\n" +
                    "\n" +
                    "Integer euismod consectetur lacus sed fermentum. Maecenas magna neque, venenatis quis est sit amet, molestie vehicula tortor. Nulla ut fermentum tortor, et feugiat mauris. Aenean faucibus elit lorem. Integer ultrices lacus quis rutrum placerat. Cras hendrerit neque sit amet lobortis accumsan. Donec non tincidunt lorem.";
    final String USERNAME = "test";
    final String PASSWORD = "testpass";
    BaasBox box;

    public BaasApplicationTest() {
        EventBus.getDefault().register(getContext());
    }


    public BaasBox initBaasbox() {
        OkClient client = new OkClient();
        BaasBox.Builder builder = BaasBox.builder(getContext());
        return builder.setApiDomain(BuildConfig.HOST)
                .setPort(BuildConfig.PORT)
                .setAppCode(BuildConfig.APP_KEY)
                .setAuthentication(BaasBox.Config.AuthType.SESSION_TOKEN)
                .setSessionTokenExpires(false)
                .setSenderId(BuildConfig.SENDERID)
                .setRestClient(client)
                .init();
    }

    public void createUser() {
        BaasResult<BaasUser> result = BaasUser.fetchSync(USERNAME);
        if (result.isFailed()) {
            result = BaasUser.withUserName(USERNAME)
                    .setPassword(PASSWORD)
                    .signupSync();
            if (result.isFailed()) fail(result.error().toString());
        }
    }


    public void testFileUpload() {
        box = initBaasbox();
        createUser();
        Log.i(getName(), "SETUP FINISHED");

        byte[] bytes = TEXT_FILE.getBytes();
        BaasFile f = new BaasFile();
        RequestToken token = f.upload(bytes, BaasHandler.NOOP);
        BaasResult<BaasFile> result = token.await();

        try {
            f = result.get();
            assertNotNull(f.getId());
            assertNotNull(f.getAuthor());
            assertEquals(f.getAuthor(), BaasUser.current().getName());
        } catch (BaasException e) {
            fail("upload failed");
        }
    }


}
